#!/opt/py27env/bin/python2.7
# http://gitlab/snippets/97

import datetime

# This comes from gitlab/snippets/155
from base_pygh import *

from pyghweb.ghw.models import *

from collections import namedtuple

from django.conf import settings

import core_pb2

from pyghweb.ghw.models import Contract
from pyghweb.symbology.models import ActivExchange
from pyghweb.symbology.models import ActivInstrument
from pyghweb.symbology.models import BsymExchange
from pyghweb.symbology.models import BsymInstrument
from pyghweb.symbology.models import DynamicTickSizes
from pyghweb.symbology.models import GhExchange
from pyghweb.symbology.models import GhInstrument
from pyghweb.symbology.models import MicExchange
from pyghweb.symbology.models import NativeInstrument
from pyghweb.symbology.models import RtsExchange
from pyghweb.symbology.models import RtsInstrument
from pyghweb.symbology.models import SedolInstrument


def add_contract(
        contract_type,
        exchange,
        ticker,
        currency='',
        point_value=0,
        trade_cost_per_lot=0,
        cost_currency='',
        expire_at=None,
        underlying_contract=None,
        using=None):

    manager = Contract.objects
    if using:
        manager = manager.using(using)

    contract, created = manager.get_or_create(
        type=contract_type,
        exchange=exchange,
        ticker=ticker,
        expire_at=expire_at,
        defaults={
            'currency': currency,
            'point_value': point_value,
            'trade_cost_per_lot': trade_cost_per_lot,
            'cost_currency': cost_currency,
            'underlying_contract': underlying_contract,
        }
    )
    if created:
        print 'Contract %s created' % contract
    else:
        print 'Contract %s already exists' % contract
    return contract


def add_underlying_contract(contract_type, exchange, ticker):
    return add_contract(contract_type, exchange, ticker)


def add_etf_contract(
        exchange,
        ticker,
        currency,
        point_value,
        trade_cost_per_lot,
        cost_currency):

    underlying_contract = add_underlying_contract(
        Contract.CONTRACT_TYPE_INDEX, exchange, ticker
    )

    contract = add_contract(
        Contract.CONTRACT_TYPE_STOCK,
        exchange,
        ticker,
        currency=currency,
        point_value=point_value,
        trade_cost_per_lot=trade_cost_per_lot,
        cost_currency=cost_currency,
        underlying_contract=underlying_contract
        )

    return contract


ContractRecord = namedtuple(
    'ContractRecord',
    [
        'exchange',
        'ticker',
        'currency',
        'point_value',
        'trade_cost_per_lot',
        'cost_currency',
    ])


def add_gh_instrument(
        instrument_type,
        exchange,
        symbol,
        symbol_maturity_format='',
        tick_size_fixed=None,
        tick_size_dynamic=None,
        round_lot=1):
    exchange = GhExchange.objects.get(exchange=exchange)

    instrument, created = GhInstrument.objects.get_or_create(
        exchange=exchange,
        instrument_type=instrument_type,
        symbol=symbol,
        defaults={
            'symbol_maturity_format': symbol_maturity_format,
            'tick_size_fixed': tick_size_fixed,
            'tick_size_dynamic': tick_size_dynamic,
            'round_lot': round_lot,
        }
    )
    if created:
        print 'GhInstrument %s created' % instrument
    else:
        print 'GhInstrument %s already exists' % instrument
    return instrument


def add_activ_instrument(
        instrument_type,
        exchange,
        symbol,
        gh_instrument):

    exchange = ActivExchange.objects.get(exchange=exchange)

    instrument, created = ActivInstrument.objects.get_or_create(
        exchange=exchange,
        instrument_type=instrument_type,
        symbol=symbol,
        defaults={
            'gh_instrument': gh_instrument,
        }
    )
    if created:
        print 'ActivInstrument %s created' % instrument
    else:
        print 'ActivInstrument %s already exists' % instrument
    return instrument


def add_bsym_instrument(
        instrument_type,
        exchange,
        symbol,
        gh_instrument):

    exchange = BsymExchange.objects.get(exchange=exchange)

    instrument, created = BsymInstrument.objects.get_or_create(
        exchange=exchange,
        instrument_type=instrument_type,
        symbol=symbol,
        defaults={
            'gh_instrument': gh_instrument,
        }
    )
    if created:
        print 'BsymInstrument %s created' % instrument
    else:
        print 'BsymInstrument %s already exists' % instrument
    return instrument


def add_native_instrument(
        instrument_type,
        exchange,
        symbol,
        gh_instrument,
        symbol_prefix='',
        symbol_postfix=''):

    exchange = MicExchange.objects.get(exchange=exchange)

    instrument, created = NativeInstrument.objects.get_or_create(
        exchange=exchange,
        instrument_type=instrument_type,
        symbol=symbol,
        defaults={
            'symbol_prefix': symbol_prefix,
            'symbol_postfix': symbol_postfix,
            'gh_instrument': gh_instrument,
        }
    )
    if created:
        print 'NativeInstrument %s created' % instrument
    else:
        print 'NativeInstrument %s already exists' % instrument
    return instrument


def add_rts_instrument(
        instrument_type,
        exchange,
        symbol,
        gh_instrument,
        symbol_prefix='',
        symbol_maturity_format='',
        server=''):

    exchange = RtsExchange.objects.get(exchange=exchange)

    instrument, created = RtsInstrument.objects.get_or_create(
        exchange=exchange,
        instrument_type=instrument_type,
        symbol=symbol,
        defaults={
            'symbol_prefix': symbol_prefix,
            'symbol_maturity_format': symbol_maturity_format,
            'server': server,
            'gh_instrument': gh_instrument,
        }
    )
    if created:
        print 'RtsInstrument %s created' % instrument
    else:
        print 'RtsInstrument %s already exists' % instrument
    return instrument


def add_sedol_instrument(
        instrument_type,
        symbol,
        gh_instrument,
        maturity_month=0
        ):

    instrument, created = SedolInstrument.objects.get_or_create(
        symbol=symbol,
        defaults={
            'maturity_month': maturity_month,
            'gh_instrument': gh_instrument,
        }
    )
    if created:
        print 'SedolInstrument %s created' % instrument
    else:
        print 'SedolInstrument %s already exists' % instrument
    return instrument


GhInstrumentRecord = namedtuple(
    'GhInstrumentRecord',
    [
        'symbol',
        'symbol_maturity_format',
        'tick_size_fixed',
        'tick_size_dynamic',
        'round_lot',
    ]
)


ActivInstrumentRecord = namedtuple(
    'ActivInstrumentRecord',
    [
        'symbol',
    ]
)


BsymInstrumentRecord = namedtuple(
    'BsymInstrumentRecord',
    [
        'symbol',
    ]
)


NativeInstrumentRecord = namedtuple(
    'NativeInstrumentRecord',
    [
        'symbol',
        'symbol_prefix',
        'symbol_postfix',
    ]
)


RtsInstrumentRecord = namedtuple(
    'RtsInstrumentRecord',
    [
        'symbol',
        'symbol_prefix',
        'symbol_maturity_format',
        'server'
    ]
)


SedolInstrumentRecord = namedtuple(
    'SedolInstrumentRecord',
    [
        'symbol',
        'maturity_month',
    ]
)


def add_symbology(
        exchange,
        instrument_type,
        gh_instrument_record,
        activ_instrument_record=None,
        bsym_instrument_record=None,
        native_instrument_record=None,
        rts_instrument_record=None,
        sedol_instrument_record=None,
        ):

    exchange_mapping = {
        'TSE': {
            'MIC': 'XTKS',
            'ACTIV': 'ET',
            'RTS': 'TSE',
            'BSYM': 'JT',
        }
    }

    gh_instrument = add_gh_instrument(
        instrument_type,
        exchange,
        gh_instrument_record.symbol,
        symbol_maturity_format=gh_instrument_record.symbol_maturity_format or '',
        tick_size_fixed=gh_instrument_record.tick_size_fixed,
        tick_size_dynamic=gh_instrument_record.tick_size_dynamic,
        round_lot=gh_instrument_record.round_lot or 1
    )

    if activ_instrument_record:
        activ_instrument = add_activ_instrument(
            instrument_type,
            exchange_mapping[exchange]['ACTIV'],
            activ_instrument_record.symbol,
            gh_instrument
        )
    else:
        activ_instrument = None

    if bsym_instrument_record:
        bsym_instrument = add_bsym_instrument(
            instrument_type,
            exchange_mapping[exchange]['BSYM'],
            bsym_instrument_record.symbol,
            gh_instrument
        )
    else:
        bsym_instrument = None


    if native_instrument_record:
        native_instrument = add_native_instrument(
            instrument_type,
            exchange_mapping[exchange]['MIC'],
            native_instrument_record.symbol,
            gh_instrument,
            symbol_prefix=native_instrument_record.symbol_prefix or '',
            symbol_postfix=native_instrument_record.symbol_postfix or ''
        )
    else:
        native_instrument = None

    if rts_instrument_record:
        rts_instrument = add_rts_instrument(
            instrument_type,
            exchange_mapping[exchange]['RTS'],
            rts_instrument_record.symbol,
            gh_instrument,
            symbol_prefix=rts_instrument_record.symbol_prefix or'',
            symbol_maturity_format=rts_instrument_record.symbol_maturity_format or '',
            server=rts_instrument_record.server or ''
        )
    else:
        rts_instrument = None

    if sedol_instrument_record:
        sedol_instrument = add_sedol_instrument(
            instrument_type,
            sedol_instrument_record.symbol,
            gh_instrument,
            maturity_month=sedol_instrument_record.maturity_month or 0,
        )
    else:
        sedol_instrument = None

    return (
        gh_instrument,
        activ_instrument,
        bsym_instrument,
        native_instrument,
        rts_instrument,
        sedol_instrument,
    )


def run():
    EXCHANGE = 'TSE'
    CURRENCY = 'JPY'
    POINT_VALUE = 1
    TRADE_COST_PER_LOT = 0
    COST_CURRENCY = 'JPY'

    tickers = {
        '1364': {'sedol': 'BSLS246'},
    }
    ticker_list = sorted(tickers.keys())
    records = []
    for ticker in ticker_list:
        records.append(ContractRecord(
            EXCHANGE,
            ticker,
            CURRENCY,
            POINT_VALUE,
            TRADE_COST_PER_LOT,
            COST_CURRENCY
        ))

    for record in records:
        contract = add_etf_contract(
            record.exchange,
            record.ticker,
            record.currency,
            record.point_value,
            record.trade_cost_per_lot,
            record.cost_currency
        )
        print contract.desc
        print
        print '-' * 80

    for ticker in ticker_list:
        gh_instrument_record = GhInstrumentRecord(
            symbol=ticker,
            symbol_maturity_format='',
            tick_size_fixed=None,
            tick_size_dynamic=DynamicTickSizes.ID_TSE_STOCK,
            round_lot=10)
        activ_instrument_record = ActivInstrumentRecord(
            symbol=ticker
        )
        bsym_instrument_record = BsymInstrumentRecord(
            symbol=ticker
        )
        native_instrument_record = NativeInstrumentRecord(
            symbol=ticker,
            symbol_prefix='',
            symbol_postfix=''
        )
        rts_instrument_record = RtsInstrumentRecord(
            symbol=ticker,
            symbol_prefix='',
            symbol_maturity_format='',
            server=''
        )
        sedol_symbol = tickers[ticker].get('sedol', None)
        if sedol_symbol:
            sedol_instrument_record = SedolInstrumentRecord(
                symbol=sedol_symbol,
                maturity_month=0,
            )
        else:
            sedol_instrument_record = None

        add_symbology(
            EXCHANGE,
            core_pb2.INSTRUMENT_TYPE_STOCK,
            gh_instrument_record,
            activ_instrument_record=activ_instrument_record,
            bsym_instrument_record=bsym_instrument_record,
            native_instrument_record=native_instrument_record,
            rts_instrument_record=rts_instrument_record,
            sedol_instrument_record=sedol_instrument_record,
        )
        print
        print '-' * 80

