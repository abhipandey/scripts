import asyncio
from contextlib import closing

import httplib2
from bs4 import BeautifulSoup, SoupStrainer

import sys
import os
import urllib
import threading
from queue import Queue
import urllib.request

import requests

URL = 'https://coinmarketcal.com/en/?form%5Bdate_range%5D=06%2F05%2F2021+-+01%2F08%2F2024&form%5Bkeyword%5D=mainnet&form%5Bsort_by%5D=&form%5Bsubmit%5D='
DIR_TO_DOWNLOAD = '/tmp/p'


def getLinks(url):
    http = httplib2.Http()
    status, response = http.request(URL)

    csv_files = []
    for link in BeautifulSoup(response,
                              parse_only=SoupStrainer('article'),
                              features="html.parser"):
        print(f'ABHI: link = {link}')


class DownloadThread(threading.Thread):
    def __init__(self, queue, destfolder):
        super(DownloadThread, self).__init__()
        self.queue = queue
        self.destfolder = destfolder
        self.daemon = True

    def run(self):
        while True:
            url = self.queue.get()
            try:
                self.download_url(url)
            except Exception as e:
                print(f'Exception: {str(e)}')
            self.queue.task_done()

    def download_url(self, url):
        # change it to a different way if you require
        name = url.split('/')[-1]
        dest = os.path.join(self.destfolder, name)
        print("[%s] Downloading %s -> %s" % (self.ident, url, dest))
        urllib.request.urlretrieve(url, dest)


def download(urls, destfolder, numthreads=4):
    queue = Queue()
    for url in urls:
        queue.put(url)

    for i in range(numthreads):
        t = DownloadThread(queue, destfolder)
        t.start()

    queue.join()


def main():
    urls = getLinks(URL)
    print(f'urls  = {urls}')
    # download(urls, DIR_TO_DOWNLOAD)


if __name__ == '__main__':
    main()
