# what folders should we read?
# ag Affinity /home/abhishek.pandey/code/p/higgs_config/ -l  |sed -e 's/.*higgs_config.//g' | awk 'BEGIN {FS="/"} {print $1}' | sort -u
from PythonConfluenceAPI import ConfluenceAPI
import json
import subprocess
import re

import pprint
import os
import glob

from collections import defaultdict, namedtuple

import markdown

import commentjson


def get_results(cmd):
    res = subprocess.check_output(cmd, shell=True).decode('UTF-8')
    lines = res.split('\n')
    return lines


def read_file(file_path):
    with open(file_path, 'rt') as file:
        data = file.read()
        return data


TagInfo = namedtuple('TagInfo', ['server', 'app', 'tags'])


def get_all_tags(dir):
    all_tags = []
    cmd = f"egrep Affinity {dir} -rl | sed -e 's/.*higgs_config.//g' -e 's/\/.*//g' | sort -u"
    folders_to_read = [f for f in get_results(cmd) if f and f != '.git']
    print(f'results = {folders_to_read}')
    for folder in folders_to_read:
        pattern = os.path.join(dir, folder, '*', '*.json')
        files = glob.glob(pattern)
        print(f'folder = {folder}; files={files}; pattern={pattern}')

        for file in files:
            server = os.path.basename(os.path.dirname(file))
            instance = os.path.basename(file).replace('.json',
                                                      '').replace('.yaml', '')
            searcher = TagSearcher(file)

            tags = searcher.find_tags()
            tag_info = TagInfo(server, instance, tags)
            all_tags.append(tag_info)
            # print(f'file={file}; tags = {tags}')
    return all_tags


class TagSearcher:
    def __init__(self, file_path, tag='Affinity'):
        self._file_path = file_path
        self._tag = tag
        self._cpu_tags = defaultdict(list)

    def find_tags(self):
        data = read_file(self._file_path)
        print(f'file = {self._file_path}')
        try:
            dictionary_d = commentjson.loads(data)
            self._parse_for_affinity(dictionary_d, '')
        except Exception as e:
            # TODO(abhi) yes, i will miss out some servers due to this
            # not important to fix right now. it is a POC
            print(f'Exception: File: {self._file_path}; {e}')
        # gen_dict_extract('Affinity', dictionary_d)
        return self._cpu_tags

    def hyphen_range(self, s):
        """ yield each integer from a complex range string like "1-9,12, 15-20,23"

        >>> list(hyphen_range('1-9,12, 15-20,23'))
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 15, 16, 17, 18, 19, 20, 23]

        >>> list(hyphen_range('1-9,12, 15-20,2-3-4'))
        Traceback (most recent call last):
            ...
        ValueError: format error in 2-3-4
        """
        for x in s.split(','):
            elem = x.split('-')
            if len(elem) == 1:  # a number
                yield int(elem[0])
            elif len(elem) == 2:  # a range inclusive
                start, end = map(int, elem)
                for i in range(start, end + 1):
                    yield i
            else:  # more than one hyphen
                raise ValueError('format error in %s' % x)

    def _get_uniq_numbers(self, affinities):
        if isinstance(affinities, list):
            return affinities
        uids = []
        if '-' in affinities:
            print(f'affinities = {affinities}')
            list_int_ids = tuple([int(val) for val in affinities.split('-')])
            uids = list(range(list_int_ids[0], list_int_ids[1] + 1))
        else:
            print(f'affinities = {affinities}')
            uids = [i.strip() for i in affinities.split(',')]
        return sorted(set(map(int, uids)))

    def _parse_for_affinity(self, item, parent_key):
        # print(f'Reading : {item}')
        if isinstance(item, dict):
            self._parse_dict(item, parent_key)
        elif isinstance(item, list):
            self._parse_list(item, parent_key)
        elif isinstance(item, str):
            # print(f'.......... item={item}')
            self._parse_str(item, parent_key)
        else:
            # print(f'Unknown: {item}; type={type(item)} parent={parent_key}')
            pass

    def _parse_list(self, my_list, parent_key=''):
        for l in my_list:
            self._parse_for_affinity(l, parent_key)

    def _parse_str(self, s, parent_key=''):
        if s == 'Affinity':
            print(f'AFF NFOUND : {s}; parent={parent_key}')

    def _parse_dict(self, d, parent_key=''):
        for k, v in d.items():
            if k == 'Affinity':

                # nums = self._get_uniq_numbers(str(v))
                nums = list(self.hyphen_range(str(v)))
                # print(f'{parent_key}: Val: {v}; nums = {nums}')
                for n in nums:
                    self._cpu_tags[n].append(parent_key)

            self._parse_for_affinity(v, k)


def srch():
    searcher = TagSearcher(_FILE)
    tags = searcher.find_tags()