'''
Helps find old pages on wiki.
Creates an html page that contains list of all wiki pages that are over 2 yers old. 
Also provides the date they were last updated + the age (timeago ) old they are


https://github.com/atlassian-api/atlassian-python-api/blob/master/atlassian/confluence.py
python wiki-old-files.py --wiki-user abhishek.pandey --wiki-password grass123
'''
import argparse
import os
import pprint
import subprocess
import re
import datetime
from itertools import chain

pp = pprint.PrettyPrinter(indent=4)

# from gh_wiki.wiki import GhWiki
from atlassian import Confluence
import markdown

import timeago

_CURR_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')
_SCRIPT_RE = re.compile(r'<scriptPath>(.*)</scriptPath>')
_URL_RE = re.compile(r'<url>(.*)</url>')
_GIT_REPO_RE = re.compile(r'.*:p/(.*).git')
_NODE_LABEL_RE = re.compile(r'''node.*\n.*label ['\"](.*)['\"]''')
_SPACE = 'GHP'


class GhWiki:
    def __init__(self, name, password, space=_SPACE):
        self.name = name
        self.password = password
        self.space = space

        # https://atlassian-python-api.readthedocs.io/en/latest/confluence.html
        self.confluence = Confluence(url='http://wiki.grass.corp',
                                     username=name,
                                     password=password)

    def get_all_pages(self):
        all_pages = []
        pages = self.confluence.get_all_pages_from_space(
            self.space, limit=1000, expand='history.lastUpdated')
        start = 0
        while pages:
            all_pages.append(pages)

            # print(f'Got total pages={len(pages)}')
            total_pages_received = len(pages)
            start += total_pages_received
            pages = self.confluence.get_all_pages_from_space(
                self.space,
                start=start,
                limit=start + total_pages_received,
                expand='history.lastUpdated')
        return list(chain(*all_pages))


_PR = lambda x: print(
    f'pages = {x["title"]} .. {pprint.pformat(x, compact=True)}')


def find_all_pages_older_than_x(pages, days=365 * 2):
    for p in pages:
        last_updated = p['history']['lastUpdated']['when']
        dt_last_updated = datetime.datetime.strptime(
            last_updated,
            '%Y-%m-%dT%H:%M:%S.000+08:00')  # '2019-06-26T10:26:19.000+08:00'
        p['LAST_UPDATED'] = dt_last_updated
        # _PR(p)
    now = datetime.datetime.now()
    old_pages = list(
        filter(
            lambda x: x['LAST_UPDATED'] + datetime.timedelta(days=days) < now,
            pages))
    for p in old_pages:
        _PR(p)
    return old_pages


def create_markdown(old_pages):
    md = '''# Pages on wiki that are older than 2 years
    
| Page | Date  | Age |
| --- | --- | --- |
'''
    for p in old_pages:
        title = p['title']
        tinurl = f"http://wiki.grass.corp{p['_links']['tinyui']}"
        last = p['history']['lastUpdated']['when'][:10]
        dt_ago = timeago.format(p['LAST_UPDATED'])
        md += f"| [{title}]({tinurl}) | {last} | {dt_ago} |\n"
    print(f'md = {md}')
    write_to_html_nice(md)
    return md


def write_to_html_nice(md_text):
    html_file = f'wiki-old-pages.html'
    html = markdown.markdown(
        md_text,
        extensions=['markdown.extensions.extra', 'markdown.extensions.tables'])
    head = '''

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table.min.css">


        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table.min.js"></script>
    '''

    with open(html_file, 'wt') as opened_file:
        full_html = '''

<html lang="en">
  <head>
  ''' + head + '''
  </head>
  <body>


            ''' + html + '''

  </body>
</html>

            '''
        full_html = full_html.replace('<table>', '<table data-toggle="table">')
        opened_file.write(full_html)
        print(f'Updated {html_file}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Python code to find old wiki files')
    parser.add_argument('--wiki-user', action='store', required=True)
    parser.add_argument('--wiki-password', action='store', required=True)
    args = parser.parse_args()

    wiki = GhWiki(args.wiki_user, args.wiki_password)
    pages = wiki.get_all_pages()
    # for page in pages:
    #     page_str = pprint.pformat(page, compact=True)
    #     print(f'pages = {page["title"]} .. {page_str}')

    old_pages = find_all_pages_older_than_x(pages)
    print(f'total pages={len(pages)}')
    print(f'total pages={len(old_pages)}')
    create_markdown(old_pages)
