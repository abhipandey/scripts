from jira import JIRA


def main(username, ldp_password):
    options = {
        'server': 'http://jira/',
        'verify': True,
        'basic_auth': (username, ldp_password),
    }
    gh = JIRA(options=options, basic_auth=(username, ldp_password))
    # Find issues with attachments:
    # query = gh.search_issues(jql_str='summary ~ "FIX"')
    query = gh.search_issues(jql_str='text ~ FIX')
    for issue in query:
        print('Issue: {}: {}: {}    '.format(issue.key, issue.fields.summary,
                                             issue.permalink()))
        # print(dir(issue.fields))
        # print(issue)

    # main('gitlab', 'gitlab')


import re


def san(text):

    text = re.sub(r'<([^|>]+)\|([^|>]+)>', r'\2', text)
    print('text1: {}'.format(text))
    text = re.sub(r'<(http([^>]+))>', r'\1', text)
    print('text2: {}'.format(text))
    return text


san('<http://jira/browse/RW-325|CRW-325>')