'''
Helps creates links for JIRA ticket. 
If anybody mentions a jira ticket, this creates a link to it automatically.

https://github.com/atlassian-api/atlassian-python-api/blob/master/atlassian/confluence.py
python jr.py --jira-user abhishek.pandey --jira-password grass123
'''
import argparse
import os
import pprint
import subprocess
import re
import datetime
from itertools import chain

pp = pprint.PrettyPrinter(indent=4)

from atlassian import Jira
import markdown

import timeago

_CURR_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')
_SCRIPT_RE = re.compile(r'<scriptPath>(.*)</scriptPath>')
_URL_RE = re.compile(r'<url>(.*)</url>')
_GIT_REPO_RE = re.compile(r'.*:p/(.*).git')
_NODE_LABEL_RE = re.compile(r'''node.*\n.*label ['\"](.*)['\"]''')
_SPACE = 'GHP'


class GhJira:
    def __init__(self, name, password, space=_SPACE):
        self.name = name
        self.password = password
        self.space = space

        # https://atlassian-python-api.readthedocs.io/en/latest/confluence.html
        self.jira = Jira(url='http://jira.grass.corp',
                                     username=name,
                                     password=password)

    def issue_exists(self, key):
        return self.jira.issue_exists(key)
    def update_links(self):

        # Create or Update Issue Links
        issue_key= 'CM-4620'
        link_url = 'http://jira.grass.corp/browse/TRD-3573'
        title = 'TRD-3573'
        relationship = 'Mentions'
        # res = self.jira.create_or_update_issue_remote_links(issue_key, link_url, title, relationship=relationship)
        # print(f'res = {res}')
        link_id = 10003
        remote_link  = self.jira.get_issue_remote_link_by_id(issue_key, link_id)
        print(f'remote link = {remote_link}')
        link_types = self.jira.get_issue_link_types()
        print(f'link types={link_types}')
        iss = self.jira.issue(issue_key)
        # print(f'iss = {iss}')

        print(f'issue exists={self.issue_exists(title)}')
        print(f'----')
        print(f'issue exists={self.issue_exists("CM-6666")}')




_PR = lambda x: print(
    f'pages = {x["title"]} .. {pprint.pformat(x, compact=True)}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Python code to find old jira files')
    parser.add_argument('--jira-user', action='store', required=True)
    parser.add_argument('--jira-password', action='store', required=True)
    args = parser.parse_args()

    jira = GhJira(args.jira_user, args.jira_password)
    pages = jira.update_links()
