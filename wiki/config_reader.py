"""
Helps create cpu html files
pip install pypuppetdb==1.0.0
"""

import subprocess
import re

from collections import defaultdict

import markdown

from ghwiki import GhWiki
import tag_reader
import pypuppetdb

PUPPET_HOST = 'csqmgmt-puppetdb02.grass.corp'

_DIR = '/home/abhishek.pandey/code/p/higgs_config'
_PARENT_PAGE = 'Bot Pages'


def is_valid_server(server):
    return server.startswith('kdh') or 'jpxtrd' in server or 'stage' in server


# Got to hard-code as there's no easy to upload consistently
# Also, that would be a wastage

IMG_REPLACEMENTS = {
    'aqua': '/download/attachments/31588409/icons8-water-50.png',
    'mds': '/download/attachments/31588409/icons8-data-transfer-50.png',
    'hummer': '/download/attachments/31588409/icons8-hummer-50.png',
    'fish': '/download/attachments/31588409/icons8-fish-50.png',
    'babel': '/download/attachments/31588409/icons8-babel-50.png',
    'chewy': '/download/attachments/31588409/icons8-chewbacca-48.png',
    'phobos': '/download/attachments/31588409/icons8-drop-of-blood-48.png',
    'bob': '/download/attachments/31588409/bob-50.png',
    'carbo': '/download/attachments/31588409/icons8-electricity-50.png',
    'dcv': '/download/attachments/31588409/icons8-disconnected-50.png'
}


def parse_cpu(cpu_str):
    result = []
    for part in cpu_str.split(','):
        if '-' in part:
            a, b = part.split('-')
            a, b = int(a), int(b)
            result.extend(range(a, b + 1))
        else:
            a = int(part)
    if not result:
        result.append(a)
    return result


def find_affinities_infos():
    cmd = f'/usr/bin/egrep -ir affinity {_DIR} |  egrep -v "\/\/.*Affinity"'

    res = subprocess.check_output(cmd, shell=True).decode('UTF-8')
    lines = res.split('\n')
    pattern = re.compile(
        r'.*higgs.config.(?P<APP>.*)\/(?P<SERVER>.*)\/(?P<ID>.*).json.*Affinity...?.(?P<CPU>.*)\",?'
    )

    details = []
    for line in lines:
        print(f'..... \n\n${line}')
        groups = re.search(pattern, line)
        if groups:
            g_dict = groups.groupdict()
            print(f'g_dict = ${g_dict}')
            cpu_parsed = parse_cpu(g_dict['CPU'].replace('"', ''))
            g_dict['CPU'] = cpu_parsed
            details.append(g_dict)
            print(f'cpu_parsed = ${cpu_parsed}')
    return details
    # print(f'results = ${res}')


def find_affinities():
    # <Server, <CPU, [APP, ID]>>

    affinities = defaultdict(lambda: defaultdict(list))
    uniq_apps_by_srvr = defaultdict(set)
    infos = find_affinities_infos()
    for info in infos:
        server = info['SERVER']
        cpus = info['CPU']
        app = info['APP']
        id = info['ID']
        cpu_info = affinities[server]
        uniq_apps_by_srvr[server].add(id)

        for cpu in cpus:
            cpu_info[cpu].append([app, id])
        # affinities[server]

    for server in affinities:
        # s_info = affinities[server]
        app_ids_curr_srvr = uniq_apps_by_srvr[server]
        # print(f'Server = {server}; Details= {s_info}')
        print(f'Server = {server:30}; app_ids_curr_srvr={app_ids_curr_srvr}')
        # print(f'Server = {server}; ')
    tmds = affinities['tilde-prod-mds-1']
    tmds_keys = sorted(list(tmds.keys()))
    print(f'tilde .... {tmds}')
    print(f'tilde .... {tmds_keys}')
    print(f'affinities={affinities}, uniq_apps_by_srvr={uniq_apps_by_srvr}')
    return affinities, uniq_apps_by_srvr


def write_to_wiki(markdowns, uniq_general_names):
    wiki = GhWiki(name='abhishek.pandey', password='grass123')
    for markdown_arr in markdowns:
        server = markdown_arr[0]
        if is_valid_server(server):
            md_text = '\n'.join(markdown_arr[1:])
            html = get_html(md_text)
            for name in uniq_general_names:
                replacement = IMG_REPLACEMENTS[name]
                html = html.replace(f'##{name}##',
                                    f'<img src="{replacement}" />')
            page_title = f'CPUs for {server}'
            print(f'page_title = {page_title}')

            print(f'Running for {page_title}; \n html={html}')
            results = wiki.update_or_create(_PARENT_PAGE, page_title, html)
            print(f' results = {results}')


def get_html(md_text):
    html = markdown.markdown(
        md_text,
        extensions=['markdown.extensions.extra', 'markdown.extensions.tables'])
    return html


def run_test():
    puppet_db = pypuppetdb.connect(PUPPET_HOST)
    puppet_node = puppet_db.node(f'{server}.grass.corp')
    node_processors = puppet_node.fact('processors')


def convert_affinities_to_md(affinities, uniq_apps_by_srvr, all_tags):
    uniq_servers = set([x.server for x in all_tags])
    print(f'alltags = {all_tags}; \n len tags={len(uniq_servers)}; '
          f' len affinities={len(affinities)}')
    server_markdowns = []
    uniq_general_names = set()
    puppet_db = pypuppetdb.connect(PUPPET_HOST)

    def find_tag(srvr, app_id):
        return next(x for x in all_tags
                    if x.server == srvr and x.app == app_id)

    def find_cpu(srvr, app_id, cpu_id):
        tag = find_tag(srvr, app_id)
        # print(f'ABHI: app_id={app_id}; tag={tag}; cpu_id={cpu_id}')
        if tag and cpu_id in tag.tags:
            return '<br />' + ','.join(tag.tags[cpu_id])

    for server in affinities:
        print('_' * 80)
        print(f'server = {server}')
        print('_*80')

        md = [server]
        count_node_processors = 0
        try:
            puppet_node = puppet_db.node(f'{server}.grass.corp')
            node_processors = puppet_node.fact('processors')
            count_node_processors = node_processors.value['count']
            print(f'server = {server}; processors={count_node_processors}')
        except Exception as e:
            print(f' MISSING SERVER details not found for {server}; {str(e)}')
            continue

        s_info = affinities[server]
        cpu_ids = list(sorted(s_info.keys()))
        missing_cpu_ids = [
            x for x in range(count_node_processors) if x not in cpu_ids
        ]
        app_ids_curr_srvr = list(sorted(uniq_apps_by_srvr[server]))
        # print(f'app_ids_curr_srvr = {app_ids_curr_srvr}')

        # time to start the "table"
        print(f' cpu_ids = {cpu_ids}; missing cpu id={missing_cpu_ids}')
        if missing_cpu_ids:
            missing_cpu_str = str(missing_cpu_ids)
            md.append(f'Missing CPU allocation for <strong>{missing_cpu_str}'
                      '</strong>')
            md.append('')
        # for cpu-2
        header = '| APP | ' + ' | '.join([str(x) for x in cpu_ids]) + '|'
        dashes = '| --' * (1 + len(cpu_ids)) + ' |'
        print(f'Server= {server:30} ; header = {header}; dashes={dashes}')
        md.append(header)
        md.append(dashes)
        for app_id in app_ids_curr_srvr:
            text = f'| {app_id} | '

            app_general_name = app_id.split('.')[1]
            uniq_general_names.add(app_general_name)
            for cpu_id in cpu_ids:
                tag = find_cpu(server, app_id, cpu_id)
                s_info_by_cpu = [x[1] for x in s_info[cpu_id]]
                print(f' cpu id = {cpu_id}; s_info_by_cpu={s_info_by_cpu}')
                text += f'##{app_general_name}## {tag} |' \
                        if app_id in s_info_by_cpu else ' |'
            md.append(text)
        server_markdowns.append(md)

    print(f'uniq_general_names={uniq_general_names}')
    write_to_wiki(server_markdowns, uniq_general_names)


def main(all_tags):
    affinities, uniq_apps_by_srvr = find_affinities()
    # convert_affinities_to_md(affinities, uniq_apps_by_srvr, all_tags, style=1)
    convert_affinities_to_md(affinities, uniq_apps_by_srvr, all_tags)


if __name__ == '__main__':
    run_test()
    # all_tags = tag_reader.get_all_tags(_DIR)
    # main(all_tags)
    # # srch()
    # print('results=done')
