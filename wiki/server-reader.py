"""
First version of config_reader.py
Not really good enough, but was something we started with.
"""

from PythonConfluenceAPI import ConfluenceAPI
import json

import pprint

def get_pretty_print_string(item):
    pp = pprint.PrettyPrinter(indent=4)
    return pp.pformat(item)
pp = lambda x: get_pretty_print_string(x)

_FILE='/home/abhishek.pandey/code/puppet/hieradata/nodes/jpxstage-ems02.grass.corp.json'
_WIKI_URL = 'http://wiki/'

class PageNotFound(Exception):
    pass

class GhWiki:
    def __init__(self, username, password):
        self.api = ConfluenceAPI('abhishek.pandey', 'grass123', _WIKI_URL)
        print('login successful')

    def _get_content_id(self, title):
        page_content = self.api.search_content('title="{}"'.format(title))['results']
        if not len(page_content):
            raise PageNotFound('Page not found by title: ' + title)
        cid = page_content[0]['id']
        return cid

    def get_page_content(self, page_title):
        content_id = self._get_content_id(page_title)
        page_json = self.api.get_content_by_id(
            content_id, 
            expand='body.storage,ancestors,version'
            )
        # print('page_json = {}\n'.format(pp(page_json)))
        return content_id, page_json

    def get_page_html(self, title):
        _, page_json = self.get_page_content(title)        
        return page_json['body']['storage']['value']
        
    def _modify_page_content(self, page_json):
        ancestors = page_json['ancestors']

        # somehow, you should keep only the last ancestor
        # last ancestor == immediate parent
        anc = ancestors[-1]
        del page_json['_links']
        del page_json['_expandable']
        del page_json['extensions']
        page_json['ancestors'] = [anc]
        page_json['version']['number'] = page_json['version']['number'] + 1
        

    def update_page_html(self, title, body, is_markdown=False):
        content_id, page_json = self.get_page_content(title)
        # body_text_to_use = body.format(page_json['version']['number'])
        self._modify_page_content(page_json)
        page_json['body']['storage']['value'] = body
        status = self.api.update_content_by_id(page_json, content_id)
        print('status = {}'.format(pp(status)))

def read_file():
    with open(_FILE, 'rt') as file:
        data = file.read()
        return data

def get_shields(json_d):
    instances = [i for i in json_d if i.endswith('::instances')]
    print(f'instances = ${instances}')
    for instance in instances:
        apps = json_d[instance]
        apps_shields = [v['shield'] for k,v in apps.items() if 'shield' in v]
        print(f'instance:: = ${instance}; apps_shields=${apps_shields}')
            
        

def main():
    data = read_file()
    # print(f'data= ${data}')
    json_d = json.loads(data)
    # print(f'lines=${json_d}')
    shields  = get_shields(json_d)

if __name__ == '__main__':
    main()
    print('results=done')

    wiki = GhWiki('abhishek.pandey', 'grass123')
    try:
        # wiki.get_page_content('TesterPage1')
        # wiki.update_page_html ('TesterPage1', '<p>Python code test V: 8</p>')
        body = wiki.get_page_html('TesterPage1')
        # print('body = {}'.format(body))
        

    except PageNotFound:
        print('TesterPage1')
