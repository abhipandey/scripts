from atlassian import Confluence
import markdown

import pprint

_WIKI_URL = 'http://wiki.grass.corp/'
_LABEL_TO_ADD = 'tech/automated'


class PageNotFound(Exception):
    pass


def get_pretty_print_string(item):
    pp = pprint.PrettyPrinter(indent=4)
    return pp.pformat(item)


pp = lambda x: get_pretty_print_string(x)


class GhWiki:
    def __init__(self, name, password, space='GHP'):
        self._name = name
        self._password = password
        self.space = space

        # https://atlassian-python-api.readthedocs.io/en/latest/confluence.html
        self.confluence = Confluence(url=_WIKI_URL,
                                     username=name,
                                     password=password)

    def get_id(self, title):
        return self.confluence.get_page_id(self.space, title)

    def get_page_id(self, title):

        print(f' looking for id for title {title}; space={self.space}')
        exists = self.confluence.page_exists(self.space, title)
        print(f'  exists = {exists}')
        page = self.get_page(title)
        print(f'page = {page}')
        page_id = self.confluence.get_page_id(self.space, title)
        return page_id

    def get_page(self, title):
        pages = self.confluence.get_page_by_title(self.space,
                                                  title,
                                                  start=None,
                                                  limit=None)
        return pages

    def get_parent_id_by_title(self, title):
        page_id = self.get_page_id(title)
        if page_id:
            parent_id = self.confluence.get_page_ancestors(page_id)[-1]['id']
            return parent_id
        return None

    def create_page(self,
                    title,
                    body,
                    parent_id=None,
                    type='page',
                    representation='storage'):
        results = self.confluence.create_page(self.space,
                                              title,
                                              body,
                                              parent_id=parent_id,
                                              type=type,
                                              representation=representation)
        print(f'create results={results}')
        return results

    def update_or_create(self, parent_title, title, body):
        parent_id = self.get_page_id(parent_title)
        return self.update_or_create_with_parent_id(parent_id, title, body)

    def update_or_create_with_parent_id(self, parent_id, title, body):
        # Update page or create page if it is not exists
        results = self.confluence.update_or_create(parent_id,
                                                   title,
                                                   body,
                                                   representation='storage')

        print(f'results of update or create: {results}')

        if 'id' in results:  # most likely succeeded
            page_id = results['id']
            self.confluence.set_page_label(page_id, _LABEL_TO_ADD)
        return results

    def update_page_html(self, title, body):
        parent_id = self.get_parent_id_by_title(title)
        page_id = self.get_page_id(title)
        print(f'parent_id = {parent_id}')

        update_results = self.confluence.update_page(parent_id, page_id, title,
                                                     body)
        print(f'update_results = {update_results}')


if __name__ == '__main__':
    wiki = GhWiki(username='abhishek.pandey', password='grass123')
    try:
        # wiki.get_page_content('TesterPage1')
        wiki.update_page_html('TesterPage1', '<p>Python code test V: 8</p>')
        body = wiki.get_page_html('TesterPage1')
        print('body = {}'.format(body))

    except PageNotFound:
        print('TesterPage1')
