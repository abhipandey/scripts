import markdown

# pip install markdown

md_str = '''
# this is a test

**At vero eos et accusamus**
et iusto _odio dignissimos_ ducimus qui blanditiis


> praesentium voluptatum deleniti atque corrupti quos


Here's some code


    dolores et quas molestias excepturi sint occaecati 


Did that show up?



```
html = markdown.markdown(your_text_string)

```
'''

html = markdown.markdown(md_str)
print('html = {}'.format(html))