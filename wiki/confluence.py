# pip install PythonConfluenceAPI ---> only for python 3.3+
# http://wiki/rest/api/content/1771401?expand=body.storage
from PythonConfluenceAPI import ConfluenceAPI
import pprint


def get_pretty_print_string(item):
    pp = pprint.PrettyPrinter(indent=4)
    return pp.pformat(item)


pp = lambda x: get_pretty_print_string(x)

# Create API object.
api = ConfluenceAPI('abhishek.pandey', 'grass123', 'http://wiki/')
# _TITLE = 'Git Cookbook'
_TITLE = 'TesterPage1'


def _get_content_id(title):
    page_content = api.search_content('title="{}"'.format(title))
    cid = page_content['results'][0]['id']
    return cid


def update_content(title, body):
    content_id = _get_content_id(title)
    page_json = api.get_content_by_id(
        content_id, expand='body.storage,ancestors,version')

    body_text_to_use = body.format(page_json['version']['number'])
    print('page_json = {}\n'.format(pp(page_json)))

    ancestors = page_json['ancestors']
    print('ancestors = {}'.format(ancestors))

    # somehow, you should keep only the last ancestor
    # last ancestor == immediate parent
    anc = ancestors[-1]
    del page_json['_links']
    del page_json['_expandable']
    del page_json['extensions']
    page_json['ancestors'] = [anc]
    page_json['version']['number'] = page_json['version']['number'] + 1
    page_json['body']['storage']['value'] = body_text_to_use

    status = api.update_content_by_id(page_json, content_id)
    print('status = {}'.format(pp(status)))


new_text = '<p>Python code test V: {}</p>'

# print('content_id={}'.format(content_id))

# update_content(_TITLE, new_text)


def test_me():
    title = 'Dev'
    # res = api.search_content('space=GHP and title="{}"'.format(title))
    # res = api.search_content('space=GHP and title="{}"'.format(title))
    # res = api.search_content('(title=Dev AND space="/rest/api/space/GHP")')
    res = api.search_content('(title~"Credentials Dev")')
    print(pp(res))


# spaces = api.get_spaces()
# print('spaces = {}'.format(pp(spaces)))
test_me()
