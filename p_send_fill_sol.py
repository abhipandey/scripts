#!/opt/py27env/bin/python2.7



from base_pygh import *
rom google.protobuf.text_format import Merge
from ems_pb2 import Fill
from gh.solace import SimpleApi

text = '''fill_id: "20150720-0"
seq_number: 0
source_app_id: "a/b/c"
client_order_id: "0"
created_at { date: 20150720 time { time: 121212 nanosecond: 0 } }
instrument_id { type: INSTRUMENT_ID_TYPE_NATIVE key: "FUTURE|XOSE|FUT_NK225_1509|1509" }
order_book_id: "uatCheewah.ne"
trade_price: "20650"
trade_quantity_long: 93
'''

fill = Fill()

Merge(text, fill)

api = SimpleApi() # connects to dev
api.publish_msg('ems/v0/pb/pub/a/b/c/fill', fill.SerializeToString())
api.close()
