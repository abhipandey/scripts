#!/usr/bin/python3.5
from itertools import groupby

def read_text():
    with open('git.cmd', 'rt') as f:
        text = f.readlines()
        print('ABHI: text = {}'.format(text))
        return text





def process_lines():
    lines = read_text()
    cmds = []
    for l in lines:
        abbrev = l[2:23].strip()
        stripped_sec = l[25:].rstrip('\n').rstrip('|').strip()
        print('abbrev = {} | {}'.format(abbrev, stripped_sec))
        git_type = 'misc'
        if stripped_sec.startswith('git'):
            parts = stripped_sec.split(' ', 2)
            print('parts = {}'.format(parts))
            git_type = parts[1]
            long_cmd = parts[2] if len(parts) > 2 else ''
        else:
            git_type = 'misc'
            long_cmd = stripped_sec
        cmds.append((git_type, abbrev, long_cmd))
    return cmds


def process_cmds():
    cmds = process_lines()
    grps = dict()

    for gittype, groups in \
            groupby(
                sorted(cmds, key=lambda x: x[0]),
                key=lambda x: x[0]):
        print('*'*80)
        print('gitype = {}'.format(gittype))
        filtered = [(x[1], x[2]) for x in groups]
        grps[gittype] = filtered
        print('groups = {}'.format(filtered))
    return grps

_TABLE_BEGIN = '''

<div class="w3-third" style="border:0px solid black;">
  <h2>{}</h2>

'''
_ROW = '''
        <tr>
            <td width="100px" style="font-family: 'Courier New', Courier, monospace; font-weight:bold;text-align:right;">{ABBREV}</td>
            <td style="font-family: 'Courier New', Courier, monospace;">{LONG_CMD}</td>
        </tr>
'''

_START = '''

<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">


  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>

<div class="w3-container w3-orange">
  <h1>Git Shortcuts</h1>
  <p>Cheat Sheet</p>
</div>

<div class="w3-row-padding">


'''


_END = '''
</div>

</body>
</html>
'''
def process_groups():
    final = ''
    print('***'*80)
    print('***'*80)
    print('***'*80)
    grps = process_cmds()
    for x in sorted(grps):
        gittype, values = x, grps[x]
        # print('gittype={}; values = {};'.format(gittype, values))
        if gittype != 'misc':
            cmd = 'git ' + gittype
        else:
            cmd = 'MISC'
        text = _TABLE_BEGIN.format(cmd)
        # text += '    <table class="table table-bordered table-condensed">\n'
        text += '    <table class="table table-bordered">\n'
        for tpl in sorted(values, key=lambda x:x[1]):
            text += _ROW.format(
                ABBREV=tpl[0],
                LONG_CMD=tpl[1]
            )
        text += '    </table>\n'
        text += '</div>\n'
        # print('=='*80)
        # print('text  = {}'.format(text))
        final += text
        print(final)
    with open('/tmp/final.html', 'wt') as f:
        f.write(_START + final + _END)

process_groups()
