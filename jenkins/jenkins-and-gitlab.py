from jenkinsapi.jenkins import Jenkins

from build_params import BuildParams
import re

# http://jenkins2.grass.corp/job/higgs/job/xraider-configs/293/

# import pprint.info
import pprint
import logging
from logging import handlers

import gitlab_client

logger = logging.getLogger(__name__)

jenkins_server = 'https://prod.jenkins.ghpr.asia'
jenkins_password = '118eca6ce88336e1260f53cca322686ac6'
jenkins_user = 'jenkins.robot'


def setup_logging():
    """
    Set up handlers, formatters and loggers
    """
    # We configure the root logger with handlers and rely on all other
    # loggers propagating events to it
    root = logging.getLogger()
    root.level = logging.DEBUG

    log_format = "%(asctime)s-%(levelname)s[%(threadName)s]|%(name)s|: %(funcName)s:%(lineno)d %(message)s"
    screen_format = "%(asctime)s - %(levelname)s, %(lineno)d: %(message)s"
    summary_log_filename = "summary.log"
    detail_log_filename = "debug.log"

    # Info logs to screen and to summary file
    screen_fmt = logging.Formatter(screen_format)
    stream_handler = logging.StreamHandler()
    stream_handler.formatter = screen_fmt
    stream_handler.level = logging.INFO
    root.addHandler(stream_handler)

    summary_file_handler = logging.handlers.RotatingFileHandler(
        summary_log_filename, maxBytes=200000, backupCount=5, encoding="utf-8"
    )
    summary_file_handler.setFormatter(logging.Formatter(log_format))
    summary_file_handler.setLevel(logging.INFO)
    root.addHandler(summary_file_handler)

    # Push debug level logs to the rotating debug log file
    rotating_handler = logging.handlers.RotatingFileHandler(
        detail_log_filename, maxBytes=250000, backupCount=5, encoding="utf8"
    )
    rotating_handler.setFormatter(logging.Formatter(log_format))
    rotating_handler.setLevel(logging.DEBUG)
    root.addHandler(rotating_handler)


def get_build_info(build, build_number):
    info = {'ID': build_number}
    info['BRANCH'] = build.get_params()['branch']
    info['STATUS'] = build.get_status()
    info['COMMIT'] = build.get_revision()
    info['IS_RUNNING'] = build.is_running()
    info['BUILD'] = build
    return info


def run_jenkins_job(app, tag):
    jenkins = Jenkins(
        jenkins_server, username=jenkins_user, password=jenkins_password,
    )
    higgs_app_job = jenkins.get_job(f'higgs-{app}')
    queue_item = higgs_app_job.invoke(build_params={'branch': tag})
    logger.info(f'QueueItem for invoking build: {queue_item}')
    logger.info('Waiting until build starts')
    queue_item.block_until_building()
    build = queue_item.get_build()
    build_num = queue_item.get_build_number()
    build_info = get_build_info(build, build_num)
    logger.info(f'QueueItem: Build # = {build_num}: {build_info}')
    logger.info('Canceled build')
    build.stop()
    return queue_item


def get_mr_id(description):
    pattern = 'Triggered by GitLab Merge Request #([\d]+):'
    res = re.search(pattern, description)
    if res:
        return res.group(1)


def _get_news_files(params: BuildParams):
    return [NewsReader(x) for x in params.files if x.endswith('NEWS.md')]


class NewsReader:
    _FILE_PATTERN = re.compile(r'ccgh\/(.*)\/NEws.md', re.IGNORECASE)
    VersionPattern = re.compile(r'# +(.*) *')

    def __init__(self, path):
        self.file = path
        self.version = None
        with open(path, 'rt') as file_to_read:
            self.lines = file_to_read.readlines()[0:100]

    def is_a_release(self):
        begin = False
        for line in self.lines:
            print(f'line = {line}')
            line = line.strip()
            if not line:
                continue
            if begin:
                # i can either have a line with text, or another release
                if line.startswith('# '):  # this is a release
                    self.version = re.search(
                        NewsReader.VersionPattern, line
                    ).group(1)
                    return True
                return False
            if line == '# Next':
                begin = True

    def __str__(self):
        return 'NewsReader: <%s>; isRelease? %s; App: %s; Version: %s' % (
            self.file,
            self.is_a_release(),
            self.app,
            self.version,
        )

    @property
    def app(self):
        return re.search(NewsReader._FILE_PATTERN, self.file).group(1)


def main():
    project_id = 208
    params = BuildParams()
    print(f'params = {params}')
    setup_logging()

    # Is this commit of interest?
    if not params.is_tagged_build():
        # only then it makes sense to go ahead
        logger.info('Not a tagged build. Ignoring')
        return
    logger.info('This is a tagged build')
    news_files = _get_news_files(params)
    for file in news_files:
        logger.info(f'file = %s' % file)

    # We only want to release aqua for now
    aqua_file_info = [f for f in news_files if f.app == 'aqua']
    if not aqua_file_info:
        logger.info('News files exist, but none of them are aqua. Returning')
        return
    if not aqua_file_info[0].version:
        logger.info('Aqua news with no version. Returning')
        logger.info(
            'First 10 lines of aqua news=%s'
            % '\n'.join(aqua_file_info[0].lines[:10])
        )
        return
    aqua_version = aqua_file_info[0].version
    app = aqua_file_info[0].app
    commit = params.commit_after

    logger.info(f'AQUA Version: %s; commit=%s' % (aqua_version, commit))

    # TODO(abhi) Eventually use the correct tag
    full_tag_name = f'{app}-A.{aqua_version}'

    tag_result = gitlab_client.add_project_tag(project_id, full_tag_name)
    logger.info(f'tag_result = {tag_result}')
    if tag_result:
        run_jenkins_job(app, full_tag_name)

    # TODO(abhi) Dont need this eventually
    gitlab_client.remove_project_tag(project_id, full_tag_name)


if __name__ == '__main__':
    main()
