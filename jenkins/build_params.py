import yaml
import json

import json, ast, datetime
from datetime import timedelta
import subprocess
import os
import logging
import logging.handlers
import pprint
from typing import List, Sequence


logger = logging.getLogger(__name__)


DEFAULT_TIMEOUT = 300  # in seconds


class BuildParams:
    """
    Every Jenkins build contains some environment variables with which
    it starts up. This class collates some of them which are useful.
    We put the meaningful variables here, in a single class, so that:
    * It is easy to see what variables are really in use
    * It is easy to pass around, if needed

    Please note that this only works because of Trigger pipelines.
    The pipeline triggers other pipelines that have been setup, while
        adding more parameters. That is the assumption being made here.
    """

    def __init__(self):
        self._src_branch = os.environ.get(
            "gitlabSourceBranch"
        ) or os.environ.get("gitlabBranch")

        self._action_type = os.environ.get("gitlabActionType")

        if self._action_type == "PUSH":
            self._commit_before = os.environ.get("gitlabBefore")

            # happens when UI is used to create  updates
            if self._commit_before == 40 * "0":
                self._commit_before = "origin/master"
            self._commit_after = os.environ.get("gitlabAfter")
        else:
            self._commit_before = "origin/master"
            self._commit_after = os.environ.get("GIT_COMMIT")

        self._files = self._get_modified_files()
        # Action=MERGE; src branch=modify-harvester-jpxstage-ems02; commit_before=origin/master, commit_after=c56fc12ddcae2e5cb2c20c00d0e5fbeac62d2b3f; files=['templates/jpxstage-ems02/harvester.MN1909.test.erb']
        '''
        JobParams: Action=PUSH; src branch=master; commit_before=3465fd02a4cb491bc66e0dc12e96f37f66151610, commit_after=4772f8ea051a7a7ceba39247b3c9e6625db00376; files=['news/news.JPX.section', 'news/news.JPX_FastQuote.section', 'news/news.JPX_JGB.section', 'news/news.JPX_Xfade.section', 'news/news.JPX_Xfade_big.section']
        JobParams: Action=PUSH; src branch=edwin-nfp; commit_before=56edcc4d52db0ba246a0e3e00db471545b798aec, commit_after=6bbea72edc2b71db5f8cd12fad6d455c38f67c55; files=['news/news.JPX_Xfade_big.section']

        '''
        # ------------

        # self._action_type = 'MERGE'
        # self._src_branch = 'modify-harvester-jpxstage-ems02'
        # self._commit_before = 'origin/master'
        # self._commit_after = 'c56fc12ddcae2e5cb2c20c00d0e5fbeac62d2b3f'
        # self._files = ['templates/jpxstage-ems02/harvester.MN1909.test.erb']

        # ------------
        # TODO(abhi) stop using this eventually
        self._action_type = 'PUSH'
        self._src_branch = 'master'
        self._commit_before = 'origin/master'
        self._commit_after = '0a5cfeffbbdb313b23f55ba70e76fd74e2e81058'
        self._files = [
            'ccgh/aqua/NEWS.md',
            'ccgh/fish/NEWS.md',
            './ccgh/babel/NEWS.md',
        ]

        logger.info(
            "BuildParams: Action=%s; src branch=%s; commit_before=%s, "
            "commit_after=%s; files=%s; is_tagged_build=%s",
            self._action_type,
            self._src_branch,
            self._commit_before,
            self._commit_after,
            self._files,
            self.is_tagged_build(),
        )

    @property
    def action_type(self) -> str:
        """
        What was the gitlabActionType reported by Jenkings for the build?

        Possible values = PUSH/MERGE
        """
        return self._action_type

    @property
    def src_branch(self) -> str:
        """
        What was the source branch reported by Jenkins for the build?
        """
        return self._src_branch

    @property
    def commit_before(self) -> str:
        """
        The commit or tag just before the build started.
        """
        return self._commit_before

    @property
    def commit_after(self) -> str:
        """
        The latest commit that this build contains
        """
        return self._commit_after

    @property
    def files(self) -> List[str]:
        """
        List of files that have been modified/updated through the latest build
        """
        return self._files

    def does_commit_have_news_files(self):
        return any([f for f in self._files if f.endswith('NEWS.md')])

    def does_commit_have_code_files(self):
        return any([f for f in self._files if not f.endswith('.md')])

    def is_tagged_build(self):
        '''
        What do i need for a tagged build?
        - Shuold be master
        - Should have news.md update
        - action should be push
        '''
        return (
            self.action_type == 'PUSH'
            and self.src_branch == 'master'
            and self.does_commit_have_news_files()
        )

    def _get_modified_files(self) -> List[str]:
        """
        Provides a list of all the files that have been modified/updated
        through the latest build.
        """
        current_pwd = os.getcwd()
        current_branch = get_cmd_results(["git", "branch"])

        if not self._action_type:
            return []

        cmd = [
            "/usr/bin/git",
            "diff",
            "--name-only",
            f"{self._commit_before}...{self._commit_after}",
        ]
        file_lines = get_cmd_results(cmd).splitlines()

        return file_lines


def get_cmd_results(cmd: Sequence[str]) -> str:
    """
    Runs a command, provides the results as Pipes
    """
    return run_cmd(cmd).stdout.decode("utf-8").strip()


def run_cmd(
    cmd: Sequence[str], timeout: int = DEFAULT_TIMEOUT
) -> subprocess.CompletedProcess:
    """"
    Run command in subprocess and return job output

    :param cmd: The command to run
    :param timeout: Command timeout, in seconds
    """
    logger.debug("Running %s", cmd)
    try:
        job = subprocess.run(
            args=cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            timeout=timeout,
        )
    except subprocess.CalledProcessError:
        logger.exception("Hit error running cmd:")
        raise
    except subprocess.TimeoutExpired:
        logger.exception("Timed out waiting for cmd:")
        raise

    logger.debug(
        "RC: %s, output: %s", job.returncode, job.stdout.decode("utf-8")
    )

    return job
