#!/usr/bin/env python
# coding: utf-8
from __future__ import absolute_import
import re
import logging
import pprint
import base64
from itertools import chain
import utils

import requests
from gitlab import Gitlab

import os
import logging


GITLAB_URL = os.getenv('GITLAB_URL') or 'http://example.com'
GITLAB_TOKEN = os.getenv('GITLAB_TOKEN')
# GITLAB_USERNAME = os.getenv('GITLAB_USERNAME')
# GITLAB_PASSWORD = os.getenv('GITLAB_PASSWORD')
SLACK_TOKEN = os.getenv('SLACK_TOKEN')
# CONFIG_PATH = '.mention-bot'

# def get_default_config_yaml():
#     file_path = 'sample-protected-list.yaml'
#     from utils import load_yaml_from_file

#     return load_yaml_from_file(file_path)


# get_default_config = get_default_config_yaml


logger = logging.getLogger(__name__)

session = requests.Session()
_gitlab_client = None


def check_config():
    if not GITLAB_URL:
        logger.error(
            "No Gitlab address detected, please expose GITLAB_URL as environment variables."
        )
        exit(1)

    if not GITLAB_TOKEN:
        logger.error(
            "goto Profile Settings -> Access Token -> Create Personal Access Token"
        )
        logger.error("append GITLAB_TOKEN= before start command.")
    else:
        logger.info('GITLAB_TOKEN = {}'.format(GITLAB_TOKEN))
    if not SLACK_TOKEN:
        logger.error("Create a valid slack token first.")
        logger.error("append SLACK_TOKEN= before start command.")
        exit(1)


class GitlabError(Exception):
    pass


class ConfigSyntaxError(Exception):
    pass


# def get_pretty_print(item, depth=None):
#     pp = pprint.PrettyPrinter(indent=4, depth=depth)
#     return pp.pformat(item)


# PP = lambda x: get_pretty_print(x)


# def get_channels_based_on_labels(cfg, labels):
#     labels = labels or ['']
#     logger.info(
#         'labelNotifications = {}\n labels={}'.format(
#             cfg.labelNotifications, labels
#         )
#     )
#     channels = list(
#         set([y for x, y in cfg.labelNotifications.items() if x in labels])
#     )
#     return channels


# def get_payload_labels(payload):
#     return (
#         [l[u'title'] for l in payload[u'labels']]
#         if u'labels' in payload
#         else []
#     )


#
# def get_labels(cfg, files):
#     labels_to_add = []
#     files_to_use = [x[0] for x in files]
#     for file in files_to_use:
#         for path, label in cfg.labels.items():
#             if file.startswith(path):
#                 labels_to_add.append(label)
#     return sorted([x for x in list(set(labels_to_add))])
#


def get_gitlab_client():
    global _gitlab_client
    if _gitlab_client is None:
        _gitlab_client = Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
        _gitlab_client.auth()
    return _gitlab_client


def get_project(project_id):
    client = get_gitlab_client()
    logger.info('project_id= {}'.format(project_id))
    project = client.projects.get(project_id)
    return project


def get_project_tags(project_id):
    client = get_gitlab_client()
    logger.info('project_id= {}'.format(project_id))
    project = client.projects.get(project_id)
    tags = project.tags.list()

    return tags


def add_project_tag(project_id, tag_info: str):
    client = get_gitlab_client()
    logger.info('project_id= {}'.format(project_id))
    project = client.projects.get(project_id)
    tag_exists = project.tags.list(search=tag_info)
    if tag_exists:
        logger.info(f'Tag <{tag_exists} aleady exists. not creating')
        return
    else:
        logger.info(f'No Existing tag: {tag_info}')
    tag_result = project.tags.create(
        {'tag_name': tag_info, 'ref': 'master', 'message': 'Automated tag'}
    )
    return tag_result


def remove_project_tag(project_id, tag_info: str):
    client = get_gitlab_client()
    logger.info('project_id= {}'.format(project_id))
    project = client.projects.get(project_id)
    removd_tag = project.tags.delete(tag_info)
    logger.info(f'removd_tag = {removd_tag}')
    return removd_tag


# def get_all_projects():
#     client = get_gitlab_client()
#     return client.projects.list(all=True)


# def get_merge_request(project_id, merge_request_id):
#     project = get_project(project_id)
#     mr = project.mergerequests.get(merge_request_id)
#     return mr


# def add_comment_merge_request(project_id, merge_request_id, note):
#     merge_request = get_merge_request(project_id, merge_request_id)
#     res = merge_request.notes.create({u'body': note})
#     return res.attributes


# def get_active_users():
#     client = get_gitlab_client()
#     return [
#         u.attributes[u'username']
#         for u in client.users.list(all=True)
#         if u.attributes[u'state'] == u'active'
#     ]


# def get_blocked_users():
#     client = get_gitlab_client()
#     # for u in client.users.list(all=True):
#     #     logger.info('custom users = {}'.format(u.attributes[u'username']))
#     blocked_users = [
#         u.attributes[u'username']
#         for u in client.users.list(all=True)
#         if u.attributes[u'state'] != u'active'
#     ]

#     return blocked_users


# def get_user(id):
#     client = get_gitlab_client()
#     return client.users.get(id).attributes


# def update_labels(project_id, merge_request_id, labels_list):
#     merge_request = get_merge_request(project_id, merge_request_id)
#     merge_request.labels = labels_list
#     merge_request.save()
#     # client = get_gitlab_client()
#     # res = client.updatemergerequest(
#     #     project_id, merge_request_id, labels=labels)
#     # logger.info('merge_request= {}; dir={}'.format(res, dir(res)))
#     # return 'kk'


# def get_merge_request_plain_changes(project_id, merge_request_id):
#     mr = get_merge_request(project_id, merge_request_id)
#     commits = mr.commits()
#     changes = ''
#     for d in commits:
#         # logger.info('commit = {}'.format(d.diff()))
#         ch = '\n'.join([x[u'diff'] for x in d.diff()])
#         changes += ch
#     return changes


# def get_merge_request_diff(project_id, merge_request_id):
#     mr = get_merge_request(project_id, merge_request_id)
#     diffs = mr.diffs.list(all=True)
#     uniq_diffs = set()
#     changes = []
#     for d in diffs:
#         diff = mr.diffs.get(d.attributes[u'id'])
#         if diff.attributes[u'base_commit_sha'] not in uniq_diffs:
#             uniq_diffs.add(diff.attributes[u'base_commit_sha'])
#             logger.info('adding for ' + diff.attributes[u'base_commit_sha'])
#             changes.append(diff.attributes[u'diffs'])
#         # logger.info('diff = {}'.format(diff.attributes[u'diffs'][u'diff']))
#     changes = list(chain(*changes))
#     logger.info('changes = {}'.format(changes))
#     return changes


# # def get_merge_request_diff(project_id, merge_request_id):
# #     client = get_gitlab_client()
# #     changes = client.getmergerequestchanges(project_id, merge_request_id)
# #     return changes['changes']


# def has_mention_comment(project_id, merge_request_id, comment):
#     merge_request = get_merge_request(project_id, merge_request_id)
#     notes = merge_request.notes.list(all=True)
#     discussions = [n.attributes[u'body'] for n in notes]
#     return comment in discussions


# def get_project_file(project_id, branch, path):
#     project = get_project(project_id)
#     f = project.files.get(file_path=path, ref=branch)
#     content = base64.b64decode(f.attributes[u'content'])
#     return content.decode('utf-8')


# # def get_project_file(project_id, branch, path):
# #     client = get_gitlab_client()
# #     return client.getrawfile(project_id, branch, path)


# def _search_authenticity_token(html):
#     matched = re.search(
#         r'<input type="hidden" name="authenticity_token" value="(.*)" />', html
#     )
#     if not matched:
#         raise GitlabError("Fetch login page failed.")
#     return matched.group(1)


# def login():
#     login_url = GITLAB_URL + '/users/auth/ldapmain/callback'
#     login_page = GITLAB_URL + '/users/sign_in'
#     headers = {
#         'Origin': GITLAB_URL,
#         'Referer': login_page,
#     }
#     response = session.get(login_url, headers=headers)
#     authenticity_token = _search_authenticity_token(response.text)
#     data = {
#         'username': GITLAB_USERNAME,
#         'password': GITLAB_PASSWORD,
#         'authenticity_token': authenticity_token,
#         'utf8': u'√',
#     }
#     return session.post(
#         login_url, headers=headers, data=data, allow_redirects=False
#     )


# def setup_cookie():
#     if session.cookies is None or len(session.cookies) == 0:
#         login()


# def fetch_blame(namespace, target_branch, path):
#     setup_cookie()
#     try:
#         url = '%s/%s/blame/%s/%s' % (
#             GITLAB_URL,
#             namespace,
#             target_branch,
#             path,
#         )
#         logger.info('fetch_blame: locals={}'.format(locals()))
#         response = session.get(url)
#         response.raise_for_status()
#     except requests.HTTPError:
#         logger.warning("Fetch %s blame failed.".format(url))
#     return response.text


# def get_protected_branches(project_id):
#     protected_branches_str = get_project_file(
#         project_id, 'master', '.protected-branches'
#     )
#     return utils.load_yaml_from_str(protected_branches_str)['branch_list']
