import os, sys
import re
import argparse

_FILE = 'NEWS.md'
_PATTERN_BEGIN = r'^# (?!Next).* *$'
_PATTERN_END = r'^# ({}) *$'

parser = argparse.ArgumentParser()

parser.add_argument('-v', action="store", default="0.410", required=True)


def get_lines(filepath):
    with open(filepath, 'rt') as file:
        lines = file.readlines()
        return lines


def go():
    args = parser.parse_args()
    pattern_end = _PATTERN_END.format(args.v)

    print(f'args = {args}')
    continue_to_add_lines = False
    parsed_news = []
    lines = get_lines(_FILE)
    for l in lines:
        if re.search(_PATTERN_BEGIN, l):
            continue_to_add_lines = True
        if re.search(pattern_end, l):
            break
        if continue_to_add_lines:
            parsed_news.append(l)
    full_news = ''.join(parsed_news)
    print(f'{full_news}')


if __name__ == "__main__":
    go()
