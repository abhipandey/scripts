import re
from errbot import BotPlugin, botcmd, re_botcmd
import time


class Example(BotPlugin):
    """
    This is a very basic plugin to try out your new installation and get you started.
    Feel free to tweak me to experiment with Errbot.
    You can find me in your init directory in the subdirectory plugins.
    """


    # According to this example, wfh can occur anywhere in a line
    # and this function gets triggered.
    @re_botcmd(pattern=r"(^| )wfh?( |$)", prefixed=False, flags=re.IGNORECASE)
    def listen_for_talk_of_wfh(self, msg, match):
        """Note down when somebody talks about WFH..."""
        # self.send(msg.frm, '_Noted_')  # disabled for now
        return
