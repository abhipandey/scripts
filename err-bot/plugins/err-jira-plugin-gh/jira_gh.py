import re
from errbot import BotPlugin, botcmd, re_botcmd
import time

from jira import JIRA


class JiraBot(BotPlugin):
    """
    JIRA plugin that will help you search on Grasshopper JIRA.
    """

    def activate(self):
        super().activate()
        username, ldp_password = 'gitlab', 'gitlab'
        options = {
            'server': 'http://jira/',
            'verify': True,
            'basic_auth': (username, ldp_password),
        }
        self.jira = JIRA(options=options, basic_auth=(username, ldp_password))

    @botcmd  # flags a command
    def jira_search(self, msg, args):
        """
        **Find JIRA tickets** based on the summary or DIY advanced query.

        Know more about advanced search at: https://docs.atlassian.com/jira/jsw-docs-074/Advanced+searching

        Some examples are:
        `!jira search google-api-python-client`

        `!jira search summary ~  "gitlab" and status = Open`
        """
        print('msg = {}; args={}; type={}'.format(msg, args, type(args)))
        if '~' in args or '=' in args:
            query = self.jira.search_issues(jql_str=args)
        else:
            query = self.jira.search_issues(jql_str='summary ~ {}'.format(args))

        if query:
            for issue in query:
                self.send(msg.frm,
                          text=
                          '{}: {} {}'.format(
                              issue.key,
                              issue.fields.summary,
                              issue.permalink(),
                          ),
                          in_reply_to=msg)
        else:
            self.send(msg.frm, '_No tickets found_', in_reply_to=msg)
