import re
from errbot import BotPlugin, botcmd, re_botcmd
import time
import logging

from PythonConfluenceAPI import ConfluenceAPI

_WIKI_URL = 'http://wiki'


class ConfluenceBot(BotPlugin):
    """
    Confluence plugin that will help you search on Grasshopper wiki.
    """

    def activate(self):
        super().activate()
        self.wiki = ConfluenceAPI('robot', 'beepbeepskynet', _WIKI_URL)

    @botcmd  # flags a command
    def wiki_search(self, msg, args):
        """
        **Find WIKI pages** based on the page title or DIY advanced query.

        Know more about using general search: https://confluence.atlassian.com/doc/confluence-search-syntax-158720.html
        Advanced search strategies: https://developer.atlassian.com/server/confluence/advanced-searching-using-cql/

        Some examples are:
        `!wiki search dev`

        `!wiki search title ~ "dev credentials"`
        """
        print('msg = {}; args={}; type={}'.format(msg, args, type(args)))
        if '~' in args or '=' in args:
            results = self.wiki.search_content(args)
        else:
            if ' ' in args and '"' not in args:
                args = '"{}"'.format(args)
            results = self.wiki.search_content('(title~{})'.format(args))

        if results['results']:
            for res in results['results']:
                if 'title' in res and '_links' in res:
                    self.send(msg.frm,
                              '{}: {}{}'.format(res['title'], _WIKI_URL,
                                                res['_links']['webui'])
                              , in_reply_to=msg)
        else:
            self.send(msg.frm, '_No pages found_', in_reply_to=msg)