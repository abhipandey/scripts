import logging

# This is a minimal configuration to get you started with the Text mode.
# If you want to connect Errbot to chat services, checkout
# the options in the more complete config-template.py from here:
# https://raw.githubusercontent.com/errbotio/errbot/master/errbot/config-template.py

BACKEND = 'Slack'  # Errbot will start in text mode (console only mode) and will answer commands from there.

BOT_DATA_DIR = r'/home/abhishek.pandey/apps/code/scripts/err-bot/data'
BOT_EXTRA_PLUGIN_DIR = r'/home/abhishek.pandey/apps/code/scripts/err-bot/plugins'

BOT_LOG_FILE = r'/home/abhishek.pandey/apps/code/scripts/err-bot/errbot.log'
BOT_LOG_LEVEL = logging.DEBUG

BOT_ADMINS = ('@abhi', )  # !! Don't leave that to "@CHANGE_ME" if you connect your errbot to a chat system !!

# harry
# xoxb-320700588132-zzW67MxhY4UaQkAyqgZjo9iy
BOT_IDENTITY = {
    'token': 'xoxb-320700588132-zzW67MxhY4UaQkAyqgZjo9iy',
}

BOT_ALT_PREFIXES = ('@harry',)

# If you're using a bot account you should set CHATROOM_PRESENCE = (). 
# Bot accounts on Slack are not allowed to join/leave channels on their own 
# (they must be invited by a user instead) so having any rooms setup in 
# CHATROOM_PRESENCE will result in an error.
CHATROOM_PRESENCE = ()

