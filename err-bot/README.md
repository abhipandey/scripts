


[List of all errbot plugins](https://github.com/errbotio/errbot/wiki)
[Errbot-jira](https://github.com/cwjohnston/err-jira)
[Installing plugins](http://errbot.io/en/latest/user_guide/administration.html#installing-plugins)



[bot commands - similar to what I need](http://errbot.io/en/latest/user_guide/plugin_development/botcommands.html)

[Trigger a callback with every message](http://errbot.io/en/latest/user_guide/plugin_development/messaging.html)

[Dynamic plugins](http://errbot.io/en/latest/user_guide/plugin_development/dynaplugs.html)



### Some great features that I noticed

This is the [same link](http://errbot.io/en/latest/user_guide/plugin_development/messaging.html) as before, but still worth repeating.

- Templating
- Cards
- Return multiple responses
- Sending a message to a specific user or room
- [Without a bot prefix, trigger commands even when no bot is specified](http://errbot.io/en/latest/user_guide/plugin_development/botcommands.html#without-a-bot-prefix)





### Good JIRA projects


https://github.com/sijis/err-jira/blob/master/jira.py


Lot of matches stuff in there. Not sure how this is different.
https://github.com/cwjohnston/err-jira/blob/master/jira.py


Simple implementation
https://github.com/jvasallo/err-plugins/blob/master/err-jira/jira.py






---------------


## Setting it up

```
pip install --ignore-requires-python PythonConfluenceAPI
```



## Additional task:


Had to delete the following lines from `/home/abhishek.pandey/apps/code/scripts/err-bot/ve/lib/python3.6/site-packages/PythonConfluenceAPI/__init__.py` file:


```python
from future import standard_library
standard_library.install_aliases()
```


