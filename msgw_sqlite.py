#!/opt/py27env/bin/python2.7



import sqlite3
import ast
import re
from collections import namedtuple

_DB_FILE = 'cheewah.boboxses-t01.sqlite3.db'
_SPLITTER = '.*'
_JSON_FMTS = [
    'key: "(?P<SYM>[|\w]+)',
    'order_book_id: "(?P<ORDERBOOK>[\w\.]+)',
    'position_long: (?P<LONG>-?[0-9]+)',
    'position_short: (?P<SHORT>-?[0-9]+)'
    ]
_FMT = _SPLITTER.join(_JSON_FMTS)

conn = sqlite3.connect(_DB_FILE)
c = conn.cursor()
rows = c.execute("SELECT * FROM position")

compressed_positions = []
for row in rows:
    orderbook, instrument = str(row[0]).split('|', 1)
    json = str(row[1])
    # print('json = {}'.format(json))
    # print('json = {} : {}'.format(type(json), json))
    # print('OB: {:30}: instrument: {:30} Position: {}'.format(
    #     orderbook, instrument, json
    # ))

    groups = re.search(_FMT, json, re.MULTILINE | re.DOTALL | re.M)

    print('groups = {}'.format(groups.groupdict()))
    d = groups.groupdict()
    p = namedtuple('PositionDetails', d.keys())(**d)
    print('p = {}'.format(p.SYM))
    compressed_positions.append(p)

print('mysql_positions = {}'.format(compressed_positions))


