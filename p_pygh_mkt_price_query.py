#!/opt/py27env/bin/python2.7
from base_pygh import *

from django.conf import settings

import core_pb2
from gh import solace
from pyghweb.pricesvc.requests import TradePrice

api = solace.SimpleApi.make_from_django_settings(settings)
trade_price = TradePrice(api) # this gets from pricesvc

symbols = ['STOCK|TSE|1320|', 'FUTURE|OSE|NIKKEI225|1409', 'FUTURE|OSE|NIKKEI225|1412']
for symbol in symbols:
    resp = trade_price.request(symbol, core_pb2.INSTRUMENT_ID_TYPE_GHW)
    if resp.return_code == 0:
        price = float(resp.data[0].price.numerator) / resp.data[0].price.denominator
        print 'price of %s is %s' % (symbol, price)
    else:
        print 'price of %s cannot be determined: %s' % (symbol, resp.return_message)
