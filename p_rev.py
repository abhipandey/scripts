#!/usr/bin/python3.4

import os
import sys
import argparse
import re


e = 'FGHJKMNQUVXZ'
p = '(.*1\d)([{}])(.*)'.format(e)
p2 = '(.*)(1[4-9][01]\d)(.*)'
fmt = '{:>30} ------> {:30}'

def rev_to_expiry_mon_num(d):
    # second style
    r = re.search(p, d)
    if r:
        d_first = r.group(1)
        d_char = r.group(2)
        d_last = r.group(3)
        # print('first = {}; char={}; last={}'.format(d_first, d_char, d_last))
        s = '{}{}{}'.format(
                d_first,
                ('0' + str(e.index(d_char)+1))[-2:],
                d_last
            )
        return s
    return ''

def rev_to_expiry_mon_num2(d):
    # second style
    r = re.search(p2, d)
    if r:
        d_first = r.group(1)
        d_nums = r.group(2)
        d_last = r.group(3)
        # print('first = {}; char={}; last={}'.format(d_first, d_nums, d_last))
        s = '{}{}{}'.format(
                d_first,
                '{}{}'.format(d_nums[:2], e[int(d_nums[-2:])-1]),
                d_last
            )
        return s
    return ''

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Reverse Strings like AL1501 into GH symbology')
    parser.add_argument('-s', '--symbols', nargs='+',
                       help='list of symbols to reverse', required=True)
    args = parser.parse_args()
    syms = args.symbols
    
    for s in syms:
        fn = rev_to_expiry_mon_num if re.search(p, s) else rev_to_expiry_mon_num2
        print(fmt.format(s, fn(s)))
