
import subprocess
import re
import os, sys
import datetime
from datetime import timedelta
from itertools import izip_longest

_FILE = 'hiera/node/common.json'
FLAGS = re.MULTILINE | re.IGNORECASE
# _PATTERNS = ['^- +.(?P<name>[\w]+)" *[: ]+"(?P<version>[\d\.]+)']
_DATE_PATTERN = r'^Date:   (?P<date>.*) \+..00$'

_PATTERNS = [_DATE_PATTERN]


class Deployments:
    def __init__(self, name, before, after, dt):
        self.name = name
        self.before = before
        self.after = after
        self.is_upgrade = self._is_upgrade()
        self.dt = datetime.datetime.strptime(dt, '%a %b %d %H:%M:%S %Y')
        self.hours_since_last_deploy = 0

    @property
    def proper_upgrade(self):
        return self.hours_since_last_deploy > 20 and self.is_upgrade

    def _is_upgrade(self):
        # when splitting, # of items shud b same
        item_before = [x.rjust(10, '0') for x in self.before.split('.')]
        item_after = [x.rjust(10, '0') for x in self.after.split('.')]
        if self.name.upper() == 'PYGHWEB':
            item_before = item_before[-2:]
            item_after = item_after[-2:]
        y1 = list(izip_longest(item_before, item_after))
        debug = self.name.upper() == 'HUMMER' and False
        if debug:
            print('ABHI: y1 <Before: {}>:<After: {}> ..... item before={}; item after={};  {}'.format(
                self.before,
                self.after,
                item_before,
                item_after,
                y1))
        for a,b in y1:
            if b > a:
                return True
        return False
        # return all([x[0]<= x[1] for x in y1])


    def __repr__(self):
        return str(self)
    def __str__(self):
        return '{:20} {:20} {:20} {:20} {:>20.1f}       {:<20}'.format(
            self.name.upper(),
            self.before,
            self.after,
            # 'True' if self.is_upgrade else 'False',

            str(self.dt),
            self.hours_since_last_deploy,
            'True' if self.proper_upgrade else 'False',
        )

# this would have creatd by "git log --oneline hiera/node/common.json > /tmp/1"
def create_git_commands(file='/tmp/1'):
    fmt = 'git show {0} {1} > /tmp/shows/{0}.log\n'
    commits = [l.split(' ')[0] for l in get_file_content(file)]
    with open('/tmp/1.sh', 'wt') as f:
        for c in commits:
            f.write(fmt.format(c, _FILE))
    print(commits[:10])


def get_file_content(file_path, joined=False):
    with open(file_path, 'rt') as file_placeholder:
        lines = file_placeholder.readlines()
        return lines if not joined else '\n'.join(lines)


def read_folder(folder='/tmp/shows'):
    files = os.listdir(folder)
    # files = files[0:3]
    return [os.path.join(folder, f) for f in files]


_PATTERNS_FOR_VERSIONS = [r'- +.production" *[: ]+"(?P<version>.+)"',
             r'\+ +.production" *[: ]+"(?P<version>.+)"',
    r'"(?P<project>.*)" : {']


def get_versions(lines, file_path):
    clean = lambda x, project: x.replace('{}-'.format(project), '')
    versions = []
    date = ''
    project = ''
    ver_before = ''
    ver_after = ''

    for l in lines:
        s = re.search(_DATE_PATTERN, l)
        if s:
            date = s.groups()[0]

        s = re.search(_PATTERNS_FOR_VERSIONS[2], l)
        if s:
            project = s.groups()[0]

        s = re.search(_PATTERNS_FOR_VERSIONS[0], l)
        if s:
            ver_before = s.groups()[0]


        s = re.search(_PATTERNS_FOR_VERSIONS[1], l)
        if s:
            ver_after = s.groups()[0]
            if ver_before and ver_after and project:
                # print('.. {}, {}, {}'.format(project, ver_before, ver_after))
                try:
                    dep_ver = Deployments(project,
                                      clean(ver_before, project),
                                      clean(ver_after, project),
                                      date)
                    versions.append(dep_ver)
                except Exception as e:
                    print('Exception = {} :::: {} {} '.format(
                        file_path,
                        str(e),
                        locals()
                    ))
                ver_before = ''
                ver_after = ''
                project = ''

    return versions


def find_info(file_path):
    d = {}
    lines = get_file_content(file_path, False)
    content = '\n'.join(lines)
    y = get_versions(lines, file_path)
    return y


create_git_commands()

files = read_folder()
files.append('/tmp/shows/0b5c47b.log')

from itertools import chain, groupby
all_versions = sorted(list(chain(*([find_info(f) for f in files]))), key=lambda x: x.name)


SUMMARIES = []
for project, a_versions in groupby(all_versions, lambda x: x.name):
    print('\n\n---------------------- PROJECT: {} -----------------------------'.format(project))
    print('{:20} {:20} {:20} {:20} {:>10}       {:<20}'.format(
        'Project', 'Prev Ver', 'This Ver', 'Commit Time', 'Prev commit? (hrs)', 'Upgrade?'
    ))
    versions = list(sorted(a_versions, key=lambda x: x.dt))
    deploys = len(versions)

    earliest = min(versions, key=lambda x: x.dt)
    deploy_dt = datetime.datetime.min
    for v in versions:
        delta = v.dt - deploy_dt
        deploy_dt = v.dt
        v.hours_since_last_deploy = float(delta.total_seconds()) / 3600
        if v.hours_since_last_deploy > 1000:
            v.hours_since_last_deploy = 999
        # print('{}: {:>10.1f}'.format(v, v.hours_since_last_deploy))
        print(v)
    fails = len([x for x in versions if not x.proper_upgrade])
    SUMMARIES.append('~~~~~~~ {}: Deploys = {}; (Possible) Failures={}; Earliest Deployment={}'.format(
        project, deploys, fails, earliest.dt
    ))

print('======================= SUMMARY ========================')
print('\n'.join(SUMMARIES))
