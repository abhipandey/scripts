#!/bin/env python

from __future__ import print_function
import httplib2
import os
from datetime import datetime
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from apiclient import errors

import re
import base64
import email

import calendar_handler
try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/gmail-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/gmail.modify'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail API Python Quickstart'

def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'gmail-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

class MailHandler:
    def __init__(self, http=None):
        if http:
            self.http = http
        else:
            self.http = get_credentials().authorize(httplib2.Http())
        self.service = discovery.build('gmail', 'v1', http=self.http)
        self.calendar = calendar_handler.CalendarHandler()

    def list_messages_matching_query(self, user_id, query=''):
      """List all Messages of the user's mailbox matching the query.

      Args:
        service: Authorized Gmail API service instance.
        user_id: User's email address. The special value "me"
        can be used to indicate the authenticated user.
        query: String used to filter messages returned.
        Eg.- 'from:user@some_domain.com' for Messages from a particular sender.

      Returns:
        List of Messages that match the criteria of the query. Note that the
        returned list contains Message IDs, you must use get with the
        appropriate ID to get the details of a Message.
      """
      try:
        response = self.service.users().messages().list(userId=user_id,
                                                   q=query).execute()
        messages = []
        if 'messages' in response:
          messages.extend(response['messages'])

        while 'nextPageToken' in response:
          page_token = response['nextPageToken']
          response = self.service.users().messages().list(userId=user_id, q=query,
                                             pageToken=page_token).execute()
          messages.extend(response['messages'])

        return messages
      except errors.HttpError as error:
        print ('An error occurred: %s' % error)


    def list_messages_with_labels(self, user_id, label_ids=[]):
      """List all Messages of the user's mailbox with label_ids applied.

      Args:
        service: Authorized Gmail API service instance.
        user_id: User's email address. The special value "me"
        can be used to indicate the authenticated user.
        label_ids: Only return Messages with these labelIds applied.

      Returns:
        List of Messages that have all required Labels applied. Note that the
        returned list contains Message IDs, you must use get with the
        appropriate id to get the details of a Message.
      """
      try:
        response = self.service.users().messages().list(userId=user_id,
                                                   labelIds=label_ids).execute()
        messages = []
        if 'messages' in response:
          messages.extend(response['messages'])

        while 'nextPageToken' in response:
          page_token = response['nextPageToken']
          response = self.service.users().messages().list(userId=user_id,
                                                     labelIds=label_ids,
                                                     pageToken=page_token).execute()
          messages.extend(response['messages'])

        return messages
      except errors.HttpError as error:
        print('An error occurred: %s' % error)

    def get_message(self, user_id, msg_id):
      """Get a Message with given ID.

      Args:
        service: Authorized Gmail API service instance.
        user_id: User's email address. The special value "me"
        can be used to indicate the authenticated user.
        msg_id: The ID of the Message required.

      Returns:
        A Message.
      """
      try:
        message = self.service.users().messages().get(userId=user_id, id=msg_id).execute()

        # print ('Message snippet: %s' % message['snippet'])

        return message
      except errors.HttpError as error:
        print('An error occurred: %s' % error)

    def get_mime_message(self, user_id, msg_id):
      """Get a Message and use it to create a MIME Message.

      Args:
        service: Authorized Gmail API service instance.
        user_id: User's email address. The special value "me"
        can be used to indicate the authenticated user.
        msg_id: The ID of the Message required.

      Returns:
        A MIME Message, consisting of data from Message.
      """
      try:
        message = self.service.users().messages().get(userId=user_id, id=msg_id,
                                                 format='raw').execute()

        print('Message snippet: %s' % message['snippet'])

        msg_str = base64.urlsafe_b64decode(message['raw'].encode('ASCII'))
        print('\n\n\nMessage snippet: 2: {}'.format(msg_str))

        mime_msg = email.message_from_string(msg_str)

        return mime_msg
      except errors.HttpError as error:
        print('An error occurred: %s' % error)


    def handle_msg(self, msg):
        t_fmt = '{}T{}+08:00'
        _event_time_l = lambda s: datetime.strptime(s, '%d/%m/%Y %I:%M:%S %p').strftime('%Y-%m-%dT%H:%M:%S+08:00')
        _pattern = re.compile('(?P<NAME>.*) new application has been approved: +(?P<TYPE>\w+) +from (?P<DATE_FROM>[0-9\/]+) to +(?P<DATE_TO>[0-9\/]+) +(?P<TIME_FROM>[0-9:]+...) to +(?P<TIME_TO>[0-9:]+...)',
                              re.IGNORECASE)
        details = self.get_message('me', msg['id'])
        print('got something: {}'.format(details['snippet']))
        re_search = re.search(_pattern, details['snippet'])
        if re_search:
            vals = re_search.groupdict()
            s_time = _event_time_l('{} {}'.format(vals['DATE_FROM'], vals['TIME_FROM']))
            e_time = _event_time_l('{} {}'.format(vals['DATE_TO'], vals['TIME_TO']))


            print('vals = {}'.format(vals))

            event_msg = {
                        'summary': '{}: {} Leave'.format(vals['NAME'], vals['TYPE']),
                        'description': details['snippet'],
                        'start': {
                            'dateTime': s_time,
                            'timeZone': 'Asia/Singapore'
                        },
                        'end': {
                            'dateTime': e_time,
                            'timeZone': 'Asia/Singapore'
                        },
                        'reminders': {
                            'useDefault': True
                        },
                    }

            print('  event = {}'.format(event_msg))
            # self.calendar.create_event(event_msg)
            # self.mark_msg_as_read(msg)

    def mark_msg_as_read(self, msg):
        self.service.users().messages().modify(
            userId='me',
            id=msg['id'],
            body={'removeLabelIds': ['UNREAD']}
        ).execute()

    def create_event(self, event_msg):
        self.calendar.create_event(event_msg)

    def get_labels(self):
        results = self.service.users().labels().list(userId='me').execute()
        return results.get('labels', [])


def main():
    """Shows basic usage of the Gmail API.

    Creates a Gmail API service object and outputs a list of label names
    of the user's Gmail account.
    """
    handler = MailHandler()
    # credentials = get_credentials()
    # http = credentials.authorize(httplib2.Http())
    # service = discovery.build('gmail', 'v1', http=http)
    #
    # results = service.users().labels().list(userId='me').execute()
    labels = handler.get_labels()

    if not labels:
        print('No labels found.')
    else:
      print('Labels:')
      for label in labels:
        print('{:30} ---> {}'.format(label['name'], label['id']))

    label = ['Label_15']
    msgs = handler.list_messages_with_labels('me', label)

    # print(msgs)
    msgs = handler.list_messages_matching_query('me', 'label:automated-leaves is:unread ')

    for msg in msgs:
        handler.handle_msg(msg)
    # print('m = {}'.format(dir(m)))
    # print('#'*80)
    # details = get_mime_message(service, 'me', m['id'])
    # print('details = {}'.format(details))




if __name__ == '__main__':
    main()
