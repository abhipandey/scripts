#!/bin/env python

#https://console.developers.google.com/apis/credentials/wizard?api=calendar-json.googleapis.com&project=melodic-now-120323&authuser=1

from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

import datetime

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None
# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json
# SCOPES = 'https://www.googleapis.com/auth/calendar.readonly'
SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Calendar API Python Quickstart'
_CAL_LEAVE = 'Planned IT leave'.upper()
_CAL_DELETED = 'DELETED'

def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

_GET_CALENDAR = lambda cals, name: [c['id'] for c in cals if c['summary'] == name][0]


class CalendarHandler:
    def __init__(self, http=None):
        if http:
            self.http = http
        else:
            self.http = get_credentials().authorize(httplib2.Http())
        self.service = discovery.build('calendar', 'v3', http=self.http)
        self.calendars = \
            self.service\
            .calendarList()\
            .list()\
            .execute()\
            .get('items', [])

    def get_calendar_id(self, name):
        return [c['id']
                for c in self.calendars
                if c['summary'].upper() == name.upper()][0]

    def get_events(self,
                   calendar_name,
                   now=None,
                   max_results=10,
                   order_by='startTime'):
        cal_id = self.get_calendar_id(calendar_name)
        if not now:
            now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        return self.service.events().list(
            calendarId=cal_id,
            timeMin=now,
            maxResults=max_results,
            singleEvents=True,
            orderBy=order_by
        ).execute().get('items', [])

    def create_event(self, event, calendar_name='DELETED'):
        cal_id = self.get_calendar_id(calendar_name)
        event = self.service.events().insert(calendarId=cal_id, body=event).execute()
        print('Event created: %s' % (event.get('htmlLink')))

def main():
    handler = CalendarHandler()
    for c in handler.calendars:
        print('{:>40} -- > {}'.format(c['summary'], c['id']))
    events = handler.get_events(_CAL_LEAVE)

    if not events:
        print('No upcoming events found.')
    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date'))
        print(start, event['summary'])
    # handler.create_event()
if __name__ == '__main__':
    main()
