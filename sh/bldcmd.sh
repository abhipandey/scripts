#!/usr/bin/env bash
clear


echo -e ""
printf "%0.s\n" {1..20}
printf "%0.s_" {1..90}
echo -e ""
printf "%0.s_" {1..90}
echo -e ""
printf "%0.s_" {1..90}
echo -e ""
printf "%0.s_" {1..90}
echo -e ""
printf "%0.s_" {1..90}
echo -e ""
echo -e "building " $(date)

ps -ef | egrep "g\+\+"| awk '{print $2}'
ps -ef | egrep "g\+\+"| awk '{print $2}' | xargs kill -9 

create_build_command()
{
    ag "$1" compile_commands.json --nonumbers | egrep -v '"file"' | sed -e 's/.*\(.opt.*g.. \)/\1/g' | sed -e 's/$/ 2>\&1 | tee \/tmp\/failed.file.log \&\& echo -e "COMPILATION COMPLETE" \&\n/g' -e 's/^/unbuffer /g' | sed -e 's/",//g'
}
FILE=$(echo $1 | sed -e 's/.*ccgh/ccgh/g')
create_build_command $FILE $2 | sed "$2q;d"
create_build_command $FILE $2 | sed "$2q;d" | bash - &
