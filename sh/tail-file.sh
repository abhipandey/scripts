#!/usr/bin/env bash
clear


echo -e ""
printf "%0.s_" {1..90}
echo -e ""
echo -e "Tailing.. " $(date)

cat /tmp/failed.file.log | egrep "error: "


