#!/bin/bash

JENKINS_URL="https://prod.jenkins.ghpr.asia/"  # Fill in the jenkins url
JENKINSFILE_PATH="/home/abhishek.pandey/code/p/higgs9/ccgh/cicd/build-ccgh-gke.groovy"  # fill in the path to the Jenkinsfile to lint
USER="abhishek.pandey"  # Fill in your API token. Generate these through the UI. See https://stackoverflow.com/a/45466184/11741678
TOKEN="1120c8cb1e7f021383023e9db942243af1"   # Fill in your username that you log in with, e.g. "alexander.hermes"



JENKINS_CRUMB=$(curl "$JENKINS_URL/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)" --user ${USER}:${TOKEN})
curl -X POST -H $JENKINS_CRUMB -F "jenkinsfile=<${JENKINSFILE_PATH}" $JENKINS_URL/pipeline-model-converter/validate --user ${USER}:${TOKEN}

