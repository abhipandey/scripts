#!/home/abhishek.pandey/code/scripts/ve_scripts/bin/python

from bs4 import BeautifulSoup
from urllib import request


from base_mail import send_mail

u  = 'http://www.xe.com/currencyconverter/convert/?Amount=1&From=SGD&To=INR'
r = request.urlopen(u).read()
soup = BeautifulSoup(r, 'html.parser')
a = soup.findAll('span', {"class": "uccResultAmount"})
# print(a[0].get_text())


send_mail('SGD INR exchange rate',
          a[0].get_text(),
          ['abhishek@tilde.sg'])
