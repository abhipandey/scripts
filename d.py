#!/opt/py27env/bin/python
import subprocess
import re
import os, sys


text = '''
commit 0ba33af8ce5fc85abfac42ad80110c586b8e8eea
Author: Anton Appelberg <anton@grasshopperasia.com>
Date:   Tue Aug 25 08:10:57 2015 +0200

    hummer: upgrade prod to 0.2.128

diff --git a/hiera/node/common.json b/hiera/node/common.json
index 1c144ad..6c0564d 100644
--- a/hiera/node/common.json
+++ b/hiera/node/common.json
@@ -43,7 +43,7 @@
       "hummer" : {
         "development" : "hummer-0.2.128",
         "staging"     : "hummer-0.2.128",
-        "production"  : "hummer-0.2.126"
+        "production"  : "hummer-0.2.128"
       },
       "bob" : {
         "development" : "bob-0.1.131",
commit 0b5c47b80793cd75ac9c3b9d448340b0aa3744d2
Author: Jacek Sieka <jacek.sieka@grasshopperasia.com>
Date:   Thu Mar 19 10:41:56 2015 +0800

    mds*: upgrade dev/staging to 0.1.42

diff --git a/hiera/node/common.json b/hiera/node/common.json
index 4006e02..9be3f62 100644
--- a/hiera/node/common.json
+++ b/hiera/node/common.json
@@ -19,23 +19,23 @@
     },
     "version_defaults" : {
       "mds" : {
-        "development" : "mds-0.1.41",
-        "staging"     : "mds-0.1.41",
+        "development" : "mds-0.1.42",
+        "staging"     : "mds-0.1.42",
         "production"  : "mds-0.1.40"
       },
       "mdsbook" : {
-        "development" : "mdsbook-0.1.40",
-        "staging"     : "mdsbook-0.1.40",
+        "development" : "mdsbook-0.1.42",
+        "staging"     : "mdsbook-0.1.42",
         "production"  : "mdsbook-0.1.40"
       },
       "mdsl2" : {
-        "development" : "mdsl2-0.1.39",
-        "staging"     : "mdsl2-0.1.39",
+        "development" : "mdsl2-0.1.42",
+        "staging"     : "mdsl2-0.1.42",
         "production"  : "mdsl2-0.1.39"
       },
       "mdsrate" : {
-        "development" : "mdsrate-0.1.40",
-        "staging"     : "mdsrate-0.1.40",
+        "development" : "mdsrate-0.1.42",
+        "staging"     : "mdsrate-0.1.42",
         "production"  : "mdsrate-0.1.40"
       },
       "hummer" : {
@@ -46,7 +46,7 @@
       "bob" : {
         "development" : "bob-0.1.91",
         "staging"     : "bob-0.1.91",
-        "production"  : "bob-0.1.85"
+        "production"  : "bob-0.1.90"
       }
     },
     "url" : {
'''
p = r'"(?P<project>.*)" : {(?:\n.*){0,3}- +.production" *[: ]+"(?P<version>.+)"'
FLAGS = re.MULTILINE | re.IGNORECASE


_PATTERNS_FOR_VERSIONS = [r'- +.production" *[: ]+"(?P<version>.+)"',
             r'\+ +.production" *[: ]+"(?P<version>.+)"',
    r'"(?P<project>.*)" : {']


def get_versions(lines):
    clean = lambda x, project: x.replace('{}-'.format(project), '')
    print('# of liens = {}'.format(len(lines)))
    versions = []
    project = ''
    ver_before = ''
    ver_after = ''

    for l in lines:
        s = re.search(_PATTERNS_FOR_VERSIONS[2], l)
        if s:
            project = s.groups()[0]

        s = re.search(_PATTERNS_FOR_VERSIONS[0], l)
        if s:
            ver_before = s.groups()[0]
            print('.. {}, {}'.format(project, ver_before))


        s = re.search(_PATTERNS_FOR_VERSIONS[1], l)
        if s:
            ver_after = s.groups()[0]
            if ver_before and ver_after and project:
                print('.. {}, {}, {}'.format(project, ver_before, ver_after))

                versions.append((project, clean(ver_before, project), clean(ver_after, project)))
                ver_before = ''
                ver_after = ''
                project = ''

    print('ABHI: l = {}'.format(versions))
    return versions

x = get_versions(text.split('\n'))