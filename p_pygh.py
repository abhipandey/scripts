#!/opt/py27env/bin/python2.7
import datetime

# This comes from gitlab/snippets/156
from base_pygh import *

from pyghweb.ghw.models import *
from pyghweb.eve.models import *
import core_pb2
import emscentral_pb2
from django.conf import settings

from pyghweb.ghw.messaging.solace import requests, topics
from gh import solace

from pyghweb.eve.fill.requests import PositionAdjustment


solapi = solace.SimpleApi.make_from_django_settings(settings)
req = PositionAdjustment(solapi)

# Find out the positions for SG
objs = ApplicationAdjustment.objects.filter(
                # application_appid=app_id,
                # order_book_name=order_book_name,
                instrument_id_type=8,
                instrument_id_key__regex=r'XSES\|SG[XZ]',
            ).order_by('-updated_at')


for o in objs:
    logging.info('_'*80)
    logging.info('Objs={}; Updating: {}'.format(
        o, o.updated_at))

    # Now double these positions
    resp = req.request(order_book_id=o.order_book_name,
                       id_type=o.instrument_id_type,
                       instrument_id=o.instrument_id_key.replace('|SG', '|SGP'),
                       adj_long=o.adjustment_long*2,
                       adj_short=o.adjustment_short*2)
    logging.info('code={}'.format(resp.return_code))
    logging.info('return_message={}'.format(resp.return_message))

    logging.info('Objs={}; Updated: {}'.format(o, o.updated_at))


logging.info('Done')
