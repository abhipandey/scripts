#!/usr/bin/python3.4

import os
import sys
import argparse
import pprint
import subprocess
import shlex

def get_pretty_print(item, force=False):
    if force:
        custom_printer = lambda a: (isinstance(a,list) or isinstance(a, tuple)) and \
                                   len(a) > 0 and \
                                   (isinstance(a[0], list) or isinstance(a[0], tuple))
        if custom_printer(item):
            s = ''
            for i in item:
                s += '\n' + str(i)
            return s
    pp = pprint.PrettyPrinter(indent=4)
    return pp.pformat(item)


pp = lambda item: get_pretty_print(item)



def run_command(command):
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    while True:
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        if output:
            print output.strip()
    rc = process.poll()
    return rc


# run_command('ls -l')
run_command('bash -i /tmp/do.sh')
