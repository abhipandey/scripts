#!/home/abhishek.pandey/code/scripts/decathlon/ve/bin/python

from bs4 import BeautifulSoup
import re
import urllib
import os
import pprint
import subprocess
import glob
from typing import List, Sequence
from collections import OrderedDict
import pickle

from slacker import Slacker

DEFAULT_TIMEOUT = 300  # in seconds

pp = pprint.PrettyPrinter(indent=4)
print('='*120)

def get_cmd_results(cmd: Sequence[str]) -> str:
    """
    Runs a command, provides the results as Pipes
    """
    return run_cmd(cmd).stdout.decode("utf-8").strip()


def run_cmd(cmd: Sequence[str],
            timeout: int = DEFAULT_TIMEOUT) -> subprocess.CompletedProcess:
    """"
    Run command in subprocess and return job output

    :param cmd: The command to run
    :param timeout: Command timeout, in seconds
    """
    print("Running %s", cmd)
    try:
        job = subprocess.run(args=cmd,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             timeout=timeout)
    except subprocess.CalledProcessError:
        print("Hit error running cmd:")
        raise
    except subprocess.TimeoutExpired:
        print("Timed out waiting for cmd:")
        raise

    print("RC: %s, output: %s", job.returncode, job.stdout.decode("utf-8"))

    return job


def parse_page(url):
    cmd = f'wget {url}'
    results = get_cmd_results(cmd.split())
    print(f' URL = {url}')
    basename = os.path.basename(url)
    return parse_html(basename)


def parse_html(url):
    print(f'URL: {url}')

    page = open(url)
    soup = BeautifulSoup(page.read(), 'html.parser')

    title = soup.find('title')

    options = soup.find_all('span', {'class': 'radio-label'})
    results = {}
    for option in options:
        classes = option.get('class')
        is_available = 'unavailable-size' not in classes
        # print(f' classes = {classes}; is_available = {is_available}')
        # print(f'Option = {option.get_text()}')
        results[option.get_text()] = is_available

    print(f'  Results = {results}')
    return {title.get_text(): OrderedDict(results)}


URLS = [
    'https://www.decathlon.sg/p/8480246-112682-riverside-900-hybrid-bike.html',
    'https://www.decathlon.sg/p/8392092-85391-hybrid-riverside-500-c1.html',
    'https://www.decathlon.sg/p/8550625-124640-riverside-100-matt-bike.html',
    'https://www.decathlon.sg/p/8522289-112591-triban-100-fb-flat-bar-road-bike.html',
    'https://www.decathlon.sg/p/8389939-87973-triban-500-road-bike-red.html'
    # 'https://www.decathlon.sg/p/8487236-124460-elops-120-low-frame-town-bike-blue.html'
]


def clear_html_files():
    fileList = glob.glob('*.html*')
    print(f'ABHI: fileList= {fileList}')
    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        try:
            os.remove(filePath)
        except:
            print("Error while deleting file : ", filePath)


SLACK_TOKEN = 'xoxb-18734302050-7Aa4j8DaPYwAKvzSWrczVJ0o'


def send_to_slack(text, attachments=None, channels=['@abhi']):
    slack = Slacker(SLACK_TOKEN)
    for channel in channels:
        slack.chat.post_message(channel,
                                text=text,
                                attachments=attachments,
                                as_user=False)


PICKLE_FILE = 'results.pickle'


def test_pickle(results):
    prev_results = pickle.load(open(
        PICKLE_FILE, "rb")) if os.path.exists(PICKLE_FILE) else None
    print(f'prev results = {prev_results}')

    pickle.dump(results, open(PICKLE_FILE, "wb"))

    has_anything_changed = prev_results != results
    return has_anything_changed


def main():
    clear_html_files()
    all_results = OrderedDict()
    for url in URLS:
        vals = parse_page(url)
        print(f'ABHI: {vals}')
        key_vals_0 = list(vals.keys())[0]
        all_results[key_vals_0] = vals[key_vals_0]
    print('-' * 80)
    print(f'all_results  = {all_results}')

    have_results_changed = test_pickle(all_results)

    filtered_results = {}
    for title, results in all_results.items():
        if any(results.values()):
            filtered_results[title] = results
    if have_results_changed and filtered_results:
        res_s = pp.pformat(dict(filtered_results))
        send_to_slack(res_s)


if __name__ == '__main__':
    main()
'''

# print(f' options = {dir(options[0])}; type - {type(options[0])}')
options = ['__bool__', '__call__', '__class__', '__contains__', '__copy__', '__delattr__',
 '__delitem__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattr__',
 '__getattribute__', '__getitem__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__',
 '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__setitem__', '__sizeof__',
  '__str__', '__subclasshook__', '__unicode__', '__weakref__', '_all_strings', '_find_all', '_find_one', '_is_xml',
   '_lastRecursiveChild', '_last_descendant', '_should_pretty_print',

 'append', 'attrs', 'can_be_empty_element', 'cdata_list_attributes', 'childGenerator',
 'children', 'clear', 'contents', 'decode', 'decode_contents', 'decompose', 'decomposed',
 'descendants', 'encode', 'encode_contents', 'extend', 'extract', 'fetchNextSiblings',
 'fetchParents', 'fetchPrevious', 'fetchPreviousSiblings',
 'find', 'findAll', 'findAllNext', 'findAllPrevious', 'findChild', 'findChildren', 'findNext', 'findNextSibling',
 'findNextSiblings', 'findParent', 'findParents', 'findPrevious', 'findPreviousSibling', 'findPreviousSiblings', 'find_all',
  'find_all_next', 'find_all_previous', 'find_next', 'find_next_sibling', 'find_next_siblings', 'find_parent', 'find_parents',
  'find_previous', 'find_previous_sibling', 'find_previous_siblings', 'format_string', 'formatter_for_name', 'get',
  'getText', 'get_attribute_list', 'get_text', 'has_attr', 'has_key', 'hidden', 'index', 'insert', 'insert_after',
  'insert_before', 'isSelfClosing', 'is_empty_element', 'known_xml', 'name', 'namespace', 'next', 'nextGenerator',
  'nextSibling', 'nextSiblingGenerator', 'next_element', 'next_elements', 'next_sibling', 'next_siblings', 'parent',
  'parentGenerator', 'parents', 'parserClass', 'parser_class', 'prefix', 'preserve_whitespace_tags',
  'prettify', 'previous', 'previousGenerator', 'previousSibling', 'previousSiblingGenerator', 'previous_element',
  'previous_elements', 'previous_sibling', 'previous_siblings', 'recursiveChildGenerator', 'renderContents',
  'replaceWith', 'replaceWithChildren', 'replace_with', 'replace_with_children', 'select', 'select_one', 'setup',
  'smooth', 'sourceline', 'sourcepos', 'string', 'strings', 'stripped_strings', 'text', 'unwrap', 'wrap']; type - <class 'bs4.element.Tag'>
'''
