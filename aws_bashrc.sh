#!/usr/bin/env bash

# AWS .bashrc
alias conf='ll /home/ec2-user/ws/tampines/confo | egrep "Mar "'
shopt -s expand_aliases
# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# GIT STUFF
# git pull
# perl -pi -e 's/foo/bar/g' file.txt
# sed -i "s/STRING_TO_REPLACE/STRING_TO_REPLACE_IT/g" <file>
alias msconfig='sudo ntsysv'
alias mem='top -u ec2-user'
export V1_SRC=~/ws/tilde_bb/reconwisev1
export V2_SRC=~/ws/tilde_bb/reconwisev2/rw
export V1_DATA=/var/shared/data/v1/
export VENV_RW1=/home/ec2-user/ws/venv_rw1
export VENV_RW2='/home/ec2-user/ws/venv_rw'
export DJANGO_SETTINGS_MODULE=rw.settings
export PYTHONUNBUFFERED=1
alias act_v2='cd $VENV_RW2 && source bin/activate'
alias act_v1='cd $VENV_RW1 && source bin/activate'
alias gg='git clone git@bitbucket.org:tampinestechnology/reconwisev2.git'
alias kilp3='pkill -9 python3.4'
alias gits='git status -uno -s'
alias gitrevert='git checkout -- ./*'
alias grh='git reset --hard origin/master && git pull'
alias gitdiff='git difftool -y -x "colordiff -y -W $COLUMNS" | less -R'
alias gdiff='git difftool -y -x "colordiff -y -W $COLUMNS"'
alias rw_abhi='vim $V1_DATA/abhishek.pandey_tampinestechnology.com'
alias rw1_log='vim /var/shared/data/rw_v1.log'
alias rw2_log='vim /var/shared/data/rw_v2.log'
alias vim_prod='vim $V1_SRC/src/main/resources/prod-config.ini'
#alias rw_ini='vim rw_latest/reconwise/src/tam-config.ini'
alias rw_ini='vim $V1_SRC/src/tam-config.ini'
export RECON_CONFIG='main/resources/prod-config.ini'
export PYTHONPATH=test/python/reconwisetestPy:main/python
alias rundj='cd $V1_SRC/src && nohup python3.4 $V1_SRC/web/manage.py runserver 0.0.0.0:8002 > /tmp/django.log 2>&1 &'
alias plrw='psql -h localhost -U postgres -d rwdb'
alias pl='psql -h localhost -U postgres'
alias prw1='vim ~/ws/scripts/p_rw1.py'
alias start_db='sudo service postgresql93 start && sudo service mysqld start'
alias rrw1="run_rw1_with_config 'main/resources/prod-config.ini'"
alias rrw2="rw_v2"
alias tailv1='tail -f ~/data/v1/nohup.log'


fix_configs()
{
    sed -i 's/^password=$/password=passwords123/g' /home/ec2-user/rw_latest/reconwise/src/tam-config.ini
    #sed -i 's/^run_main=True$/run_main=False/g' /home/ec2-user/rw_latest/reconwise/src/tam-config.ini
    sed -i 's/^report_receivers=abhi.pandey@gmail.com$/report_receivers=shan.yeo@grasshopperasia.com,grasshopper-reconwise@tampinestechnology.com/g' /home/ec2-user/rw_latest/reconwise/src/tam-config.ini
    sed -i 's/^enable_reports=False$/enable_reports=True/g' /home/ec2-user/rw_latest/reconwise/src/tam-config.ini
    sed -i 's/^reporter_mail_from_name=AbhiMac Reports$/reporter_mail_from_name=TamTech Reports/g' /home/ec2-user/rw_latest/reconwise/src/tam-config.ini
    sed -i 's/^common_folder=.var.reconwise.$/common_folder=\/home\/ec2-user\/data/v1/g' /home/ec2-user/rw_latest/reconwise/src/tam-config.ini
    sed -i 's/^local_folder_save=\/var\/reconwise\/emails\/$/local_folder_save=\/home\/ec2-user\/data/v1\/emails\//g' /home/ec2-user/rw_latest/reconwise/src/tam-config.ini
    sed -i 's/^report_folder=\/var\/reconwise\/reports\/$/report_folder=\/home\/ec2-user\/data/v1\/reports\//g' /home/ec2-user/rw_latest/reconwise/src/tam-config.ini
    sed -i 's/\/tmp\/rw.log/\/home\/ec2-user\/data/v1\/logs\/rw.log/g' /home/ec2-user/rw_latest/reconwise/src/main/resources/logging.conf
}
alias rw_uat='rw_v1'
rw_v1()
{
    echo '--> Killing all python3.4 processes'
    ps -u ec2-user | egrep -v "ssh|bash| ps$|grep$"
    pkill -9 python3.4  #kill any process that is running on the server with python3.4
    cd $V1_SRC

    echo '--> GIT Status'
    gits  # Show the git status: Ensure no mistakes
    echo '--> GIT Reverting all files.... '
    git checkout -f tags/recon_prod
    git pull origin tags/recon_prod
    #gitrevert  # Revert all changes that we made with fix_configs()
    #git pull   # Sync the latest changes

    #echo '--> Fixing all configs'
    #fix_configs  # Call the function to modify any settings and make the project production ready
    sed -i 's/\/tmp\/rw.log/\/home\/ec2-user\/data\/v1\/logs\/rw.log/g' $V1_SRC/src/main/resources/logging.conf
    mysql -u root -ppasswords123 < $V1_SRC/src/main/scripts/db_scripts.sql

    echo '--> GIT Status'
    gits  # Show the git status: Ensure no mistakes

    # Run the process now with nohup
    run_both
}
run_rw1_with_config()
{
    source $VENV_RW1/bin/activate
    pip3.4 install -r $V1_SRC/requirements.txt

    ### Set the pythonpath so that "py.test-3.4" knows how to search the paths relatively
    export PYTHONPATH=test/python/reconwisetestPy:main/python

    echo '--> Running nohup process now'
    cd $V1_SRC/src

    echo Config File= $1
    nohup python3.4 $V1_SRC/src/main/python/main.py -c $1 > ~/data/v1/nohup.log 2>&1 &
    echo 'Log File= ~/data/v1/nohup.log or rw1_log'
}

_setup_v2()
{
    cd $VENV_RW2
    source bin/activate
    echo 'Virtual Env activated'
    which python && python --version
    pip3.4 install -r $V2_SRC/../requirements.txt
    cd $V2_SRC

    mkdir -p media/log
    mkdir -p media/statics
    mkdir -p media/tmp
}
rw_v2()
{
    _setup_v2
    pkill -9 python3.4  #kill any process that is running on the server with python3.4
    git checkout -f tags/v2_uat
    git pull origin tags/v2_uat
    manage.py makemigrations
    manage.py migrate

    # We need to run this eventually to collect all static files, together with debug=False
    manage.py collectstatic --noinput
    mkdir -p $V2_SRC/log/
    run_both
}
run_both()
{
    run_rw1_with_config 'main/resources/prod-config.ini'
    # run_rw1_with_config 'main/resources/uat-config.ini'
    # run_rw1_with_config 'main/resources/test-config.ini'
    _setup_v2
    nohup python3.4 manage.py runserver --settings=rw.uat_settings 0.0.0.0:8000 > ~/data/v2/nohup.rw.v2.log 2>&1 &

    # Ensure that the process has started running
    ps -u ec2-user | egrep -v "ssh|bash| ps$|grep$"
}
################################################################################# PYTHON SECTION #################################################################################
# Installing modules
# sudo /usr/local/bin/pip3.4 install pymysql
# sudo /usr/local/bin/pip3.4 install xlrd
# sudo /usr/local/bin/pip3.4 install watchdog


# sudo yum install httpd24

##################################################################################################################################################################
alias run_recon="cd $V1_SRC/src && p3 $V1_SRC/src/main/python/main.py -c 'main/resources/prod-config.ini'"
alias run_hup="cd $V1_SRC/src && nohup python3.4 $V1_SRC/src/main/python/main.py -c 'main/resources/prod-config.ini'> ~/data/v1/nohup.log 2>&1 &"
alias run_sql='mysql -u root -ppasswords123 --database tam_tech_1'
alias gorw='cd $V1_SRC/src/main/python/'
#alias run_recon="cd /home/ec2-user/rw_latest/reconwise/src && p3 /home/ec2-user/rw_latest/reconwise/src/main/python/main.py -c 'main/resources/prod-config.ini'"
#alias run_hup="cd /home/ec2-user/rw_latest/reconwise/src && nohup python3.4 /home/ec2-user/rw_latest/reconwise/src/main/python/main.py -c 'main/resources/prod-config.ini'> ~/data/v1/nohup.log 2>&1 &"
#alias run_sql='mysql -u root -p'
#salias gorw='cd /home/ec2-user/rw_latest/reconwise/src/main/python/'

alias show_all='cat /etc/*-release'
pathadd()
{
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${PATH:+"$PATH:"}$1"
    fi
}

pathadd '/home/ec2-user/ws/scripts'
pathadd '.'
pathadd '/usr/local/bin/'
export PYTHONSTARTUP=/home/ec2-user/.pythonstartup
# User specific aliases and functions
# allow unlimited core files
ulimit -c unlimited

# command history
export HISTFILESIZE=
export HISTSIZE=
#export HISTFILESIZE=999999
export HISTCONTROL=ignoredups


#PS1='\e[1;31;34m\e[1;31;36m\h: \e[0;0m\e[1;35m[\w]\e[0;0m\n\u > '
PS1='RW: \e[0;0m\e[1;35m[\w]\e[0;0m\n > '

alias p3='python3.4'
# grep stuff
export GREP_OPTIONS='--color=auto'

# =====================================================================================================================================
alias lllogs='echo ~/jnx.pbs/logs/ && ll ~/jnx.pbs/logs/'

# Set the process to a single core - Core #0
alias cpuset='taskset -c -p 0 '

alias wl='watch -n 60 ls'
# aliai
alias sortu='sort -u'
alias vc='vim config*.xml'
alias vmake='vim Makefile'
alias gz='zipcode'
# This is for google test
alias showv='echo -e Current VARSPACE=${URed}$VARSPACE'
alias k1='kil %1'
alias fk1='fkil %1'
alias k2='kil %2'
alias fk2='fkil %2'
alias namefind='find ./ -name '
alias dirs='ls -d */'
alias myps='ps -u ec2-user | egrep -v "ssh|bash| ps$|grep$" && echo ------------ && ps -ef | egrep python'
alias kil='kill -15'
alias fkil='kill -9'
alias pykil='kill -s INT '
alias pk='kill -2 '
alias disk='du -sh'
alias lsize='ls -lshrt'
alias myvar='cd $VARSPACE && showv'
alias v='myvar'
alias chgv='chg && v'
alias llogs='ll *.log'
alias txt='ll *.txt'
alias tars='ls *.tar'
alias zips='ls *.zip'
alias gzs='ls *.gz'
alias rpwd='pwd -P'
alias mybash='pwd && vim ~/.bashrc && . ~/.bashrc'
alias b='mybash'
alias bscript='vim ~/.bash_scripts && . ~/.bashrc'
alias myvim='pwd && vim ~/.vimrc'
alias ls='ls -G --color=auto'


# Directory traversal
alias 1cd="cd ../ && pwd"
alias 2cd="cd ../../ && pwd"
alias 3cd="cd ../../../ && pwd"
alias 4cd="cd ../../../../ && pwd"
alias 5cd="cd ../../../../../ && pwd"
alias 6cd="cd ../../../../../../ && pwd"
alias 7cd="cd ../../../../../../../ && pwd"

alias pz='/var/reconwise/scripts/process_zip.sh '
alias gt='gnome-terminal'
alias l="ls -F"
alias ll="l -lh"
alias lll='ll -rt | tail'


# Passphrase for deployment key on bitbucket is awstilde
# alias pubip='echo Reconwise Server && curl ipecho.net/plain && echo '
if [[ $- =~ "i" ]]; then
    IP=$(curl -s ipecho.net/plain)
    echo '################################################################'
    echo '             RECONWISE SERVER : tildesg.ddns.net'
    echo '                  Public IP: '  $IP
    echo '################################################################'
fi
