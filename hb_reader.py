import re
import datetime
import sys

#  bob.settrd-ems01.grass.corp.2020-03-12.061001.14966.log.gz
# sudo date +%T -s "12:43:33" && LIBLOG_logtostderr=1 LIBLOG_logtosyslog=1 LIBLOG_log_dir=./log ./bobD --config=./bob.json HLA
# FILE=bob.settrd-ems01.grass.corp.2020-03-12.061001.14966.log.gz && GREP=ag && $GREP "7[56]=.*hb#" $FILE | $GREP Create | sed -e 's/.*Create//g'  -e 's/.*session_id.//g' -e 's/]//g'  | xargs -I {} $GREP "(Send|Recv).*{}" $FILE >> /tmp/3
# ag "7[56]=.*hb#" bobD.dev41-ws.grass.corp.2020-03-03.124333.5406.log | ag Create | sed -e 's/.*Create//g'  -e 's/.*session_id.//g' -e 's/]//g'  | xargs -I {} ag "(Send|Recv).*{}"  bobD.dev41-ws.grass.corp.2020-03-03.124333.5406.log >> /tmp/3
#  ag "Send TaxHeartbeatReq message precision_hb_seq|ecv ResponseMessage header" > /tmp/4

## Server version
# FILE=bob.current
# EGREP=egrep
# GREP=grep
# $EGREP "7[56]=.*hb#" $FILE | $EGREP Create | sed -e 's/.*Create//g'  -e 's/.*session_id.//g' -e 's/]//g'  | xargs -I {} $EGREP {} $FILE
#
# last insert rejected:
# $GREP "9:\(29\|30\).*Insert rejected" $FILE | tail -1 | sed -e 's/.*client.order.id.//g' -e 's/].*//g' | xargs -I {} $GREP "Send .eader.*{}" $FILE
#
# First accept details
# $GREP  -m1 "OrderInsertRsp.*2=OK" $FILE | sed -e 's/.*client.tx.ref..//g' -e 's/ .*//g' | xargs -I {} $GREP "client.tx.ref..{}" $FILE

# _FILE_HB = '/home/abhishek.pandey/code/docs/logs/hbs.log'
_FILE_HB = '/tmp/1'
_FILE_QUERY_USER_REQ = '/home/abhishek.pandey/code/docs/logs/query-user-req.log'

# how to create a CSV file?#
# USAGE: python hb_reader.py | ag diff | awk '{print $2, $4, $6 }' | sed -e 's/09:29:59.//g' -e 's/'\''/"/g' -e 's/,$//' > /tmp/o.csv

import time
import datetime


def parse_time(hash):
    s = int(hash, 16) / 1000
    res = datetime.datetime.fromtimestamp(s).strftime('%Y-%m-%d %H:%M:%S.%f')
    return res


with open('/tmp/1', 'rt') as file:
    lines = file.readlines()
    for l in lines:
        items = l.split()
        t = parse_time(items[0])
        print(f'{items[0]}:: {t}')

sys.exit(0)
lines = []

_FILE = _FILE_HB
# _FILE = _FILE_QUERY_USER_REQ
with open(_FILE, 'rt') as file:
    lines = file.readlines()

if _FILE == _FILE_HB:
    HBs = dict()
    hb_id = ''
    _PAT_START = r'2020-0.-...(..:..:.........).*message id.(hb#.*#).*]'
    _PAT_END = r'2020-0.-...(..:..:.........).*Recv TaxHeartbeatRsp head'
    for l in lines:
        fmt_start = 'Send TaxHeartbeatReq message id'
        # print(f'{l}')
        if fmt_start in l:
            start_info = re.search(_PAT_START, l, re.IGNORECASE)
            if start_info:
                hb_id = start_info.group(2)
                HBs[hb_id] = {'ID': hb_id, 'SEND': start_info.group(1)}
        else:
            end_info = re.search(_PAT_END, l, re.IGNORECASE)
            if end_info:
                HBs[hb_id]['RECV'] = end_info.group(1)

    for hb_id, values in HBs.items():
        dt = lambda x: datetime.datetime.strptime(x, '%H:%M:%S.%f')
        start_dt = dt(values['SEND'])
        recv_dt = dt(values['RECV'])

        print(f'recv = {recv_dt}; send={start_dt}; ')
        diff = (recv_dt - start_dt).total_seconds() * 1000000
        values['DIFF'] = diff
        print(f'{values}')
else:
    info = list()
    hb_id = ''
    recv_idx = 0
    _PAT_START = r'2020-03-...(..:..:.........).*Send TaxHeartbeatReq mes'
    _PAT_END = r'2020-03-...(..:..:.........).*Recv ResponseMessage header'
    for l in lines:
        fmt_start = 'Send TaxHeartbeatReq message id'
        start_info = re.search(_PAT_START, l, re.IGNORECASE)
        if start_info:
            # hb_id = start_info.group(2)
            info.append({'ID': recv_idx, 'SEND': start_info.group(1)})
        else:
            end_info = re.search(_PAT_END, l, re.IGNORECASE)
            if end_info:
                print(f'recv_idx-{recv_idx}:  = {l}')
                info[recv_idx]['RECV'] = end_info.group(1)
                recv_idx += 1
                print(f'recv_idx = {recv_idx}')

    for values in info:
        dt = lambda x: datetime.datetime.strptime(x, '%H:%M:%S.%f')
        start_dt = dt(values['SEND'])
        recv_dt = dt(values['RECV'])
        print(f'recv = {recv_dt}; send={start_dt}')
        diff = (recv_dt - start_dt).microseconds / 1000
        values['DIFF_ms'] = diff
        print(f'{values}')