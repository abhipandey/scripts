#!/opt/py27env/bin/python2.7
import os
import sys

pygh_path = '/home/abhishek.pandey/code/higgs/pygh/'
os.chdir(pygh_path)
sys.path.append(pygh_path)
sys.path.append('/home/abhishek.pandey/code/higgs/_build/rls/protobuf/lib/python2.7/site-packages/')
sys.path.append('/home/abhishek.pandey/code/higgs/_build/rls/pylib/pysolace/pysolace/')
sys.path.append(os.path.join(pygh_path, 'pyghweb'))

# Use the following line for debugging
# print('sys path = {}'.format('\n'.join(sys.path)))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pyghweb.settings")

# Import normal stuff
from django.db.models import Sum, Avg
import django
from django.db.models import F, FloatField
from collections import defaultdict
import pprint
from itertools import chain, groupby
import logging


# Import project specific stuff, as an example
# from pyghweb.ghw.models import EtfEodValue
from pyghweb.fairvalue.datastore import FairValueDatastore


# Basic config to redirect everything to stdout. You can change it to a file
logging.basicConfig(
    # filename='/tmp/pygh.log',
    level=logging.DEBUG,
    # format='%(asctime)s %(levelname)-8s %(message)s',
    format='%(message)s',
    datefmt='%a, %d %b %Y %H:%M:%S',
    stream=sys.stdout)


print('Current Working Dir={}'.format(os.getcwd()))

def get_pretty_print_string(item):
    pp = pprint.PrettyPrinter(indent=4)
    return pp.pformat(item)
pp = lambda x: get_pretty_print_string(x)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Testing PyghWEB')
