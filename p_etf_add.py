#!/opt/py27env/bin/python2.7

from p_pygh import *
from pyghweb.ghw.models import Contract, ContractGroup, RiskProduct
import pyghweb.refdata.etf.models as etf_models
import pyghweb.symbology.models as sym_models

# reference: http://www.jpx.co.jp/english/equities/products/etfs/issues/

# we may want to deploy and make sure system is ready first before the actual
# listing of etf, hence will have the use case of creating all DB entries first
# except those that appear in ETF page.
add_to_etf_page = False

nk225_index = etf_models.Instrument.objects.get(code='=NK225.OS')
topix_index = etf_models.Instrument.objects.get(code='=TPX1.T')
jp400_index = etf_models.Instrument.objects.get(code='=JP400.OS')

logging.debug('nk225_index = {}'.format(nk225_index))
logging.debug('topix_index={}'.format(topix_index))
logging.debug('jp400_index={}'.format(jp400_index))




# (index, ticker, name, beta, trading unit)
etfs = (
(jp400_index,1464,'Daiwa ETF Japan JPX-Nikkei 400 Leveraged (2x) Index',2,1),
(jp400_index,1465,'Daiwa ETF Japan JPX-Nikkei 400 Inverse (-1x) Index',-1,1),
(jp400_index,1466,'Daiwa ETF Japan JPX-Nikkei 400 Double Inverse (-2x) Index',-2,1),
(jp400_index,1467,'JPX-Nikkei 400 Bull 2x Leveraged ETF',2,10),
(jp400_index,1468,'JPX-Nikkei 400 Bear -1x Inverse ETF',-1,10),
(jp400_index,1469,'JPX-Nikkei 400 Bear -2x Double Inverse ETF',-2,10),
(jp400_index,1470,'NEXT FUNDS JPX-Nikkei 400 Leveraged Index Exchange Traded Fund',2,1),
(jp400_index,1471,'NEXT FUNDS JPX-Nikkei 400 Inverse Index Exchange Traded Fund',-1,1),
(jp400_index,1472,'NEXT FUNDS JPX-Nikkei 400 Double Inverse Index Exchange Traded Fund',-2,1),
)

mic_tse = sym_models.MicExchange.objects.get(exchange='XTKS')
gh_tse = sym_models.GhExchange.objects.get(exchange='TSE', mic_exchange=mic_tse)
activ_tse = sym_models.ActivExchange.objects.get(exchange='ET', mic_exchange=mic_tse)


for e in etfs:
    print('ETF : {}'.format(e))

for index, ticker, name, beta, trading_unit in etfs:
    def bool2str(boolean):
        return 'created' if boolean else 'already exists'

    print '-----------------------------------------------------------'

    underlying, is_underlying_created = Contract.objects.get_or_create(type=Contract.CONTRACT_TYPE_INDEX, exchange='TSE', ticker=ticker, currency='', point_value=0, trade_cost_per_lot=0, cost_currency='')
    contract, is_contract_created = Contract.objects.get_or_create(type=Contract.CONTRACT_TYPE_STOCK, exchange='TSE', ticker=ticker, currency='JPY', point_value=1, trade_cost_per_lot=0.000085, cost_currency='JPY', underlying_contract=underlying)
    contractgroup, is_contractgroup_created = ContractGroup.objects.get_or_create(name=('tse_etf_%s' % ticker), contract1=contract)
    riskproduct, is_riskproduct_created = RiskProduct.objects.get_or_create(name=name, symbol=ticker, exchange='TSE', type=Contract.CONTRACT_TYPE_STOCK)
    print '[underlying Contract] %s %s' % (underlying, bool2str(is_underlying_created))
    print '[Contract] %s %s' % (contract, bool2str(is_contract_created))
    print '[ContractGroup] %s %s' % (contractgroup, bool2str(is_contractgroup_created))
    print '[RiskProduct] %s %s' % (riskproduct, bool2str(is_riskproduct_created))

    gh_instr, is_gh_created = sym_models.GhInstrument.objects.get_or_create(exchange=gh_tse, instrument_type=1, symbol=ticker, symbol_maturity_format='', tick_size_dynamic=1, round_lot=trading_unit)
    native_instr, is_native_created = sym_models.NativeInstrument.objects.get_or_create(exchange=mic_tse, symbol_prefix='', symbol=ticker, symbol_postfix='', instrument_type=1, gh_instrument=gh_instr)
    activ_instr, is_activ_created = sym_models.ActivInstrument.objects.get_or_create(exchange=activ_tse, symbol=ticker, instrument_type=1, gh_instrument=gh_instr)

    print '[GhInstrument] %s %s' % (gh_instr, bool2str(is_gh_created))
    print '[NativeInstrument] %s %s' % (native_instr, bool2str(is_native_created))
    print '[ActivInstrument] %s %s' % (activ_instr, bool2str(is_activ_created))

    if add_to_etf_page:
        if beta > 1:
            etf_type = etf_models.Etf.TYPE_BULL_NO_PCF
        elif beta <= -1:
            etf_type = etf_models.Etf.TYPE_BEAR_NO_PCF
        else:
            etf_type = etf_models.Etf.TYPE_CLASSICAL_NO_PCF

        etf, is_etf_created = etf_models.Etf.objects.get_or_create(underlying_instr=index, code=ticker, ticker=('%s.ET' % ticker), name=name, type=etf_type, beta=beta, create_redeem_fee=0.1, sell_settlement_period=3)
        etfoffset, is_etfoffset_created = etf_models.EtfOffset.objects.get_or_create(etf=etf)

        print '[Etf] %s %s' % (etf, bool2str(is_etf_created))
        print '[EtfOffset] %s %s' % (etfoffset, bool2str(is_etfoffset_created))
    else:
        print 'Skipped creating Etf and EtfOffset.'