if [[ -z $ZSH_VERSION ]];
then
    if [ -f ~/.bash_colors ]; then source ~/.bash_colors ; fi
    if [ -f ~/.bash_scripts ]; then source ~/.bash_scripts ; fi
fi


# RECON WEB Tests
# A lot of curls which are not needed anymore
alias cu='curl -Fclient=GH -F"Newedge SGX T+1.2015-03-05.zip=@/private/var/reconwise/emails/Newedge SGX T+1.2015-03-05/Newedge SGX T+1.2015-03-05.zip" localhost:8000/confo/upload/'
alias cuf='curl -Fclient=GH -F"Newedge SGX T+1.2015-03-05.zip=@/private/var/reconwise/emails/Newedge SGX T+1.2015-03-05/Newedge SGX T+1.2015-03-05.zip" localhost:8000/confo/upload/'
alias ru='curl -Fclient=GH -F"Daily Statements 2015-02-25=@/private/var/reconwise/emails/Daily_Statements_2015-02-25.xls" localhost:8000/confo/upload/'
#alias rut='curl -Fclient=GH -F"Daily Statements 2015-02-25=@/private/var/reconwise/emails/Daily_Statements_2015-02-25.xls" tildesg.ddns.net:80/confo/upload/'
alias xx='curl -Fclient=GH -F"Newedge SGX T+1.2015-03-16=@/var/reconwise/emails/Newedge SGX T+1.2015-03-16.zip" tildesg.ddns.net:80/confo/upload/'
alias rpp='curl -F"Daily Statements 2015-02-25=@/private/var/reconwise/emails/Daily_Statements_2015-02-25.xls" localhost:8000/parser/parse/'
alias rcp='curl -F"Daily Statements 2015-02-25=@/private/var/reconwise/emails/Daily_Statements_2015-02-25.xls" localhost:8000/confo/parse/'
alias rpu='curl -F"Daily Statements 2015-03-16=@/tmp/test.csv" localhost:8000/parser/upload/'
alias rpu1='curl -F"Daily Statements 2015-03-16=@/tmp/test.csv" localhost:8000/confo/upload/'
alias rfv='curl -F"grasshopper-fair_value-20150311_010101=@/var/reconwise/ftp/grasshopper-fair_value-20150311_010101.csv" localhost:8000/client/fairvalue/'

export V1_SRC='/Users/apandey/code/tilde/reconwisev1/src'
export V2_SRC='/Users/apandey/code/tilde/reconwisev2/rw'

# Db setup
# pg_dump -d tam_tech_1 -U djangouser -O > /Users/apandey/code/tilde/reconwisev1/src/main/scripts/tam_tech_1.psql
# psql  -h reconwise.cwobo6amaswz.ap-southeast-1.rds.amazonaws.com -U reconuser -d tam_tech_1 < /Users/apandey/code/tilde/reconwisev1/src/main/scripts/tam_tech_1.psql



set_upload_site()
{
    SLOCAL='http://localhost:8000'
    SAWS='http://tildesg.ddns.net'
    SITE=${1-$SLOCAL}
    # echo $SITE
}
set_upload_site

confoload()
{
    # /var/reconwise/emails/Morgan Stanley TSE Stocks.2015-05-01.zip
    FULL=$*
    echo file=$FULL
    FILE=$(basename "$FULL")
    echo file=$FILE
    curl -Fclient=GH -F"$FILE=@$FULL" $SITE/confo/upload/
}
alias reconmsload='confoload'
reconneload()
{
    FULL=$*
    echo file=$FULL
    FILE=$(basename "$FULL")
    echo file=$FILE
    curl -Fclient=GH -F"$FILE=@$FULL" $SITE/parser/upload/ > /tmp/file_results
    curl -Fclient=GH -F"$FILE=@/tmp/file_results" $SITE/parser/upload/
}

alias rw_abhi='vim $V1_SRC/main/resources/tmp/abhishek.pandey_tampinestechnology.com'

# SCRIPTS
alias read_mail='p3 /Users/apandey/code/scripts/read_abhi_mail.py'
alias scripts='ll ~/code/scripts/'

pathadd()
{
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${PATH:+"$PATH:"}$1"
    fi
}

# Powerline
#powerline-daemon -q
#export POWERLINE_CONFIG=/Library/Frameworks/Python.framework/Versions/3.4/bin//powerline-config
#POWERLINE_BASH_CONTINUATION=1
#POWERLINE_BASH_SELECT=0
#source /Library/Frameworks/Python.framework/Versions/3.4/lib/python3.4/site-packages/powerline/bindings/bash/powerline.sh

export EC2_HOME=/usr/local/ec2
export RW=/var/reconwise/
export NGINX=/usr/local/Cellar/nginx/1.6.1_1/
export NG2=/usr/local/etc/nginx/
export TMOUT=0
pathadd /Library/Frameworks/Python.framework/Versions/3.4/bin/
pathadd /usr/local/mysql/bin
pathadd /Users/apandey/code/scripts/
pathadd $EC2_HOME/bin
pathadd /Applications/Postgres.app/Contents/Versions/9.4/bin
pathadd .
pathadd /Users/apandey/q/m32/
export PG_DUMP="/Applications/Postgres.app/Contents/Versions/9.4/bin/"
#export PATH=.:/Library/Frameworks/Python.framework/Versions/3.4/bin/:/usr/local/mysql/bin:$PATH
#export PATH=/Applications/Postgres.app/Contents/Versions/9.4/bin/:$PATH
#export PATH=/Users/apandey/code/scripts/:$PATH

export LD_LIBRARY_PATH=./lib/:$TIBRV_HOME/lib/:/bar/deps/thirdparty/google-test-framework/Linux/x86_64/2.6/gcc411_libc25/lib/:$LD_LIBRARY_PATH

#Long Version of Putty Title
#PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
PROMPT_COMMAND='echo -ne "\033]0;${PWD}\007"'

# MARKET WISE Stuff
alias tm='p2 /Users/apandey/code/marketwise/manage.py runserver'
#tng()
#{
    # Test NGINX - start up uwsgi
    # source /Users/apandey/code/venv34_1/env27_1/bin/activate
    # /Users/apandey/code/venv34_1/env27_1/bin/uwsgi --ini /Users/apandey/code/venv34_1/env27_1/uwsgi.ini
#}

# DJANGO HELP
#export DJANGO_SETTINGS_MODULE='web.settings'
export SE='/usr/local/etc/nginx/sites-enabled/'
export PYTHONPATH=.:test/python/reconwisetestPy:main/python:/Users/apandey/code/scripts/
alias rundj='p2 /Users/apandey/code/marketwise/manage.py runserver'
alias rundjt='p2 /Users/apandey/code/marketwise/manage.py test'
alias dj2='p2 /Users/apandey/code/marketwise/manage.py '
alias tag_rw='git tag -f v2_uat master && git push -f --tags'


# AWS Help
# scp -i  /Users/apandey/Documents/TamTech/tamtech.pem .vimrc ec2-user@tildesg.ddns.net:/tmp/abhivimrc
# scp -i  /Users/apandey/Documents/TamTech/tamtech.pem ec2-user@tildesg.ddns.net:/tmp/pgdump.tdb.sql ./
# ssh -i ~/Documents/TamTech/tamtech.pem ec2-user@54.169.4.132
alias qtt='conaws tamtech.ddns.net'  # 52.74.187.73
alias qt='conaws recon.tilde.sg'  # 52.74.222.148'
alias qs='sftp -i /Users/apandey/Documents/TamTech/grasshopper.pem -P 22 gh-user@tildesg.ddns.net'
alias qs1='sftp -i /Users/apandey/Documents/TamTech/grasshopper.pem -P 22 gh-user@recon.elasticbeanstalk.com'
alias ftp_ms='sftp -i /var/reconwise/certs/morganstanley_sftp_id_rsa  wakyedco@sftp.morganstanley.com'
alias ftp_abn='sftp -i /Users/apandey/Documents/TamTech/key_files/abnamro grasshopper@delivery.au.abnamroclearing.com'
#alias qs='sftp -o ConnectTimeout=1000000 -i /Users/apandey/Documents/TamTech/grasshopper.pem -P 22 gh-user@tildesg.ddns.net'
# export PATH=$PATH:$EC2_HOME/bin
export AWS_ACCESS_KEY=AKIAJUFS5D5FXNQ7CI6Q
export AWS_SECRET_KEY=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
export EC2_URL=https://ec2.ap-southeast-1.amazonaws.com
alias ship='ssh -o ConnectTimeout=1000000 ship@tildesg.ddns.net' #Kx7OlJIZ
conaws()
{
    ssh -o ConnectTimeout=1000000 -i /home/abhishek.pandey/code/reconwisev1/src/main/resources/tamtech.pem ec2-user@$1

    #scp command to aws
    #scp -i ~/Documents/TamTech/tamtech.pem ec2-user@tildesg.ddns.net:/tmp/jsons.tar.gz .
}

# Simplest verion of PS1
#export PS1="`uname -n`> "
#export PS1='\u@\h> '

# Simpler Version to see dir details on 1 line
#export PS1='\u@\h\ : \w \n>'

# Most Complicated version
# PS1='\e[1;31;34m\e[1;31;36m\h: \e[0;0m\e[1;35m[\w]\e[0;0m\n\u > '
function _update_ps1()
{
   export PS1="$(~/powerline-shell.py $? 2> /dev/null)"
   export PS1="[abhi@\W]\\$ "
}

export PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"

export LC_CTYPE=C
export LANG=C


# Using pip to install stuff
# sudo STATIC_DEPS=true python3.4 -m pip install xlwt
# sudo python3.4 -m pip install xlwt
# sudo python3.4 -m pip install watchdog
# sudo python3.4 -m pip install Django==1.7.1
# sudo python3.4 -m pip install mysql-connector-python

alias ptt='pdftotext -nopgbrk -layout "$FILE" new.txt'
alias ls='ls -G'
#alias python='python3.4'
alias p3='python3.4'
alias p2='python2.7'
alias pt3='py.test-3.4'




# FTP Commands
# Enable it
# sudo -s launchctl load -w /System/Library/LaunchDaemons/ftp.plist

# Disable it
# sudo -s launchctl unload -w /System/Library/LaunchDaemons/ftp.plist
#######################################################################################################################################

# export JAVA_HOME=/usr/java/jdk1.6.0_13/
# export JAVA_HOME=/usr/lib64/jvm/jre-1.5.0-sun
# for mamajni builds JAVA_HOME needs to point at the sdk
# so in that case use JAVA_HOME=/usr/lib64/jvm/java
# or do something like "env | grep -i java" and look for SDK_HOME



# grep stuff
export grep='grep --color=auto'


# =====================================================================================================================================
alias lllogs='echo ~/jnx.pbs/logs/ && ll ~/jnx.pbs/logs/'

# Set the process to a single core - Core #0
alias cpuset='taskset -c -p 0 '
export CC=gcc
export GCC=gcc
alias ls='ls --color=auto'
alias wl='watch -n 60 ls'
# aliai
alias sortu='sort -u'
alias vc='vim config*.xml'
alias vmake='vim Makefile'
alias gz='zipcode'
# This is for google test
alias k1='kil %1'
alias fk1='fkil %1'
alias k2='kil %2'
alias fk2='fkil %2'
alias namefind='find ./ -name '
alias dirs='ls -d */'
alias lsa='ls -a'
alias igrep='egrep -i'
alias myps='ps -u $(whoami) | egrep -v " \/System| \/Users\/| \/Applications| \/usr\/|ssh|bash| ps$|grep$"'
alias kil='kill -15'
alias fkil='kill -9'
alias pykil='kill -s INT '
kildj()
{
    ps -u apandey | grep -e "manage.py runserver" | awk '{print $2}' | xargs kill -9
}
alias kdj='kildj'
alias disk='du -sh'
alias lsize='ls -lshrt'
alias chgv='chg && v'
alias llogs='ll *.log'
alias txt='ll *.txt'
alias tars='ls *.tar'
alias zips='ls *.zip'
alias gzs='ls *.gz'
alias rpwd='pwd -P'
alias mybash='pwd && vim ~/.bashrc && . ~/.bashrc'
alias b='mybash'
alias bscript='vim ~/.bash_scripts && . ~/.bashrc'
alias myvim='pwd && vim ~/.vimrc'


# Directory traversal
alias 1cd="cd ../ && pwd"
alias 2cd="cd ../../ && pwd"
alias 3cd="cd ../../../ && pwd"
alias 4cd="cd ../../../../ && pwd"
alias 5cd="cd ../../../../../ && pwd"
alias 6cd="cd ../../../../../../ && pwd"
alias 7cd="cd ../../../../../../../ && pwd"

alias gt='gnome-terminal'
alias l="ls -F"
alias ll="l -lh"
alias lll='ll -rt | tail'
alias whos='who | sort | uniq -cw 10'
# alias whichos=~/triage.*/src/devtools/wauto/whichos.sh


# =====================================================================================================================================
# fix bug in gnome-terminal where ctrl-S for readline i-search does not work
# because ctrl-S is used by gnome-termial for some flow control nonesense
# e.g see http://www.mail-archive.com/debian-user@lists.debian.org/msg63190.html
# or https://bugs.launchpad.net/ubuntu/+source/bash/+bug/48880
#stty stop ''
#stty start ''

# allow unlimited core files
ulimit -c unlimited

# command history
#export HISTFILESIZE=999999
export HISTFILESIZE=4000000
export HISTSIZE=100000
export HISTCONTROL=ignoredups

# Multiple-Hop SCP (How To SCP Via A Gateway)

# From localhost to remotehost...
#   scp -Cp -o "ProxyCommand ssh gatewayuser@gateway nc remotehost 22" file remoteuser@remotehost:/some/where/file

# ...and back again.
#   scp -Cp -o "ProxyCommand ssh gatewayuser@gateway nc remotehost 22" remoteuser@remotehost:/some/where/file file_copy
# =====================================================================================================================================
lcd()
{
    cd $*
    ls
}
# Creates a gzip file containing all files (including API)
zipall()
{
    tar cf $1.tar --exclude=CVS $1
    gzip -9 $1.tar
}

# Creates a gzip file containing all files (excluding API)
zipcode()
{
    tar cf $1.tar --exclude=CVS --exclude=docs --exclude=doc --exclude=api $1
    gzip $1.tar
}


powerline-daemon -q
#source /Library/Frameworks/Python.framework/Versions/3.4/lib/python3.4/site-packages/powerline/bindings/bash/powerline.sh
if [  -f /Library/Frameworks/Python.framework/Versions/3.4/lib/python3.4/site-packages/powerline/bindings/bash/powerline.sh  ]; then
    source /Library/Frameworks/Python.framework/Versions/3.4/lib/python3.4/site-packages/powerline/bindings/bash/powerline.sh
fi




# Help commands for reconwise
rw()
{
    if [[ $1 == '1' ]];
    then
        echo 'Copying all Recon* files from main/resources/tmp folder'
        echo cp -v "$V1_SRC/main/resources/tmp/reports/json/Reconcile_"* "$V1_SRC/main/resources/qa/"
        cp -v "$V1_SRC/main/resources/tmp/reports/json/Reconcile_"* "$V1_SRC/main/resources/qa/"
    elif [[ $1 == '2' ]];
    # Most useful for copying 848 and 849 to QA folders. Make sure to run dev test in range 800-871 first
    then
        echo 'Copying 848 and 849 files from /var/reconwise/'
        echo cp -v /var/reconwise/reports/json/*84[89]* $V1_SRC/main/resources/qa/json/
        cp -v /var/reconwise/reports/json/*84[89]* $V1_SRC/main/resources/qa/json/
    elif [[ $1 == '3' ]];
    then
        echo 'Copying 848 and 849 files from within main/resources/tmp/ folder'
        echo cp -v $V1_SRC/main/resources/tmp/reports/json/*84[89]* $V1_SRC/main/resources/qa/json/
        cp -v $V1_SRC/main/resources/tmp/reports/json/*84[89]* $V1_SRC/main/resources/qa/json/
    elif [[ $1 == '4' ]];
    then
    # Most useful for copying 871 and 875 files to QA folders. make sure to run pytest first.
        echo 'Copying 871 and 875 files from within main/resources/tmp/ folder'
        cp -v $V1_SRC/main/resources/tmp/reports/json/*87[15]* $V1_SRC/main/resources/qa/
    elif [[ $1 == '5' ]];
    then
    # Most useful for copying all files to QA folders. make sure to run pytest first.
        echo 'Copying ALL files from within main/resources/tmp/ folder'
        cp -v $V1_SRC/main/resources/tmp/reports/json/* $V1_SRC/main/resources/qa/
    fi

    #cp -v "$V1_SRC/main/resources/tmp/reports/json/Reconcile_"* "$V1_SRC/main/resources/qa/"
    #cp -v /var/reconwise/reports/json/*84[89]* $V1_SRC/main/resources/qa/json/
}

pretty_csv()
{
    column -s, -t <$1

    # Alternative
    # python -c 'import sys,csv; c = csv.reader(sys.stdin); [sys.stdout.write("^M".join(map(repr,r))+"\n") for r in c];' <$1 | column -s '^M' -t
}
as()
{
        echo "$*" >> samples.txt
}

alias v1='source /Users/apandey/code/virtual_envs/rwv1/bin/activate && cd ~/code/tilde/reconwisev1/ && echo "VENV=/Users/apandey/code/virtual_envs/rwv1/bin" '
alias v2='source /Users/apandey/code/virtual_envs/rwv2_34_1/bin/activate && cd ~/code/tilde/reconwisev2/rw/ && echo "VENV=/Users/apandey/code/virtual_envs/rwv2_34_1/bin" '
dumpv2()
{
    v2 # This will call the alias act to set up the virtual env for RW V2
    manage.py dumpdata --database=orig --indent 4 contenttypes auth > media/fixtures/authorizations.json
    manage.py dumpdata --database=orig --indent 4 confo.Broker confo.Client > media/fixtures/parties.json
    manage.py dumpdata --database=orig --indent 4 confo.Contract confo.ContractSymbol confo.Session confo.TradeAdapter > media/fixtures/trades_and_contracts.json
}

t_1()
{
    file_functions "$*"
    TMP_CSV='/tmp/recon.csv'
    # FILE='Daily Statements 2015-05-12.xls'
    # FULL='/var/reconwise/emails/Daily Statements 2015-05-12.xls'
    curl -Fclient=GH -F"$FILENAME=@$FULL_FILENAME" $SITE/parser/parse/ > $TMP_CSV
}
t_2()
{
    curl -Fclient=GH -F"$FIRST_PART.csv=@$TMP_CSV" $SITE/parser/upload/ &
}
file_functions()
{
    # FILENAME='/var/reconwise/emails/Daily Statements 2015-05-12.xls'
    FILENAME="$*"
    FULL_FILENAME=$FILENAME
    FILENAME=${FULL_FILENAME##*/}
    FIRST_PART=${FILENAME%%.*}
    EXTENSION="${FILENAME#*.}"
    echo just file part = $FIRST_PART
    echo filename= $FILENAME
    echo extension= $EXTENSION
}


aa()
{
    grep "^$*" /tmp/1 | sed -e 's/[^A-Z ]//g' -e 's/^/'\''/g' -e 's/$/'\''/g' | xargs -n 1 ~/code/scripts/cam | sed -e 's/^_*/'\''/g' -e 's/_*$/'\''/g' -e 's/_d_c//g' | awk 'BEGIN {ORS = ", "}{print $0}'
}

alias vd='vim Dockerfile'
alias b2='boot2docker'
dinit()
{
    # If you get a tcp error again, do "b2 restart"
    b2 stop
    b2 init
    b2 start
    $(b2 shellinit)

}
alias di='docker images'
alias dr='docker run -t -i tamtech/${PWD##*/} /bin/bash'
alias drr='docker run -d -p 80:80 --name ts tamtech/${PWD##*/}'
alias drbash='docker exec -i -t ts bash'
alias dstop='docker stop ts && docker rm  ts'
alias db='echo "Building tamtech/${PWD##*/}" && docker build -t tamtech/${PWD##*/} .'
alias dp='docker ps'
alias dirmi='di | awk '\''{if ($1=="<none>") print $3}'\'' | xargs docker rmi -f'
# export DOCKER_HOST=tcp://127.0.0.1:2376
alias l0='curl 127.0.0.1:8000'
alias l1='curl 127.0.0.1:8001'
alias c0='curl localhost:8000'
alias c1='curl localhost:8001'

slash()
{
    echo $1 | sed -e 's/\//./g'
}
alias mig='python dbc.py tam_tech_1.mysql tt.psql'
alias zdock='zip old.zip * .eb*'

zdok()
{
    # cd /Users/apandey/code/scripts/docks/sina
    rm *.zip
    COUNTER_FILE=/var/reconwise/.dock
    if [[ -r $COUNTER_FILE ]] ; then
       COUNT=$(<$COUNTER_FILE)
    else
       COUNT=0
    fi
    echo 'count = ' $COUNT

    #Increment counter and save to file 
    echo $(( $COUNT+ 1 )) > $COUNTER_FILE
    zip recon$COUNT.zip * ./.eb*/*
}


## sudo /usr/local/Calpont/bin/setConfig WriteEngine AllowVarbinary Yes
## sudo /usr/local/Calpont/bin/calpontConsole RestartSystem

alias idbmysql='/usr/local/Calpont/mysql/bin/mysql --defaults-file=/usr/local/Calpont/mysql/my.cnf -u root'

