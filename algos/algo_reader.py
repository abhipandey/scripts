import param_descriptions
import re

import fnmatch
import os
from functools import reduce
import operator


_PATTERN_CREATOR = lambda s, splitter: '|'.join(['({}) (.*){}'.format(x, splitter) for x in s.split('|')])

_PARAM_PATTERN = _PATTERN_CREATOR('EnumParam<.*>|BoolParam|Int64Param|StringParam|Dec64Param|ObiParam|DateTimeParam|AlgoParam|InstParam', ';')

_FOLDER = '/home/abhishek.pandey/apps/code/higgs/ccgh/hummer/src/gng/strategy/'



class Param:
    def __init__(self, name, param_type):
        self.name = name
        self.param_type = param_type
        self.desc = ''

    def __repr__(self):
        return str(self)
    def __str__(self):
        return '{} <{}>'.format(self.name, self.param_type)

def get_file_path(path_to_search):
    matches = []
    for root, dirnames, filenames in os.walk(_FOLDER):
        for filename in fnmatch.filter(filenames, path_to_search):
            matches.append(os.path.join(root, filename))
    return matches[0]


def get_file_text(file_path):
    f_first, ext = os.path.splitext(file_path)
    with open(file_path, 'rt') as file_placeholder:
        lines = file_placeholder.readlines()
        text = ''.join(lines)
        return text

def get_file_lines(file_path):
    f_first, ext = os.path.splitext(file_path)
    with open(file_path, 'rt') as file_placeholder:
        return file_placeholder.readlines()


def get_param_with_descriptions(file_path_to_parse):

    def _get_param_names():
        text = get_file_text(get_file_path(file_path_to_parse + '.h'))
        res = [reduce(lambda x,y: x + ' ' + y, list(f), '').strip().replace('Param ', ' ') for f in re.findall(_PARAM_PATTERN, text, re.MULTILINE )]
        res2 = [[(x) for x in f.split()] for f in res]
        items = []
        for x, y in res2:
            items.append(Param(y, x))
        return items


    def _add_descriptions(params):
        names = [p.name + '(' for p in params]
        text = get_file_lines(get_file_path(file_path_to_parse + '.cc'))
        param_name = ''
        for line in text:

            line = line.strip()
            red_line = line.replace('(', ' ').\
                replace(')', ' ').\
                replace(':', ' ').\
                replace('{', ' ').\
                replace('}', ' ').\
                replace(']', ' ').\
                replace('[', ' ').\
                replace('=', ' ').\
                replace(',', ' ')
            red_line = red_line.split()
            if any([line.startswith(n) and n for n in names]):
                param_name = line[:line.index('(')]
            if param_name:
                desc = [p for p in param_descriptions.ParamList.__dict__ if p in red_line]
                if desc:
                    desc = desc[0]
                    param = [p for p in params if p.name == param_name][0]
                    param.desc = param_descriptions.ParamList.__dict__[desc].doc
                    # print('p. desc = {}'.format(param.desc))


    param_list = _get_param_names()
    _add_descriptions(param_list)
    return param_list

all_params = get_param_with_descriptions('stackmanagerparams')
all_params += get_param_with_descriptions('basicspreadinitiatorparams')
all_params += get_param_with_descriptions('singlespreadinitiatorparams')


print('''
| Variable | Type | Description | Remarks |
| --- | --- | --- | --- |''')
for p in sorted(all_params, key=lambda p: p.name):
    print('| **{}** | {} | {} | |'.format(
    p.name.replace('_', '-'),
    p.param_type.replace('Param<', ':').replace('>', ''),
    p.desc))
