

class ParamDef:
    def __init__(self, name, doc):
        self.name = name
        self.doc = doc

    def __str__(self):
        return self.name


class ParamList:
    kState = ParamDef("state", "State of the algo")


    kDisplayNameAddOn = ParamDef("display-name-add-on",
        "Add on for algo display name")


    kOrderCountTime1= ParamDef("order-count-time1", "Time frame 1 in ms")

    kOrderCountCount1 = ParamDef("order-count-count1",
        "Maximum allowed number of orders placed "
        "within time frame 1. Stop algo when "
        "breached.")

    kOrderCountTime2 = ParamDef("order-count-time2", "Time frame 2 in ms")
    kOrderCountCount2 = ParamDef("order-count-count2",
        "Maximum allowed number of orders placed "
        "within time frame 2. Stop algo when "
        "breached.")

    kOrderCountWarnRatio = ParamDef("order-count-warning-ratio",
        "Display warning notification when order "
        "count1/2 breaches self proportion of max "
        "allowed orders.")


    kMaxFillsTime1= ParamDef("max-fills-time1", "Time frame 1 in ms")

    kMaxFillsCount1 = ParamDef("max-fills-count1",
        "Maximum allowed number of fills within time "
        "frame 1. Stop algo when breached.")

    kMaxFillsTime2 = ParamDef("max-fills-time2", "Time frame 2 in ms")
    kMaxFillsCount2 = ParamDef("max-fills-count2",
        "Maximum allowed number of fills within time "
        "frame 2. Stop algo when breached.)")

    kMaxFillsWarnRatio = ParamDef("max-fills-warning-ratio",
        "Display warning notification when fill "
        "count1/2 breaches self proportion of max "
        "allowed orders.")


    kMaxOrderSize= ParamDef("max-order-size", "Fat finger limit for order")



    kRelativeLimit = ParamDef("relative-limit",
        "Allowed to buy at max relative-limit percent above last trade price. "
        "Allowed to sell at max relative-limit percent below last trade price.")

    kLastTradeAge = ParamDef("last-trade-age",
        "Effective last trade age. Disregard last trade "
        "guard if last trade age is older.")

    kActive = ParamDef("active",
        "In active mode, will add, and "
        "remove orders as the market moves. In passive mode, "
        "orders will only be removed when market moves closer "
        "to the stack. In passive mode the algo needs to be "
        "started before it acquires orders.")
    #==================  Algo  ==================


    kActiveStopper = ParamDef(
        "active",
        "In active mode, will keep sending stop orders as the market "
        "changes. In passive mode, orders will be sent except by manual "
        "firing.")

    kActiveInst = ParamDef("active-instrument", "Active Instrument")

    kActiveStopSpread = ParamDef(
        "active-stop-spread",
        "Difference between active-instrument limit price and stop-instrument "
        "tradeout price (active - tradeout price)")

    kAeMinLeanVol = ParamDef("ae-min-lean-vol", "ae-min-lean-vol")

    kAggressiveEdgeDelay = ParamDef(
        "aggressive-delay",
        "MS delay when in aggressive mode. This will make "
        "it cover immediately if set to 0 in aggressive mode.")

    kAggressiveThreshold = ParamDef(
        "aggressive-threshold",
        "Cross spread to initiate trade if reference (non-active) contract work "
        "size is less than aggressive-threshold. Larger value means a more "
        "aggressive eye. Example: for a buy Eye, Eye will buy the spread when "
        "the offer size is less than self threshold. When self feature is "
        "activated, wait-for-hedge=True is forced. Set to 0 to disable self "
        "feature.")

    kAggressorSide = ParamDef(
        "aggressor-side", "Aggressor Side(Buying at offer or Selling at bid")

    kAllowedDelta = ParamDef("allowed-delta",
        "Don't hedge smaller deltas. Should not be set to "
        "< 0.5 as that could result in a bad feedback "
        "loop of buy/sell hedges. ")

    kAlwaysHedge = ParamDef(
        "always-hedge",
        "If an order has an error, the other contract until flat and then "
        "continue running normally.")

    kArbSpread = ParamDef(
        "arb-spread",
        "Minimum price difference between orders that are crossing trading and "
        "auction contract")

    kAuctionInst1 = ParamDef("auction-instrument1", "Auction Instrument1")
    kAuctionInst2 = ParamDef("auction-instrument2", "Auction Instrument2")

    kAuctionLeanThreshold = ParamDef(
        "lean-threshold",
        "Computes the auction matching (hedge) price assuming lean-threshold was "
        "being hedged. This price is used to see if an arb exists against the "
        "current BBO. 0 makes algo use size for the above.")

    kAuctionQty1 = ParamDef("auto-auction-qty1",
        "Quantity on auction Instrument1")

    kAuctionQty2 = ParamDef("auto-auction-qty2",
        "Quantity on auction Instrument2")

    kAutomaticDeltaToSecondQty = ParamDef(
        "auto-delta-to-second-qty",
        "Quantity taken from service. 1 delta to second instrument qty.")

    kAutomaticFirstQty = ParamDef("auto-first-qty",
        "Quantity of first instrument in relation to "
        "second instrument, from service.")

    kAutomaticHedgeRatio = ParamDef(
        "auto-hedge-ratio", "Hedge ratio taken from service for 2 instruments")

    kAutomaticSecondQty = ParamDef("auto-second-qty",
        "Quantity of second instrument in relation "
        "to first instrument, from service.")

    kBatchIntervalMs = ParamDef(
        "batch-interval-ms",
        "millisecond interval between batches of order-placement, values "
        "are allowed for sub-millisecond intervals eg 0.1 = 100us ")

    kBigMoveExpiry = ParamDef("big-move-expiry",
        "How long a big move is still considered active")

    kBuyLevels = ParamDef(
        "buy-levels", "Number of BID ticks down from buy-price to fire into")

    kBuyOrSellSpread = ParamDef("buy-or-sell-spread", "Buy/Sell the Spread")

    kBuyMaxTrades = ParamDef(
        "buy-max-trades",
        "Maximum volume to fill on buy side, algo "
        "when filled volume on buy side since start is more "
        "than buy-max-trades. 0 will disable the guard.")

    kBuyOffset = ParamDef("buy-offset",
        "Offset to modify buy side, negative "
        "value to buy lower, to buy higher")

    kBuyPrice = ParamDef("buy-price", "Price for first level BUY orders")

    kBuySize = ParamDef("buy-size", "Size for buy orders")

    kBuySpread = ParamDef("buy-spread",
        "First - second / First/Second spread to buy")

    kBuySpreadPriceLimit = ParamDef("buy-spread-price-limit",
        "Highest bid/lowest offer for buy-spread")

    kChaseCut = ParamDef("chase-cut",
        "If market moves more than follow-ticks and delta is "
        "not hedged out a limit order is sent chase-cut "
        "percent away from the market price. It should always "
        "be smaller than relative-limit")

    kConstantOffset = ParamDef(
        "constant-offset",
        "Constant offset first_price = ratio(second_price - "
        "efp) + constant. Same as the normal spread efp = 0, ratio = 1")

    kCorrection = ParamDef("correction",
        "Assumption proportional change in correlated "
        "instrument : price =  price + num_ticks * "
        "correction")

    kCoverNow = ParamDef("cover-now", "Must cover now")

    kCurrencyHedger = ParamDef("currency-hedger",
        "Hedger to hedge the currency")

    kCurrencyInst = ParamDef("currency-instrument",
        "Active Price = Pricer(Hedge Instr) * "
        "Currency Instr Price, Pricer is typically "
        "spread-based or ETF-based")

    kCutoffOffsetMs = ParamDef("cutoff-offset-ms",
        "Cutoff time offset in ms, accepted")

    kCutoffTime = ParamDef("cutoff-time",
        "Cutoff time for initiating and "
        "swapping hedge orders")

    kDelta = ParamDef("delta", "Current delta to be hedged")

    kDeltaToFirstQty = ParamDef("delta-to-first-qty",
        "1 delta to first instrument qty")

    kDeltaToSecondQty = ParamDef(
        "delta-to-second-qty",
        "1 delta to second instrument qty. Second hedger delta will be "
        "calculated using notional values if self parameter is not set.")

    kDistribution = ParamDef("distribution",
        "Distribution of delta in first:second contract")

    kEfp = ParamDef(
        "efp",
        "Only to be set when using an ETF pricer. EFP to compute "
        "index in the first_price = ratio(second_price - efp) + "
        "constant. Use 0 for Futures, and Inverse ETFs")

    kEnableModify = ParamDef("enable-modify", "enable-modify")

    kEquilibriumInstrument = ParamDef(
        "equilibrium-instrument",
        "The first equilibrium-price message from self instrument starts interval "
        "firing after elapse of offset-ms period, self empty to disable self "
        "feature")

    kExitRatio = ParamDef(
        "exit-ratio",
        "Exit if both exit-ratio and exit-threshold are met, i.e.:\n"
        "1. (lean volumn / (work volumn + delta)) < exit ratio, and\n"
        "2. (lean volumn - delta) < exit threshold\n"
        "\n"
        "Else work for an extra tick.")

    kExitThreshold = ParamDef("exit-threshold", kExitRatio.doc)

    kFillDeductionLevels = ParamDef(
        "fill-deduction-levels",
        "Use L1 last trade to deduce fills. This controls the number of levels to "
        "deduce w.r.t top of stack. Set 0 to disable. To enable, must either "
        "be inactive or max-trades=1.")

    kFinalSize = ParamDef("final-size", "Final size to shave down to.")

    kFireOnAccepting = ParamDef(
        "fire-on-accepting",
        "Fire remaining size up to max-hits on phase change, or "
        "order accepted.")

    kFirstInst = ParamDef("first-instrument", "First Instrument")

    kFirstLeanThreshold = ParamDef("first-lean-threshold",
        "First Lean Threshold")

    kFirstMaxSize = ParamDef("first-max-size", "First Max Size")

    kFirstQty = ParamDef("first-quantity", "Quantity on first Instrument")

    kFlashIntervalMs = ParamDef(
        "flash-interval-ms",
        "Interval between order placements in ms, accepted. Set to 0 "
        "to disable flashing and use quotes.")

    kFollowMarket = ParamDef(
        "follow-market",
        "Follow market using exit-threshold, exit-ratio, follow-ticks and other "
        "market driven indicators to hedge out if True. Just place one order at "
        "the indicated price if set to False (like SimpleHedger)")

    kFollowTicks = ParamDef("follow-ticks",
        "Number of ticks to follow the market. If market "
        "moves more than follow-ticks and delta is not "
        "hedged out, order using pay-up-ticks will be "
        "sent and smarthedger stopped")

    kHedgeInst = ParamDef("hedge-instrument", "Hedge instrument")

    kHedgeOffsetMs = ParamDef("hedge-offset-ms",
        "Hedge time offset in ms, accepted")

    kHedger = ParamDef("hedger", "Hedger to hedge the delta")

    kHedgerReady = ParamDef("hedger-ready", "State of the hedger")

    kHedgeTime = ParamDef("hedge-time", "Hedge time")

    kInitAt = ParamDef("init-at",
        "What price to use while initiating the trade.")

    kInitiating = ParamDef(
        "initiating", "If False, won't initiate even if market is given")

    kInitInst = ParamDef("init-instrument", "Init Instrument")

    kInitQty = ParamDef("init-quantity", "Quantity on Init Instrument")

    kInstantExitThreshold = ParamDef("instant-exit-threshold",
        "Exit current position if absolute delta "
        "value is larger than instant-exit. "
        "Set to 0 to disable")

    kInstantExitTicks = ParamDef("instant-exit-ticks",
        "A pay up limit order is sent "
        "instant-exit-ticks ticks away from the "
        "market price")

    kInstrument = ParamDef("instrument", "target instrument")

    kJoinThreshold = ParamDef(
        "join-threshold",
        "Join price to work delta if size@price > join-threshold. Set to 0 "
        "to disable")

    kJumpInst = ParamDef("jump-instrument",
        "Instrument to listen to for stop-on-jump-ticks")

    kLeanThreshold = ParamDef(
        "lean-threshold",
        "Volume required in reference contract best price to assume we can hedge "
        "there. Example: A buy legger, best bid/ask 600x @16000/40x "
        "@16005, lean-threshold=500: since volume = 600 > threshold of 500 the "
        "legger can price off the best bid, it will be able to hedge at "
        "16000.Set 0 to disable.")

    kLevels = ParamDef("levels", "Levels to stack per shot")

    kListenOrderAcks = ParamDef(
        "listen-order-acks",
        "Set to False to not listen to order acks to avoid interfering with the "
        "send timer. Set to True to react to first accepted order and to stop "
        "after "
        "enough orders have been accepted.")

    kMaxBuySell = ParamDef("max-buysell",
        "Max delta that can be added with buy and sell "
        "button, or larger. If set to 0 buy/sell buttons "
        "can't be used")

    kMaxDelta = ParamDef(
        "max-delta", "Stop legger(s) if we hit self limit. Hedge smaller deltas")

    kMaxHits = ParamDef("max-hits",
        "Maximum number of orders per level to keep")

    kMaxLevels = ParamDef("max-levels",
        "An active stack will keep the stack size between "
        "min-levels and max-levels.")

    kMaxShots = ParamDef("max-shots", "Maximum number of shots attempted")

    kMaxSize = ParamDef(
        "max-size",
        "Maximum bound for order size "
        "range, for each order is randomized within range")

    kMaxStepBack = ParamDef(
        "max-step-back",
        "Number of stacked levels allowed to fill, stops if it fills "
        "max-step-back levels. Step back one tick for every filled level. Resets "
        "when hedger is flat.")

    kMaxTrades = ParamDef(
        "max-trades",
        "Maximum volume to fill, algo when filled volume since start is more "
        "than max-trades. 0 will disable the guard.")

    kMgFiringSequence = ParamDef(
        "firing-sequence",
        "Specify the firing sequence.\n"
        "  Eg B0.1|B0.2|S0.4 will generate BUY@0.1, BUY@0.2, SELL@0.4 per batch.\n"
        "If empty, automatically generate orders based on buy-price (sell-price)"
        "and buy-levels (sell-levels).")

    kMgRequestPriority = ParamDef(
        "request-priority",
        "Sets the priority level of requests from self MG to BOB\n"
        "Must be an integer value")

    kMinLevels = ParamDef("min-levels",
        "An active stack will keep the stack size between "
        "min-levels and max-levels.")

    kMinSize = ParamDef(
        "min-size",
        "Mininum bound for order size range, for each order is randomized "
        "within range")

    kMktPriority = ParamDef("mkt-priority",
        "Market data distribution priority - decides which "
        "algo is likely to see market data updates first")

    kMomentumInst = ParamDef("momentum-inst", "Instrument being watched")

    kMomentumMinMoveTicks = ParamDef(
        "momentum-min-move-ticks",
        "Minimum ticks momentum instrument need to trade up or down to generate "
        "an initiate event for the active instrument")

    kMomentumMinTradeSize = ParamDef("momentum-min-trade-size",
        "Minimum volume that needs to be traded "
        "to generate an initiate event for the"
        " active instrument")

    kMomentumTimeWindowMs = ParamDef("momentum-time-window-ms",
        "Rolling time window (milliseconds) for " +
        kMomentumMinMoveTicks.name + " and " +
        kMomentumMinTradeSize.name)

    kMultiHedgerFirstInst = ParamDef(
        "first-instrument",
        "First instrument to hedge (reference contract for pricing legger)")

    kMultiHedgerSpread = ParamDef(
        "spread",
        "First - second spread for pricing hedge in second contract (if non-empty, "
        "including 0). If spread is not set (empty), second contract hedge "
        "price will be based off current bbo. First contract hedge price is always "
        "using the price from legger. tick-offset is applied after spread.")

    kMultiHedgerTickOffset = ParamDef(
        "tick-offset",  # aka payup-ticks
        "Given the price (Px) algo is trying to get, order will be placed "
        "tick-offset from Px. If it is a positive number, algo will buy high "
        "or "
        "sell low and vice versa. Applied after spread. Applied after spread.")

    kNumTicks = ParamDef("num-ticks", "Ticks needed to trigger big move")

    kOffsetMs = ParamDef("offset-ms",
        "millisecond offset to be added to start-time decimal "
        "values are allowed for sub-millisecond offsets eg "
        "0.1 = 100us ")

    kOrderAddPriority = ParamDef(
        "order-add-priority", "Priority of adding an order. * denotes default")

    kOrderDelPriority = ParamDef(
        "order-delete-priority",
        "Priority of deleting an order. * denotes default")

    kOrderIntervalMs = ParamDef("order-interval-ms",
        "millisecond interval between order-placements "
        "decimal values are allowed for "
        "sub-millisecond intervals eg 0.1 = 100us ")

    kOrderUpdPriority = ParamDef(
        "order-update-priority",
        "Priority of updating an order. * denotes default")

    kOnAcceptingSize = ParamDef("on-accepting-size",
        "Optional size to use for fire-on-accepting. "
        "Using min/max size if not set")

    kOppoThreshold = ParamDef(
        "oppo-threshold",
        "Volume threshold in reference contract opposite side to assume we can "
        "hedge at bbo. For a buy legger with oppo-threshold = 500 and reference "
        "ask = 40x @16000 it can price off reference contract best bid "
        "(40 doesn't exceed threshold of 500). If instead oppo-threshold = 30, "
        "then cannot price off best bid. Set 0 to disable.")

    kOrderSide = ParamDef("order-side", "Buy/Sell")

    kOrderType = ParamDef("order-type", "Order type")

    kPeMinLeanVol = ParamDef("pe-min-lean-vol", "pe-min-lean-vol")

    kPayupTicks = ParamDef(
        "payup-ticks",
        "Given the price (Px) algo is trying to get, order will be placed "
        "payup-ticks from Px. If it is a positive number, algo will buy high "
        "or "
        "sell low and vice versa.")

    kPlaceOnMiss = ParamDef("place-on-miss",
        "True to fire order and exit when watch-price "
        "level is fully cleared (threshold will not be met "
        "in self case), to just exit when "
        "watch-price level is fully cleared.")

    kPrice = ParamDef("price", "Price")

    kPricer = ParamDef("Pricer", "Pricer module")

    kPriceRatio = ParamDef(
        "price-ratio",
        "Ratio first_price = ratio(second_price - efp) + "
        "constant. Same as ratio spread efp = 0, constant = 0 ")

    kPricerType = ParamDef("pricer-type",
        "Options (Future, ETF, Inverse, Leveraged). "
        "Calculate target price according the relevant "
        "pricing model.")

    kRejLatencyBatchInterval = ParamDef ("rej-latency-batch-interval-ms",
        "Batch interval (ms) to be used reject latency (inflight) is triggered.")

    kRejLatencyInflights = ParamDef("rej-latency-inflights",
        "Minimum number of occurrences before "
        "reject latency triggers a reaction.\n"
        "0 disables self feature.")

    kRejLatencyInflightThreshold = ParamDef("rej-latency-inflight-threshold-us",
        "If inflight order reject latency exceeds self value(us), "
        "it will be counted towards trigger event.")

    kRejLatencyOffset = ParamDef("rej-latency-offset-us",
        "Offset (us) to start brute-force firing at "
        "rej-latency batch and order intervals when "
        "reject latency (inflight) is triggered.")

    kRejLatencyOrderInterval = ParamDef("rej-latency-order-interval-ms",
        "Order interval (ms) to be used when "
        "reject latency (inflight) is "
        "triggered.")

    kRejLatencyThreshold = ParamDef(
        "rej-latency-threshold-us",
        "If order reject latency exceeds self value(us), "
        "reject latency event will be triggered")

    kSecondHedger = ParamDef("second-hedger",
        "Second hedger to hedge delta in excess of "
        "second-hedger-delta-threshold, "
        "if use-second-hedger is True")

    kSecondHedgerDeltaThreshold = ParamDef(
        "second-hedger-delta-threshold",
        "Send delta > second-hedger-delta-threshold to second-hedger, if "
        "use-second-hedger is True.")

    kSecondInst = ParamDef("second-instrument", "Second Instrument")

    kSecondLeanThreshold = ParamDef("second-lean-threshold",
        "Second Lean Threshold")

    kSecondQty = ParamDef("second-quantity", "Quantity on second Instrument")

    kSellLevels = ParamDef(
        "sell-levels", "Number of BID ticks down from buy-price to fire into")

    kSellMaxTrades = ParamDef(
        "sell-max-trades",
        "Maximum volume to fill on sell side, algo "
        "when filled volume on sell side since start is more "
        "than sell-max-trades. 0 will disable the guard.")

    kSellOffset = ParamDef("sell-offset",
        "Offset to modify sell side, negative "
        "value to sell lower, to sell higher.")

    kSellPrice = ParamDef("sell-price", "Price for first level SELL orders")

    kSellSize = ParamDef("sell-size", "Size for sell orders")

    kSellSpread = ParamDef("sell-spread",
        "First - second / First/Second spread to sell")

    kSellSpreadPriceLimit = ParamDef(
        "sell-spread-price-limit", "Highest bid/lowest offer for sell-spread")

    kShots = ParamDef("shots", "Number of shots to split size in")

    kSignalExpiry = ParamDef("signal-expiry-ms",
        "Allow reentering market after MS")

    kSignallerListener = ParamDef("signaller-listener",
        "Algo listening to signals")

    kSize = ParamDef("size", "Size of the order. Strictly positive integer")

    kSkewVolMin = ParamDef("skew-vol-min", "skew-vol-min")

    kSkewRatioThres = ParamDef("skew-ratio-thres", "skew-ratio-thres")

    kSniperMode = ParamDef(
        "sniper-mode",
        "\"SellAtBid - Normal Sniper\" : watch bid; place SELL order at bid when "
        "bid-qty <= volume-threshold\n \"BuyAtAsk - Normal Sniper\" : watch ask; "
        "place BUY order at ask when ask-qty <= volume-threshold\n \"SellAtAsk - "
        "Queuer Sniper\" : watch ask; place SELL order at ask when ask-qty >= "
        "volume-threshold\n \"BuyAtBid - Queuer Sniper\"  : watch bid; place BUY "
        "order at bid when bid-qty >= volume-threshold\n")

    kSplitNum = ParamDef("split-num",
        "Send num orders with size as evenly spread as "
        "possible. Set to 0 or 1 to send only one order per "
        "level. Must be 6 or less.")

    kSpread = ParamDef("spread",
        "First - second / First/Second spread to buy or sell")

    kSpreadAdjustment = ParamDef("spread-adjustment",
        "Spread adjustment increment used by change "
        "spread function. Set to 0 to use tick size.")

    kSpreadPriceLimit = ParamDef(
        "spread-price-limit",
        "Maximum spread value for buy spreads, spread value for sell "
        "spreads. Protection against setting wrong spread prices while trading.")

    kSpreadRange = ParamDef("spread-range",
        "When top of the stack is within self many "
        "points from spreaded price, it "
        "won't reprice. Use to avoid repricing "
        "unnecessarily. When set to 0 stack "
        "will reprice for every change in hedge market.")

    kSpreadType = ParamDef("spread-type",
        "Options (Regular, ratio). Regular calculates "
        "spread as a differential and ratio as a ratio "
        "between first and second contract.")

    kStackedOrderAddPriority = ParamDef(
        "stacked-order-add-priority",
        "Priority of adding stack order, e.g. High|Normal means high priority "
        "for "
        "top of stack and normal priority otherwise. * denotes default")

    kStackedOrderDelPriority = ParamDef(
        "stacked-order-del-priority",
        "Priority of deleting stack order, e.g. High|Normal means high priority "
        "for "
        "top of stack and normal priority otherwise. * denotes default")

    kStackedOrderUpdPriority = ParamDef(
        "stacked-order-upd-priority",
        "Priority of updating stack order, e.g. High|Normal means high priority "
        "for "
        "top of stack and normal priority otherwise. * denotes default")

    kStartOffsetMs = ParamDef("start-offset-ms",
        "Start time offset in ms, accepted")

    kStartTime = ParamDef("start-time",
        "Start firing at (start-time + offset-ms). Alice "
        "displays self in SGT - need to consider timezone "
        "difference if exchange is not on SGT")

    kStopCondition = ParamDef("stop-condition",
        "Condition type that the stop price refers to")

    # TODO(j) rename when bobendpoint is fixed to not require obi
    kStopInst = ParamDef("stop-instrument-x", "Stop Instrument")


    kStopLevels = ParamDef("stop-levels",
        "Number of levels to place initially. Stopper "
        "will not remove orders once "
        "placed to save on messaging.")

    kStopOnJumpTicks = ParamDef(
        "stop-on-jump-ticks",
        "Set to listen to an instrument defined by jump-instrument. If the "
        "jump-instrument moves more than stop-on-jump-ticks ticks the stack will "
        "stop and stack orders cancelled. Can typically be used to get out when "
        "a "
        "correlated market moves against the stack. Set to 0 to disable.")

    kStopOnOrderError = ParamDef("stop-on-order-error",
        "When an order error is thrown by bob, algo "
        "will cancel orders and stop if self option "
        "is True, handover and stop. ")

    kTheo = ParamDef("theo",
        "Only active when using an ETF pricer. Displays "
        "theoretical value of ETF as calculated by algo")

    kThresholdDrop = ParamDef(
        "threshold-drop",
        "If max size@price - threshold-drop > exit-threshold, use "
        "max size - threshold-drop as exit threshold. Set to 0 to disable")

    kTickOffset = ParamDef(
        "tick-offset",  # aka payup-ticks
        "Given the price (Px) algo is trying to get, order will be placed "
        "tick-offset from Px. If it is a positive number, algo will buy high "
        "or "
        "sell low and vice versa.")

    kTimeFrame = ParamDef("time-frame", "Time frame for big move")

    kTimeInForce = ParamDef("time-in-force",
        "Time in force (day, immediate, GTC, etc)")

    kTimeWindowMin = ParamDef("time-window-min",
        "Time window self instance has to complete all "
        "shaving tasks (in minutes).")

    kTradeOutThreshold = ParamDef("trade-out-threshold",
        "Consider level traded out if traded volume "
        "/ remaining qty > tradeout-threshold")

    kTradeSumPctThres = ParamDef("trade-sum-pct-thres", "trade-sum-pct-thres")

    kTradeSumMin = ParamDef("trade-sum-min", "trade-sum-min")

    kTradingSide = ParamDef("trading-side",
        "Side to take for trading instrument")

    kTotalSize = ParamDef("total-size", "Total size to be placed")

    kTriggerVolPct = ParamDef("trigger-vol-pct", "trigger-vol-pct")

    kTriggerVolMin = ParamDef("trigger-vol-min", "trigger-vol-min")

    kTriggerVolMax = ParamDef("trigger-vol-max", "trigger-vol-max")

    kUnAckedSizeMultiplier = ParamDef(
        "unacked-size-multiplier",
        "Unacknowledged-size-multiplier times max-delta gives the acceptable "
        "number of orders that can stay unacknowledged by the exchange. If the "
        "number is crossed, stops. 0 disables the guard. ")

    kUseGtc = ParamDef("use-gtc", "True to use GTC orders")

    kUseHeld = ParamDef(
        "use-held",
        "Use held orders\nEnsure we have sufficient limits when self is enabled")

    kUseMassQuote = ParamDef("use-mass-quote",
        "Use mass quote message eg MO36 for OMEX")

    kUseMktHint = ParamDef(
        "use-mkt-hint",
        "Use market hints from stop conversions and fills (canaries)")

    kUseSalvo = ParamDef(
        "use-salvo", "Send each ADD/DEL request over multiple exchange links")

    kUseSecondHedger = ParamDef(
        "use-second-hedger",
        "Set self to True to send delta > second-hedger-delta-threshold to "
        "second-hedger.")

    kUseSobolSequence = ParamDef("use-sobol-sequence",
        "Let algo generate a pseudorandom sequence. "
        "Can't use more than 128 levels.")

    kUseUpdate = ParamDef("use-update",
        "Use order update instead of delete/add")

    kVolumeThreshold = ParamDef("volume-threshold",
        "When volume threshold is achieved, order "
        "is placed. Strictly positive integer")

    kWaitForHedge = ParamDef("wait-for-hedge",
        "Wait for hedger to be totally hedged")

    kWatchPrice = ParamDef("watch-price",
        "Price that trader wants to monitor, will "
        "only be fired if price is the best bid/ask "
        "(depending on value of watch-bid)")

    kWatchThreshold = ParamDef(
        "watch-threshold",
        "Start watching price when size on price level > watch-threshold. "
        "Set to 0 to disable.")

    kMinCoverQty = ParamDef("min-cover-qty", "min-cover-qty")

    kMaxQueueVol = ParamDef("max-queue-vol", "max-queue-vol")

