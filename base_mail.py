import zipfile
from decimal import *
from pprint import *
import sys
import imaplib
import email
import os
import re
import datetime
import logging
import subprocess
import os
import paramiko


from email import encoders
import email
from email.mime.base import *
from email.mime.multipart import *
from email.mime.text import *
from email.utils import COMMASPACE, formatdate
import email.utils
import smtplib
import os
import logging



whoami = subprocess.check_output('whoami').decode('UTF-8').replace('\n', '')


_FOLDER_TO_SAVE = '/tmp/posadjs/'
_LOG_FILE = '/tmp/scripts_' + datetime.datetime.now().strftime('%Y%m%d-%H%M%S') + '.log'
_SERVER_TO_COPY_TO = 'csqprod-app05'
_MAIL_USER = whoami + '@grasshopperasia.com'
_MAIL_PASSWORD = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

logging.basicConfig(filename=_LOG_FILE,level=logging.DEBUG)


# _P = 'order_book_name=(?P<ORDERBOOK>[\w.]+).*\n?.*instrument_key=(?P<SECURITY>[\w|]+).*\n?.*\((?P<LONG>[-\d]+), (?P<SHORT>[-\d]+)\)'
# _PATTERN = re.compile(_P, re.MULTILINE)

_try_decode = lambda v: v.decode('utf-8') if isinstance(v, bytes) else v
_decoded_val = lambda vals: ([_try_decode(x[0]) for x in vals if not x[1]] or
                      [x[0].decode(x[1]) for x in vals if x[1]])[0].strip()

# def write_to_file(file_path, text_to_write, write_type='wt'):
#     with open(file_path, write_type) as f:
#         f.write(text_to_write)
#         f.close()


class EmailAccount:
    def __init__(self, last_uid):
        self.last_uid = str(last_uid)

    def login(self):
        logging.debug("Email_Service::_check_mail:: Time to check mail")
        self._mailbox = imaplib.IMAP4_SSL('imap.gmail.com')

        try:
            rv, data = self._mailbox.login(_MAIL_USER, _MAIL_PASSWORD)
            # ll = [x.decode('UTF-8') for x in self._mailbox.list()[1]]
            # print('ll = {}'.format('\n'.join(ll)))
            logging.debug("Worked for {}" + whoami)
            logging.debug("EMAIL DATA:%s ", data)
        except imaplib.IMAP4.error as e:
            logging.debug("LOGIN FAILED for {}: {}".format(whoami, str(e)))
            return False
        return True

    def logout(self):
        self._mailbox.logout()

    def run(self):
        if self.login():
            self.read_mails()
            self.logout()

    # def read_mails(self):
    #     subprocess.check_output(['rm', '-rf', '/tmp/posadjs/'])
    #     os.makedirs('/tmp/posadjs/', exist_ok=True)
    #     status, x = self._mailbox.select('"[Gmail]/All Mail"')
    #     print('x = {}'.format(x))
    #     result, mail_numbers = self._mailbox.uid('search', None, r'SUBJECT "CRITICAL: Position adjustment"')
    #     messages = mail_numbers[0].split()
    #     logging.debug('messages={}'.format(messages))
    #
    #     for message_uid in messages:
    #         message_uid_i = int(message_uid)
    #         result, mail_data_raw = self._mailbox.uid('fetch', message_uid, '(RFC822)')
    #         # yield raw mail body
    #         if mail_data_raw[0] is not None:
    #             self.process_mail(message_uid_i, mail_data_raw)

    # def process_mail(self, num, mail_data):
    #     _mail_header = lambda x: _decoded_val(email.header.decode_header(mail[x]))
    #     header_decoder = lambda x: email.header.decode_header(mail[x])[0][0]
    #     mail = email.message_from_string(mail_data[0][1].decode("utf-8"))
    #     logging.debug("\n\n %s", "#_" * 90)
    #
    #     subject = _mail_header('Subject').replace('\r\n', '')
    #     print('_'*80 + '\n\n\n')
    #     print('...... {}'.format(email.header.decode_header(mail['Subject'])))
    #     print('UID: {}.... subject = {}'.format(num, subject))
    #     groups = re.search(_PATTERN, subject).groupdict()
    #     print('dicts = {}'.format(groups))
    #     mail_date = _mail_header('Date').upper()[5:16].replace(' ', '_')
    #
    #     text = posadj_s.format(**groups)
    #     filename = os.path.join(_FOLDER_TO_SAVE, 'posadj_{}_{}.py'.format(mail_date, num))
    #     write_to_file(filename, text)

####################################################################################

def send_mail(subject, body, recipients,
              author='abhishek.pandey@grasshopperasia.com',
              author_name='Abhishek',
              password='Tsuguri0',
              recipient_name='Stakeholders'):
    # Create the message
    # msg = MIMEText(body)
    # msg['To'] = email.utils.formataddr((recipient_name, recipients[0]))
    # msg['From'] = email.utils.formataddr((author_name, author))
    # msg['Subject'] = subject

    msg = MIMEMultipart()
    msg['From'] = email.utils.formataddr((author_name, author))
    msg['To'] = COMMASPACE.join(recipients)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(body, 'html'))



    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    # server.set_debuglevel(True) # show communication with the server
    server.starttls()
    server.login(author, password)
    try:
        server.sendmail(author, recipients, msg.as_string())
    finally:
        server.quit()



def send_mail_with_attachments(send_to, subject, text,
                               sender_tuples,
                               files=[],
                               server="smtp.gmail.com:587",
                               str_attachments={}
):
    sender_name = sender_tuples[0]
    sender_accnt = sender_tuples[1]
    sender_encrypted_pwd = sender_tuples[2]
    sender_decrypted_pwd = __get_decrypted_pwd(sender_encrypted_pwd)
    assert isinstance(send_to, list)
    assert isinstance(files, list)
    env_type = os.environ.get('RW1_ENV', 'UNKNOWN')
    logging.debug('env type = {}'.format(env_type))

    msg = MIMEMultipart()
    msg['From'] = sender_name
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text, 'html'))

    for f in files:
        if f != '' and os.path.exists(f):
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(f, "rb").read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
            msg.attach(part)
        else:
            logging.warning('File does not exist. Ignoring this as attachment: {}'.format(f))

    if len(str_attachments) > 0:
        for name, str_item in str_attachments.items():
            part = MIMEText(str_item, 'html')
            part.add_header('Content-Disposition', 'attachment; filename="{}.html"'
                                                    .format(name))
            msg.attach(part)

    smtp = smtplib.SMTP(server)
    smtp.ehlo()
    smtp.starttls()
    smtp.login(sender_accnt, sender_decrypted_pwd)
    smtp.sendmail(sender_accnt, send_to, msg.as_string())
    smtp.close()

    # Remove the attachment file now
    for f in files:
        if f != '' and not f.endswith('.xls') and not f.endswith('.txt'):
            os.remove(f)
