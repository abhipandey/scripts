from base_rw1 import *

from reconwisepy.services.sftp_service import Sftp_Service
from reconwisepy.services.email_service import Email_Service
from reconwisepy.services.service_runner import get_services
from reconwisepy.services.file_parser import FileParser
from reconwisepy.services.settings import *
from reconwisepy.utils import crypts
from reconwisepy.services.settings import _broker_ne, _broker_ms, _broker_abn


def get_pretty_print(item, force=False):
    if force:
        custom_printer = lambda a: (isinstance(a,list) or isinstance(a, tuple)) and \
                                   len(a) > 0 and \
                                   (isinstance(a[0], list) or isinstance(a[0], tuple))
        if custom_printer(item):
            s = ''
            for i in item:
                s += '\n' + str(i)
            return s
    pp = pprint.PrettyPrinter(indent=4)
    return pp.pformat(item)


pp = lambda item: get_pretty_print(item)

from reconwisepy.utils.utilities import *
from reconwisepy.db.db_layer import MyData

def _find_files():
    files = find_dirs_and_files(
        '*.json',
        '/var/reconwise/reports/json',
        False, True, True
        )
    db = MyData()
    for file in files:
        uid_s = int(file[file.rindex('_')+1:].replace('.json', ''))
        dt = filename_to_date(file)
        print('File={}; Date={}; uid={}; '.format(
            file, dt, uid_s
            ))
        text = get_file_text(file).replace('\'', '').replace('\n', '')
        db.cache_report(file, text, uid_s, dt)


if __name__ == "__main__":
    _find_files()

