#!/usr/bin/python3.7

from collections import OrderedDict
import datetime
import random
import time
import copy
import logging

from google.protobuf import text_format

from gore.protobuf.base import log as pblog

import ems_pb2, core_pb2

from gore.util.app import SolCmd
from gore.clients.ems import Client

logging.basicConfig(level=logging.DEBUG)


def make_order_id():
    oid = hex(random.randint(0, pow(2, 36)))[2:] + hex(random.randint(0, pow(2, 24)))[2:]
    if len(oid) != 16:
        oid = ('0' * (16 - len(oid))) + oid
    return oid.upper()

# APP_ID = 'CSQ/csqstage-ems03/bobnxtks-tne01'
# APP_ID = 'CSQ/csqstage-ems03/bobnxose-tne01'
# APP_ID = 'CSQ/csqstage-ems02/boboxses-t01'
# APP_ID = 'CSQ/csqstage-ems04/bobn0bme-t01'
# APP_ID = 'CSQ/aseem/bobnxses-tne01'
# APP_ID = 'CSQ/aseem/bobcrypto01'
# APP_ID = 'CSQ/aseem/bobn0bme-t01'
# APP_ID = 'CSQ/aseem/bobnxcme-t01'
# APP_ID = 'CSQ/csqstage-ems02/boboxses-t01'
# APP_ID = 'JPX/jpxstage-ems02/bobnxtks-tsg01'
# APP_ID = 'JPX/jpxstage-ems02/bobnxose-tne01'
# APP_ID = 'KDH/kdhstage-ems03/bobnxses-tne01'
# APP_ID = 'GCE/ghpr-dev-ems-sg-1/bobaexbkk-tfss01'
# APP_ID = 'JPX/jpxprod-app02/bobfxtks-tsg01'
APP_ID = 'GCE/ghpr-dev-ems-sg-1/bobaexbkk-tfss01'

# INSTRUMENT = "STOCK|XTKS|1321|"
# INSTRUMENT = "FUTURE|XOSE-T|FUT_NK225_1906|1906"
# INSTRUMENT = "FUTURE|XSES|NKU16|1609"
# INSTRUMENT = "STOCK|XTKS|1320|"
# INSTRUMENT = "CURRENCY|0BME-T|XBTUSD|"
INSTRUMENT = "STOCK|XBKK-T|ADVANC|"
# INSTRUMENT = "STOCK|XTKS-T|1305|"

# ORDERBOOK = "uatLance.sg"
# ORDERBOOK = "uatAseem2.ne"
# ORDERBOOK = "uatAseem.carbo.ne"
# ORDERBOOK = "uatAseem.sg"
# ORDERBOOK = "uatAseem.ne"
# ORDERBOOK = "uatAseem.cp"
# ORDERBOOK = "uatAseem.cp2"
ORDERBOOK = "uatThaistocks.fs"
# ORDERBOOK = "uatLance.sgFIX.sg"

PRICE = "266"
PRICE2 = "267"

add_template = """
client_order_id: "%(orderid)s"
type: NEW
owner_app_id: "web/lance/alice"
instrument_id { type: INSTRUMENT_ID_TYPE_NATIVE key: \"""" + INSTRUMENT + """\" }
order_book_id: \"""" + ORDERBOOK + """\"
order_side: OS_BUY
quantity: 200
limit_price: \"""" + PRICE + """\"
is_manual_order: true
"""

upd_template = """
client_order_id: "%(orderid)s"
type: UPDATE
owner_app_id: "web/lance/alice"
instrument_id { type: INSTRUMENT_ID_TYPE_NATIVE key: \"""" + INSTRUMENT + """\" }
order_book_id: \"""" + ORDERBOOK + """\"
order_side: OS_BUY
limit_price: \"""" + PRICE2 + """\"
is_manual_order: true
"""

ROM = ems_pb2.ReqOrderMerge

def make_new():
    req_str = add_template % {"orderid": make_order_id()}
    req = ems_pb2.ReqOrderMerge.Data()
    text_format.Merge(req_str, req)
    return req


def make_delete(*client_order_ids):
    ret = []
    for cid in client_order_ids:
        ret.append(ems_pb2.ReqOrderMerge.Data())
        ret[-1].type = ems_pb2.ReqOrderMerge.DELETE
        ret[-1].client_order_id = client_order_id
    return ret

def do_held(client):
    req1 = make_new()
    req1.held = True
    # print req1
    client.order_merge([req1])

    input("Press Enter to continue...")

    req3 = ems_pb2.ReqOrderMerge.Data()
    req3.type = ems_pb2.ReqOrderMerge.UPDATE
    req3.client_order_id = req1.client_order_id
    req3.held = True
    # print req3
    client.order_merge([req3])

    input("Press Enter to continue...")

    reqd = ems_pb2.ReqOrderMerge.Data()
    reqd.type = ems_pb2.ReqOrderMerge.DELETE
    reqd.client_order_id = req1.client_order_id
    # reqd.held = True
    # print reqd
    # client.order_merge([reqd])

def do_add(client):
    reqa1 = ems_pb2.ReqOrderMerge.Data()
    req_str = add_template % {"orderid": make_order_id()}
    text_format.Merge(req_str, reqa1)
    reqa1.priority = 4242
    reqa1.time_in_force = ems_pb2.TIF_IMMEDIATE
    # print reqa1
    client.order_merge([reqa1])

def do_mkt_to_limit(client):
    reqa = ems_pb2.ReqOrderMerge.Data()
    req_str = add_template % {"orderid": make_order_id()}
    text_format.Merge(req_str, reqa)
    reqa.ClearField('limit_price')
    reqa.held = True
    # print reqa
    client.order_merge([reqa])

    time.sleep(0.5)

    requ = ems_pb2.ReqOrderMerge.Data()
    requ.client_order_id = reqa.client_order_id
    requ.held = False
    requ.limit_price = '19050'
    # print requ
    client.order_merge([requ])

# def do(client):
#     add_req = ems_pb2.ReqOrderMerge.Data()
#     req_str = add_template % {"orderid": make_order_id()}
#     text_format.Merge(req_str, add_req)
#     add_req.held = True
#     print add_req
#     client.order_merge([add_req])

#     raw_input("Press Enter to continue...")

#     req = ems_pb2.ReqOrderMerge.Data()
#     req.type = ems_pb2.ReqOrderMerge.UPDATE
#     req.client_order_id = add_req.client_order_id
#     req.quantity = add_req.quantity + 1
#     # req.held = True
#     req.held = False
#     req.priority = 20
#     print req
#     client.order_merge([req])

#     raw_input("Press Enter to continue...")

#     req = ems_pb2.ReqOrderMerge.Data()
#     req.type = ems_pb2.ReqOrderMerge.UPDATE
#     req.client_order_id = add_req.client_order_id
#     req.quantity = add_req.quantity + 2
#     print req
#     client.order_merge([req])

#     raw_input("Press Enter to continue...")

#     req = ems_pb2.ReqOrderMerge.Data()
#     req.type = ems_pb2.ReqOrderMerge.DELETE
#     req.client_order_id = add_req.client_order_id
#     print req
#     client.order_merge([req])

def do(client):
    add_reqs = []
    for i in range(20):
        req = ems_pb2.ReqOrderMerge.Data()
        req_str = add_template % {"orderid": make_order_id()}
        text_format.Merge(req_str, req)
        # print req
        add_reqs.append(req)

    upd_reqs = []
    for i in range(20):
        upd1 = ems_pb2.ReqOrderMerge.Data()
        upd1.type = ems_pb2.ReqOrderMerge.UPDATE
        upd1.client_order_id = add_reqs[i].client_order_id
        upd1.limit_price = str(int(add_reqs[i].limit_price) - 20)
        upd1.quantity = add_reqs[i].quantity + 1
        # print upd1
        upd_reqs.append(upd1)

    upd2 = ems_pb2.ReqOrderMerge.Data()
    upd2.type = ems_pb2.ReqOrderMerge.UPDATE
    upd2.client_order_id = add_reqs[-1].client_order_id
    upd2.limit_price = str(int(add_reqs[-1].limit_price) - 40)
    upd2.quantity = add_reqs[-1].quantity + 2
    upd2.held = True
    upd2.priority = 20
    # print upd2

    upd3 = ems_pb2.ReqOrderMerge.Data()
    upd3.type = ems_pb2.ReqOrderMerge.UPDATE
    upd3.client_order_id = add_reqs[-1].client_order_id
    upd3.limit_price = str(int(add_reqs[-1].limit_price) - 60)
    upd3.quantity = add_reqs[-1].quantity + 3
    upd3.held = False
    # print upd3

    client.order_merge(add_reqs)

    raw_input("Press Enter to continue...")

    client.order_merge(upd_reqs)
    client.order_merge([upd2])
    client.order_merge([upd3])

def do_fpga(client):
    reqs = []
    for i in range(2):
        req = ems_pb2.ReqOrderMerge.Data()
        req_str = add_template % {"orderid": make_order_id()}
        text_format.Merge(req_str, req)
        req.priority = 4242
        req.held = True
        # print req
        reqs.append(req)

    fut = client.order_merge(reqs)

    resp = fut.result()
    # print resp
    # print resp.data[0].__class__
    # print resp.data[0].fpga_data.__class__
    # print len(resp.data[0].fpga_data)


def do_new(client):
    req = ems_pb2.ReqOrderMerge.Data()
    req_str = add_template % {"orderid": make_order_id()}
    text_format.Merge(req_str, req)
    # print req
    client.order_merge([req])

def do_aud(client):
    reqa1 = ems_pb2.ReqOrderMerge.Data()
    req_str1 = add_template % {"orderid": make_order_id()}
    text_format.Merge(req_str1, reqa1)
    # reqa1.quantity = 2
    # reqa1.quantity_exp = -3
    # reqa1.limit_price = "11000"
    # reqa1.held = True
    # reqa1.quantity = 20
    # print reqa1
    client.order_merge([reqa1])

    # raw_input("Press Enter to continue...")

    # requ1 = ems_pb2.ReqOrderMerge.Data()
    # requ1.client_order_id = reqa1.client_order_id
    # requ1.quantity = 15
    # requ1.quantity_exp = -3
    # requ1.limit_price = "11600"
    # print requ1
    # client.order_merge([requ1])

    raw_input("Press Enter to continue...")

    reqd1 = ems_pb2.ReqOrderMerge.Data()
    reqd1.client_order_id = reqa1.client_order_id
    reqd1.type = ems_pb2.ReqOrderMerge.DELETE
    # print reqd1
    client.order_merge([reqd1])

    # print "done"

def do_aud_multiple(client):
    reqs1 = []
    for i in range(4):
        reqa = ems_pb2.ReqOrderMerge.Data()
        req_str = add_template % {"orderid": make_order_id()}
        text_format.Merge(req_str, reqa)
        # print reqa
        reqs1.append(reqa)

    client.order_merge(reqs1)

    raw_input("Press Enter to continue...")

    reqs2 = []
    # modify 2
    for r in reqs1[:2]:
        req = ems_pb2.ReqOrderMerge.Data()
        req.type = ems_pb2.ReqOrderMerge.UPDATE
        req.client_order_id = r.client_order_id
        req.limit_price = "7000"
        req.quantity = 2
        # print req
        reqs2.append(req)
    # delete other 2
    for r in reqs1[2:]:
        req = ems_pb2.ReqOrderMerge.Data()
        req.type = ems_pb2.ReqOrderMerge.DELETE
        req.client_order_id = r.client_order_id
        # print req
        reqs2.append(req)
    # add 2 more
    for i in range(2):
        reqa = ems_pb2.ReqOrderMerge.Data()
        req_str = add_template % {"orderid": make_order_id()}
        text_format.Merge(req_str, reqa)
        # print reqa
        reqs2.append(reqa)

    client.order_merge(reqs2)

    raw_input("Press Enter to continue...")

    reqs3 = []
    # delete all
    for r in reqs2[:2]:
        req = ems_pb2.ReqOrderMerge.Data()
        req.type = ems_pb2.ReqOrderMerge.DELETE
        req.client_order_id = r.client_order_id
        # print req
        reqs3.append(req)
    for r in reqs2[4:]:
        req = ems_pb2.ReqOrderMerge.Data()
        req.type = ems_pb2.ReqOrderMerge.DELETE
        req.client_order_id = r.client_order_id
        # print req
        reqs3.append(req)

    client.order_merge(reqs3)


def do(client):
    reqas = []
    for i in range(20):
        reqa = ems_pb2.ReqOrderMerge.Data()
        req_str = add_template % {"orderid": make_order_id()}
        text_format.Merge(req_str, reqa)
        # print reqa
        reqas.append(reqa)

    client.order_merge(reqas)

    raw_input("Press Enter to continue...")

    requs = []
    for reqa in reqas:
        requ = ems_pb2.ReqOrderMerge.Data()
        requ.type = ems_pb2.ReqOrderMerge.UPDATE
        requ.client_order_id = reqa.client_order_id
        requ.limit_price = str(int(reqa.limit_price) - 10)
        requ.quantity = reqa.quantity + 1
        # print requ
        requs.append(requ)

    client.order_merge(requs)

    raw_input("Press Enter to continue...")

    reqds = []
    for reqa in reqas:
        reqd = ems_pb2.ReqOrderMerge.Data()
        reqd.type = ems_pb2.ReqOrderMerge.DELETE
        reqd.client_order_id = reqa.client_order_id
        # print reqd
        reqds.append(reqd)

    client.order_merge(reqds)

def do_custom(clt):
    reqa = ems_pb2.ReqOrderMerge.Data()
    req_str = add_template % {"orderid": make_order_id()}
    text_format.Merge(req_str, reqa)
    print(reqa)
    clt.order_merge([reqa], batch_type=ems_pb2.ReqOrderMerge.AUTO)

    input("Press Enter to continue...")
    requ = ems_pb2.ReqOrderMerge.Data()
    req_str = upd_template % {"orderid": reqa.client_order_id}
    text_format.Merge(req_str, requ)
    print(requ)
    clt.order_merge([requ], batch_type=ems_pb2.ReqOrderMerge.AUTO)

    input("Press Enter to continue...")
    reqd = ems_pb2.ReqOrderMerge.Data()
    reqd.type = ems_pb2.ReqOrderMerge.DELETE
    reqd.client_order_id = reqa.client_order_id
    print(reqd)
    clt.order_merge([reqd])

def main(session, cmdline_args):
    clt = Client(
        session,
        appid=APP_ID
    )

    # do_aud_multiple(clt)
    # do_aud(clt)
    #do_custom(clt)
    # do_custom(clt)
    do_held(clt)
    #do_new(clt)

if __name__ == "__main__":
    app = SolCmd()
    app.run(main)

