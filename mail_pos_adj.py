import zipfile
from decimal import *
from pprint import *
import sys
import imaplib
import email
import os
import re
import datetime
import logging
import subprocess
import os
import paramiko

whoami = subprocess.check_output('whoami').decode('UTF-8').replace('\n', '')


_FOLDER_TO_SAVE = '/tmp/posadjs/'
_LOG_FILE = '/tmp/mail_posadj.log'
_SERVER_TO_COPY_TO = 'csqprod-app05'
_MAIL_USER = whoami + '@grasshopperasia.com'
_MAIL_PASSWORD = 'Tsuguri0'



posadj_s = '''import os
import sys

pygh_path = '/usr/local/pygh/current/pygh/'
os.chdir(pygh_path)
sys.path.append(pygh_path)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pyghweb.settings")

# Import normal stuff
from django.db.models import Sum, Avg
import django

from pyghweb.eve.requests import SaveAdjustmentRequest
from gh.solace import SimpleApi
from django.conf import settings



print('CACHE_BACKEND = {{}}'.format(settings.CACHE_BACKEND))

solapi = SimpleApi.make_from_django_settings(settings)
req = SaveAdjustmentRequest(solapi)

resp = req.request('{ORDERBOOK}', # order book
                    8, # instrument id type
                    '{SECURITY}', # instrument id key
                    {LONG}, # long adjustment amount
                    {SHORT} # short adjustment amount
                    )
if resp.return_code == 0:
    print('Worked. OB: {ORDERBOOK}; Sec={SECURITY}; Long={LONG}; Short={SHORT}')
else:
    print('FAILED. OB: {ORDERBOOK}; Sec={SECURITY}; Long={LONG}; Short={SHORT}')

print('done.......')
'''

logging.basicConfig(filename=_LOG_FILE,level=logging.DEBUG)


_P = 'order_book_name=(?P<ORDERBOOK>[\w.]+).*\n?.*instrument_key=(?P<SECURITY>[\w|]+).*\n?.*\((?P<LONG>[-\d]+), (?P<SHORT>[-\d]+)\)'
_PATTERN = re.compile(_P, re.MULTILINE)

_try_decode = lambda v: v.decode('utf-8') if isinstance(v, bytes) else v
_decoded_val = lambda vals: ([_try_decode(x[0]) for x in vals if not x[1]] or
                      [x[0].decode(x[1]) for x in vals if x[1]])[0].strip()

def write_to_file(file_path, text_to_write, write_type='wt'):
    with open(file_path, write_type) as f:
        f.write(text_to_write)
        f.close()



class Account:
    def __init__(self, last_uid):
        self.last_uid = str(last_uid)

    def login(self):
        logging.debug("Email_Service::_check_mail:: Time to check mail")
        self._mailbox = imaplib.IMAP4_SSL('imap.gmail.com')

        try:
            rv, data = self._mailbox.login(_MAIL_USER, _MAIL_PASSWORD)
            # ll = [x.decode('UTF-8') for x in self._mailbox.list()[1]]
            # print('ll = {}'.format('\n'.join(ll)))
            logging.debug("Worked for {}" + whoami)
            logging.debug("EMAIL DATA:%s ", data)
        except imaplib.IMAP4.error as e:
            logging.debug("LOGIN FAILED for {}: {}".format(whoami, str(e)))
            return False
        return True
    def logout(self):
        self._mailbox.logout()

    def run(self):
        if self.login():
            self.read_mails()
            self.logout()

    def read_mails(self):
        subprocess.check_output(['rm', '-rf', '/tmp/posadjs/'])
        os.makedirs('/tmp/posadjs/')
        status, x = self._mailbox.select(r'"[Gmail]/All Mail"')
        print('x = {}'.format(x))
        result, mail_numbers = self._mailbox.uid('search', None, r'(Subject "CRITICAL: Position adjustment")')
        # result, mail_numbers = self._mailbox.uid('search',
        #                                          None,
        #                                          r'(X-GM-RAW "subject:\"CRITICAL: Position adjustment\"")'
        #                                          )
        messages = mail_numbers[0].split()
        logging.debug('messages={}'.format(messages))

        for message_uid in messages:
            message_uid_i = int(message_uid)
            result, mail_data_raw = self._mailbox.uid('fetch', message_uid, '(RFC822)')
            # yield raw mail body
            if mail_data_raw[0] is not None:
                self.process_mail(message_uid_i, mail_data_raw)

    def process_mail(self, num, mail_data):
        email_header = email.Header
        _mail_header = lambda x: _decoded_val(email_header.decode_header(mail[x]))

        header_decoder = lambda x: email_header.decode_header(mail[x])[0][0]
        mail = None
        try:
            mail = email.message_from_string(mail_data[0][1].decode("utf-8"))
        except Exception as e:
            print('Exception: {}... locals={}'.format(str(e), locals()))
            return
        logging.debug("\n\n %s", "#_" * 90)
        print('sb   = {}'.format(email_header.decode_header(mail['Subject'])))

        subject = _mail_header('Subject').replace('\r\n', '')
        print('_'*80 + '\n\n\n')
        print('...... {}'.format(email_header.decode_header(mail['Subject'])))
        print('UID: {}.... subject = {}'.format(num, subject))
        groups = None
        try:
            groups = re.search(_PATTERN, subject).groupdict()
        except Exception as e:
            print('Exception: {}. locals={}'.format(str(e), locals()))
            pass
        if groups:
            print('dicts = {}'.format(groups))
            mail_date = _mail_header('Date').upper()[5:16].replace(' ', '_')


            text = posadj_s.format(**groups)
            filename = os.path.join(_FOLDER_TO_SAVE, 'posadj_{}_{}.py'.format(mail_date, num))
            write_to_file(filename, text)


        # mail_from = header_decoder('From').upper()
        # mail_date = header_decoder('Date').upper()
        # logging.debug("UID={}; Subject={}\n\tFrom={}\n".format(num, subject, mail_from))
        #
        # logging.debug('Message:: {}'.format(subject))


acc = Account(sys.argv[1] if len(sys.argv) > 1 else 1000)
acc.run()

def copy_files():
    subprocess.Popen(["scp " + _FOLDER_TO_SAVE + '* {}@{}:/tmp/'.format(whoami, _SERVER_TO_COPY_TO)],
                     shell=True).wait()
print('..... ')

copy_files()
