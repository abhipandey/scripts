import json
from itertools import chain

_FILE = 'cicd/puppet-versions.json'

def read_file(path):
    with open(_FILE, 'r') as myfile:
        data=myfile.read().replace('\n', '')
        return data

def get_default_versions():
    s = read_file(_FILE)
    # print('s = {}'.format(s))

    json_dict = json.loads(s)
    # print('version_defaults= {}'.format(json_dict['higgs']['version_defaults'].keys()))
    return json_dict['higgs']['version_defaults']

def get_versions(app_name):
    versions = get_default_versions()
    print('versions[app_name] = {}'.format(versions[app_name]))
    return '|'.join(chain(versions[app_name].values()))

print('alice 2 = {}'.format(get_versions('alice_v2')))
print('risk3ui = {}'.format(get_versions('risk3ui')))