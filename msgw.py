#!/opt/py27env/bin/python2.7

'''
 Remember to set LD_LIBRARY_PATH & PYTHONPATH
 export LD_LIBRARY_PATH=/usr/local/pygh/current/pygh/_run/vendor/
 export PYTHONPATH=/usr/local/pygh/current/pygh/_run/lib/python2.7/site-packages/
'''

'''
Script for MSGW migration

http://jira/browse/RISK-355
'''

_DEBUG = False
_USE_MYSQL = True

_DB_FILE = '/home/abhishek.pandey/code/scripts/cheewah.boboxses-t01.sqlite3.db'

class UatSettings:
    IMMOVABLE_ORDER_BOOKS = [
            'uatSunny.ne',
            'uatLance.ne'
        ]
    BOB_FROM = 'bobcxcme01'
    BOB_TO   = 'CHI/chitrd-ems02/bobcxcmene68'


class ProdSettings:
    IMMOVABLE_ORDER_BOOKS = 'firm.ne/mantis.trader.ne/chuck.carbotwo.ne/chuck.carbo.ne/weizheng.ye.ne/isaac.kwan.ne'.split('/')
    BOB_FROM = 'CHI/chitrd-ems02/bobcxcme01'
    BOB_TO   = 'CHI/chitrd-ems02/bobcxcmene68'


Settings = UatSettings
# Settings = ProdSettings

from base_pygh import *

import sqlite3
import re
from collections import namedtuple

from pyghweb.eve.models import *

PositionDetails = namedtuple('PositionDetails', ('ORDERBOOK', 'SYM', 'LONG', 'SHORT'))


_POS_ADJ_S = '''#!/opt/py27env/bin/python

import os
import sys

pygh_path = '/usr/local/pygh/current/pygh/'
os.chdir(pygh_path)
sys.path.append(pygh_path)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pyghweb.settings")

from django.db.models import Sum, Avg
import django

from pyghweb.eve.requests import SaveAdjustmentRequest
from gh.solace import SimpleApi
from django.conf import settings

print('CACHE_BACKEND = {{}}'.format(settings.CACHE_BACKEND))

solapi = SimpleApi.make_from_django_settings(settings)
req = SaveAdjustmentRequest(solapi)

def send_adj_request(orderbook_name, security_name, qty_long, qty_short):
    resp = req.request(orderbook_name, # order book
                        8, # instrument id type
                        security_name, # instrument id key
                        qty_long, # long adjustment amount
                        qty_short # short adjustment amount
                        )
    if resp.return_code == 0:
        print('Worked. OB: {{}}; Sec={{}}; Long={{}}; Short={{}}'.format(
            orderbook_name, security_name, qty_long, qty_short
        ))
    else:
        print('!!! FAILED. OB: {{}}; Sec={{}}; Long={{}}; Short={{}}'.format(
            orderbook_name, security_name, qty_long, qty_short
        ))

{LINES}

print('done.......')
'''

_LINE = '''send_adj_request('{ORDERBOOK}', '{SECURITY}', {LONG}, {SHORT})
'''



def write_to_file(file_path, text_to_write, write_type='wt'):
    file_path = '/tmp/msgw_' + file_path
    with open(file_path, write_type) as f:
        f.write(text_to_write)
        # f.write(text_to_write if isinstance(bytes, type(text_to_write)) \
        #             else bytes(text_to_write, 'UTF-8'))
        f.close()

compressed_positions = []
if _USE_MYSQL:

    mysql_positions = Position.objects.filter(
                    last_fill__source_app_id__endswith=Settings.BOB_FROM,
                    )\
                    .exclude(order_book__name__in=Settings.IMMOVABLE_ORDER_BOOKS)\
                    .select_related('eve_orderbook')
    if _DEBUG:
        for p in mysql_positions:
            # print('p = {}'.format(pp(vars(p))))
            print('Position<{}>: Instr: {:40}; OrderBook: {:30}: Qty: {}/{}: Adj: {}/{}'.format(
                p.last_fill.source_app_id, p.instrument_key, p.order_book.name,
                p.trade_quantity_long, p.trade_quantity_short,
                p.adjustment_long, p.adjustment_short
            ))

    compressed_positions = [
        PositionDetails(
            ORDERBOOK=p.order_book,
            SYM=p.instrument_key,
            LONG=p.adjustment_long + p.trade_quantity_long,
            SHORT=p.adjustment_short + p.trade_quantity_short
            )
        for p in mysql_positions
        ]
else:
    _SPLITTER = '.*'
    _JSON_FMTS = [
        'key: "(?P<SYM>[|\w]+)',
        'order_book_id: "(?P<ORDERBOOK>[\w\.]+)',
        'position_long: (?P<LONG>-?[0-9]+)',
        'position_short: (?P<SHORT>-?[0-9]+)'
        ]
    _FMT = _SPLITTER.join(_JSON_FMTS)

    conn = sqlite3.connect(_DB_FILE)
    c = conn.cursor()
    rows = c.execute("SELECT * FROM position")

    for row in rows:
        orderbook, instrument = str(row[0]).split('|', 1)
        json = str(row[1])
        # print('json = {}'.format(json))
        # print('json = {} : {}'.format(type(json), json))
        # print('OB: {:30}: instrument: {:30} Position: {}'.format(
        #     orderbook, instrument, json
        # ))

        groups = re.search(_FMT, json, re.MULTILINE | re.DOTALL | re.M)

        print('groups = {}'.format(groups.groupdict()))
        d = groups.groupdict()
        p = namedtuple('PositionDetails', d.keys())(**d)
        print('p = {}'.format(p.SYM))
        compressed_positions.append(p)

    print('mysql_positions = {}'.format(compressed_positions))


def write_into_file(filename, multiplier, normalized_positions, debug=_DEBUG):
    lines = []

    for p in normalized_positions:
        qty_long = p.LONG
        qty_short = p.SHORT
        s = _LINE.format(
            ORDERBOOK=p.ORDERBOOK,
            SECURITY=p.SYM,
            LONG=multiplier * qty_long,
            SHORT=multiplier * qty_short
        )
        if debug:
            print('qty long = {}; type={}'.format(qty_long, type(qty_long)))
        lines.append(('# ' if (qty_long == 0 and qty_short == 0) else '') + s)
        if debug: print(s)
    # print('lines = {}'.format(lines))
    file_content = _POS_ADJ_S.format(
        LINES=''.join(lines)
    )
    write_to_file(filename, file_content)


write_into_file('current_bob.py', -1, compressed_positions)
write_into_file('new_bob.py', 1, compressed_positions)



_SQL = '''
SELECT
  eve_config.id,
  eve_application.appid,
  eve_clearingaccount.account_key,
  ghw_contract.exchange, ghw_contract.ticker,
  eve_orderbook.name,
  eve_tradablegroup.future_product_id
FROM eve_config
JOIN eve_application
JOIN eve_clearingaccount
JOIN eve_tradablegroup
JOIN ghw_contract
JOIN eve_orderbook

ON eve_config.tradable_group_id = eve_tradablegroup.id
    AND eve_config.clearing_account_id = eve_clearingaccount.id
    AND eve_config.application_id = eve_application.id
    AND eve_tradablegroup.future_product_id = ghw_contract.id
    AND eve_orderbook.id = eve_config.order_book_id
WHERE
 appid = '{BOB_FROM}' AND
 eve_orderbook.name NOT IN ({ORDERBOOKS}) AND
 ticker IN ('NIY', 'NKD', 'ENY')
'''
query = _SQL.format(
    BOB_FROM=Settings.BOB_FROM,
    ORDERBOOKS=','.join(map(lambda x: "'" + x + "'",
                                Settings.IMMOVABLE_ORDER_BOOKS))
)

if _DEBUG:
    print('sql = {}'.format(query))

write_to_file('query.sql', query)

_SQL = '''
SELECT eve_config.id, eve_application.appid, eve_clearingaccount.account_key, eve_tradablegroup.future_product_id, eve_tradablegroup.instrument_id, ghw_contract.exchange, ghw_contract.ticker
FROM eve_config
JOIN eve_application
JOIN eve_clearingaccount
JOIN eve_tradablegroup
JOIN ghw_contract ON eve_config.tradable_group_id = eve_tradablegroup.id
AND eve_config.clearing_account_id = eve_clearingaccount.id
AND eve_config.application_id = eve_application.id
AND eve_tradablegroup.future_product_id = ghw_contract.id
WHERE appid LIKE '%{BOB_FROM}'
'''
query = _SQL.format(
    BOB_FROM=Settings.BOB_FROM,
    ORDERBOOKS=','.join(map(lambda x: "'" + x + "'",
                                Settings.IMMOVABLE_ORDER_BOOKS))
)

if _DEBUG:
    print('sql = {}'.format(query))

write_to_file('view_configs.sql', query)


print('''FILES CREATED:
  - /tmp/msgw_current_bob.py
  - /tmp/msgw_new_bob.py
  - /tmp/msgw_query.sql
  - /tmp/msgw_view_configs.sql

REMEMBER to make the /tmp/*.py files executable by running:
  chmod +x /tmp/*.py
''')


