#!/usr/bin/bash
source ~/.bashrc

todos | /usr/local/bin/aha | sed -e 's/color:olive/color:DeepSkyBlue; font-style:italic;/g' -e 's/color:green;/color:LimeGreen;/g' -e 's/<pre>/<pre style="color:gray;">/g'
exit 0
