#!/usr/local/bin/python3.4
from base_rw2 import *

def get_confo(id):
    conf = Confo.objects.get(pk=id)
    return conf

class Line(object):
    def __init__(self, header, line):
        [self.__setattr__(*z) for z in zip(header, line)]
        self.id = None
    def __unicode__(self):
        return '\tLine: {}\n'.format(str(vars(self)))
    def __repr__(self):
        return '\tLine: {}\n'.format(str(vars(self)))

class TradeContractInfo:
    def __init__(self, contract=None, qty=0, price=0.0):
        if contract:
            self.contract, self.year, self.expiry = contract.split('-')
        else:
            self.contract = self.expiry = self.year = None
        self.qty = qty
        self.price = price
        self.settlement_price = 0.0
        self.settlement_diff = 0.0
        self.adjustment = 0.0
        self.adjustment_usd = 0.0
        self.props = {}
    def identifier(self):
        return '{}-{}-{}'.format(self.contract, self.expiry, self.year)
    def __repr__(self):
        return str(self)
    def __str__(self):
        return 'TradeContractInfo <{}>: Qty: {}; Price={}; StlmntPrice={}; StlmntDiff={}; Adj={}; AdjUsd={}; \n\t Props={}'.format(
            self.identifier(),
            self.qty,
            self.price,
            self.settlement_price,
            self.settlement_diff,
            self.adjustment,
            self.adjustment_usd,
            pp(self.props)
        )




def get_confos(c):
    broker_name = c.trade_set.distinct('broker')[0].broker.name
    items = Confo.objects.filter(Q(date__exact=c.date) & Q(client__exact=c.client) & Q(confo_type__exact='t') & Q(filename__iregex='{}.*'.format(broker_name)) )
    t_plus_1_expr = Q(filename__contains='T+1')
    sess_t_plus_1_items = items.filter(t_plus_1_expr)
    sess_t_items = items.exclude(t_plus_1_expr)
    # print('items={}'.format(items))
    # print('sess_t_plus_1_items={}'.format(sess_t_plus_1_items))
    # print('sess_t_items={}'.format(sess_t_items))
    return items, sess_t_items, sess_t_plus_1_items


# tkr = '{}-{}'.format(contract.id, contract.ticker('{}{}'.format(x0[1].upper(), x0[0])))
# _expiration = lambda year, expiry: year*100 + EXPIRY.index(expiry.upper()) + 1
# _tiker = lambda self: contract and contract.ticker('%s%s' % (self.get_expiry_display(), self.year)) or self.symbol
def get_trades_by_contract(confos, debug=False):
    idx_qty = 3
    idx_px = 4
    trade_summary = [c.trade_set.filter(adapter__party__exact='b').values(
                                    'contract',
                                    'side',
                                    'expiry',
                                    'year'
                                    ).annotate(
                                            qty=Sum('qty'), premium=Avg('premium'))
                     for c in confos]
    t_sorted = sorted(
        list(
            chain(*[[
                        (
                            d['contract'],
                            d['year'], d['expiry'],
                            d['qty'] * (-1 if d['side'] == 's' else 1), 
                            d['premium']
                        )
                        for d in t if d['contract']]
                    for t in trade_summary
                    ]))
        , key=lambda x: '{}-{}-{}'.format(x[0] or 0, x[1], x[2]))
    print('t_sorted={}'.format(pp(t_sorted)))
    red_ts = []
    for sec, vals in groupby(t_sorted, lambda x: '{}-{}-{}'.format(x[0] or 0, x[1], x[2])):
        l_vals = list(vals)
        red_ts.append([sec, 
            sum([t[idx_qty] for t in l_vals]),
            sum([t[idx_qty] * t[idx_px] for t in l_vals]) / (sum([t[idx_qty] for t in l_vals]) or 1)])
    print('red_ts={}'.format(pp(red_ts)))
    return red_ts



def create_recon_report(conf):
    c299 = Confo.objects.get(pk='199')
    confos, sess_t_items, sess_t_plus_1_items = get_confos(c)
    print('#_'*80)
    # r1 = get_trades_by_contract([c], True)
    r_trades = _convert_recon_to_contract_trades(c)

    for t in r_trades:
        print('t={}'.format(t))
    cids = list(set([t.contract for t in r_trades]))
    print('cids={}'.format(cids))

    cr_hedge = ContractRelation.objects.filter(contract1__in=cids, date__exact=datetime.date(2015, 4, 3), key__exact='hedge_ratio').values('contract1', 'expiration1', 'contract2', 'expiration2', 'value')
    print('\n\n contract_relations={}'.format(cr_hedge))

    cr_fv = ContractRelation.objects.filter(contract1__in=cids, date__exact=datetime.date(2014, 12,16), key__in=['gradient', 'offset']).values('key', 'contract1', 'expiration1', 'contract2', 'expiration2', 'value')
    print('\n\n FairValues={}'.format(cr_fv))

    update_trade_contract_info(r_trades, cr_hedge, cr_fv)




    all_sec_ids = get_all_securities(r_trades)
    contracts = Contract.objects.filter(pk__in=all_sec_ids)
    contr_dict = dict([ (contr.id, contr) for contr in contracts])
    print('All Contracts={}'.format(all_sec_ids))
    print('Contract Dict for all: {}'.format(pp(contr_dict)))



    delta_groups = get_deltas(r_trades)

    print('\n\n\n')
    e_rates = {
        'USD': 1.0,
        'SGD': 1.3,
        'JPY': 118.89
    }


    # print('contracts={}'.format(contr_dict))
    # print('\n\n\n')

    get_fair_values(r_trades, e_rates, c299, contr_dict, get_contract_symbol_set(c))

    print('\n')
    print('\n')
    print('\n')
    print('#^'*80)
    print('\n')
    print('\n')



    # get_all(c299)
    get_all_syms(confos)

# ---------------------------------------------------------------------------------------------------------------------------------------
# def _convert_recon_to_contract_trades(c):
#     return [TradeContractInfo(trade[0], trade[1], trade[2])
#             for trade in get_trades_by_contract([c])]


def _convert_recon_to_contract_trades(c):
    if isinstance(c, list) or isinstance(c, QuerySet):
        return [TradeContractInfo(trade[0], trade[1], trade[2])
                for trade in get_trades_by_contract(c)]
    return [TradeContractInfo(trade[0], trade[1], trade[2])
            for trade in get_trades_by_contract([c])]


def _convert_recon_to_contract_trades(c):
    if isinstance(c, list) or isinstance(c, QuerySet):
        return [TradeContractInfo(trade[0], trade[1], trade[2])
                for trade in get_trades_by_contract(c)]
    return [TradeContractInfo(trade[0], trade[1], trade[2])
            for trade in get_trades_by_contract([c])]

# ---------------------------------------------------------------------------------------------------------------------------------------

def update_trade_contract_info(ts, hedge_values, fv_values):
    for tci in ts:
        mt = mnth(tci.expiry)
        expiration = '{}{}'.format(tci.year, mt)
        tci.props['expiration'] = expiration
        # print('#_' *80 + '\n' + 'tci.contract={}; expiry={}; yr={}; mt={}; expiration={}'.format(tci.contract, tci.expiry, tci.year, mt, expiration))

        # l = [ (f['contract1'], tci.contract, type(f['contract1']), type(tci.contract)) for f in fv_values]
        # print('pp={}'.format(pp(l)))

        fv = [f for f in fv_values if str(f['contract1']) == tci.contract 
            and str(f['expiration1']) == expiration]

        if fv:
            fvs = fv[0].copy()
            # print('fvs BEFORE: {}'.format(fvs))
            [fvs.update({dset['key']: dset['value']}) for dset in fv]
            # print('fvs AFTER: {}'.format(fvs))
            tci.props['fair_value'] = fvs


        hv = next( (h for h in hedge_values if str(h['contract1']) == tci.contract), None)
        # print('FOUND.... fv={} \n\t hedge = {}'.format(fv, hv))
        tci.props['hedge'] = hv
        # print('TCI={}'.format(tci))


def get_all_securities(ts):
    #c2  = lambda tci, : tci.props[k] if k in tci.props else ''
    cc = lambda tci, k, contr: tci.props[k][contr] if tci and k in tci.props and tci.props[k] and contr in tci.props[k] else ''
    c2 = lambda tci, k: cc(tci, k, 'contract2')
    c1 = lambda tci, k: cc(tci, k, 'contract1')
    sec_ids = sorted(list(set([str(i) for i in chain(*[[
                            c1(tci, 'fair_value'),
                            c1(tci, 'hedge'),
                            c2(tci, 'fair_value'),
                            c2(tci, 'hedge'),
                            tci.contract]
                        for tci in ts]) if i])))
    return sec_ids



def get_deltas(ts):
    deltas = defaultdict(lambda: 0.0)
    for tci in ts:
        # print('__#'*80)
        # print('tci={}'.format(tci))
        hdg_dict = tci.props['hedge'] if 'hedge' in tci.props else None
        if hdg_dict:
            # print('tci contract = {}; hdg_dict={}'.format(tci.contract, hdg_dict))
            hedge_ratio = float(hdg_dict['value'])
            group = hdg_dict['contract2']
            normalized_position = tci.qty * hedge_ratio
            deltas[group] += normalized_position
            # print('Sec={}; hedge_ratio={}; grp={}; qty={}; normalized_position={}'.format(
            #     tci.contract, hedge_ratio, group, tci.qty, normalized_position
            #     ))

    print('Deltas={}'.format(pp(deltas)))
    return deltas

def get_contract_symbol_set(conf):
    adp = c.trade_set.distinct('adapter').select_related('contractsymbol')
    # print('csym = {}; type={}; set={}'.format(tt.adapter, type(tt.adapter), tt.adapter.contractsymbol_set.all()))
    syms = {}
    if adp:
        for contractsymbol in adp[0].adapter.contractsymbol_set.all():
            syms[contractsymbol.contract.id] = contractsymbol
        return syms
    return None

def get_trade_price(tci, all_trades, contract):
    trds = [trd for trd in all_trades if trd.contract == contract and tci.props['expiration'] == trd.props['expiration']]
    print('trds={}'.format(trds))
    return trds[0] if trds.price else None

def get_fair_values(ts, exch_rates, sess_t_confo, contract_dict, contractsymbol_set):
    sess_t_trades = _convert_recon_to_contract_trades(sess_t_confo)
    print('fv len={}; sess_t_items={}'.format(len(ts), len(sess_t_trades)))
    print('session_t_adj_trades={}'.format(pp(sess_t_confo)))
    for tci in ts:
        print('__#'*80)
        # print('tci={}'.format(tci))
        fv_dict = tci.props['fair_value'] if 'fair_value' in tci.props else None

        contract_size = contractsymbol_set[int(str(tci.contract))].multiplier
        size = tci.qty
        if fv_dict:
            print('tci={}-{}; FV Dict={}; contr_multiplier={}'.format(
                tci.contract, tci.props['expiration'], fv_dict, contract_size))
            
            gradient = fv_dict['gradient']
            offset = fv_dict['offset']
            print('tci={}-{}; size={}; gradient={}; offset={}'.format(
                tci.contract, tci.props['expiration'], size, gradient, offset))
            tci.settlement_price = tci.price
            tci.settlement_diff = offset
            ref_tci = None
            tci_sym = fv_dict['contract1']
            ref_sym = fv_dict['contract2']
            print('ref = {}'.format(ref_sym))

            # If same sec exists in sess1, we take it's price
            t_sess = [t for t in sess_t_trades if t.identifier() == tci.identifier()]
            if t_sess:
                print('Found settlement_price from prev sess: t_sess={}'.format(t_sess))
                tci.settlement_price = t_sess[0].settlement_price
            else:
                t_ref = [trd for trd in ts if trd.contract == ref_sym and tci.props['expiration'] == trd.props['expiration']]
                if t_ref:
                    print('Found settlement_price from ref sym: t_ref={}; gradient={}; stlmt_diff={}'.format(
                        t_ref, gradient, tci.stlmt_diff))
                    tci.settlement_price = (t_ref[0].price * gradient) + tci.stlmt_diff
        tci.adjustment = tci.qty * size * (tci.settlement_price - tci.price)
        cntr = contractsymbol_set[int(str(tci.contract))]
        curr = cntr.contract.get_ccy_display().upper().strip()
        if curr == 'USD':
            tci.adjustment_usd = tci.adjustment
        else:
            rate = exch_rates[curr] if curr in exch_rates else 1
            print('exch_rate for curr: {} = {}'.format(curr, rate))
            tci.adjustment_usd = tci.adjustment / rate
            
        # print('tci: contract={}; type={}; cntr={}'.format(tci.contract, type(tci.contract), cntr.contract.get_ccy_display()))
        
    # print('FINALIZED ADJUSTED PRICES={}'.format(ts))


def get_exch_rate(conf):
    _conversion_dict = {
        'EURO': 'EUR',
        'BRITISH POUND': 'GBP',
        'HONG KONG DLR': 'HKD',
        'JAPANESE YEN': 'JPY',
        'SINGAPORE DOLLAR': 'SGD'
    }
    rates = {}
    lines  = AccountLine.objects.filter(confo__id=c.id, info_type__exact='CURRENCY CONVERSION RATE TO US', account__exact='M')
    for l in lines:
        print('symbol={}; value={}'.format(l.symbol, l.content))
        rates[_conversion_dict[l.symbol] if l.symbol in _conversion_dict else l.symbol] = 1 / float(l.content)
    print('rates  = {}'.format(rates))
    return rates

# get_exch_rate(c)


def get_all_syms(cids):
    syms = {}
    a = Trade.objects.filter(confo_id__in=cids.values('id')).distinct('adapter').select_related('contractsymbol')
    for i in a:
        for sym in i.adapter.contractsymbol_set.all():
            syms[sym.contract.id] = sym
    return syms


def test_sessions():
    sessions = Session.objects.all()

    for sess in sessions:
        print('sess={}; vars={}; \n\t fromto={}'.format(sess, vars(sess), sess.fromto(datetime.date(2015, 4, 3))))

    sessions = Session.objects.filter(exchange__exact='o')[:1]

    for sess in sessions:
        print('sess={}; vars={}; \n\t fromto={}'.format(sess, vars(sess), sess.fromto(datetime.date(2015, 4, 3))))

