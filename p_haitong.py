#!/home/abhishek.pandey/code/scripts/ve_scripts/bin/python3.5
import zipfile
from decimal import *
from pprint import *
import sys
import imaplib
import csv
import email
import csv
import os
import base64
import logging
import re
logging.basicConfig(
    filename='/var/reconwise/logs/haitong.log',
    level=logging.DEBUG,
    format="%(asctime)s - %(levelname)s - %(message)s")


_VALID_FILE_NAME = '80801035JSD'
LOCAL_PATH = '/home/abhishek.pandey/code/scripts/'
_HAITONG_PERCENT_FILE = os.path.join(LOCAL_PATH, 'haitong_percents.csv')
f0 = folder_stmts = LOCAL_PATH + '/haitong'
f1 = folder_recon_ouputs = '/Users/apandey/Documents/TamTech/NE Reconcilers - Output'
# f0 = folder_stmts = '/Users/apandey/Documents/TamTech/ne_stmt'
# f1 = folder_recon_ouputs = '/Users/apandey/Documents/TamTech/ne_output'

_try_decode = lambda v: v.decode('utf-8') if isinstance(v, bytes) else v
_decoded_val = lambda vals: ([_try_decode(x[0]) for x in vals if not x[1]] or
                     [x[0].decode(x[1]) for x in vals if x[1]])[0].strip()

_HAITONG_UID_FILE = os.path.join(LOCAL_PATH, '_haitong.uid')


from email import encoders
from email.mime.base import *
from email.mime.multipart import *
from email.mime.text import *
from email.utils import COMMASPACE, formatdate
import email.utils
import smtplib

from openpyxl import load_workbook


def _read_excel():

    wb = load_workbook(filename='China SHFE Comissions schedule.xlsx', read_only=True)
    ws = wb['big_data'] # ws is now an IterableWorksheet

    for row in ws.rows:
        for cell in row:
            logging.info(cell.value)


_STYLES = {
    'TABLE': """ style="width: 100%;border-collapse: collapse;-webkit-box-shadow: 10px 10px 17px -2px rgba(63, 114, 133, 0.93);-moz-box-shadow: 10px 10px 17px -2px rgba(63, 114, 133, 0.93);box-shadow: 10px 10px 17px -2px rgba(63, 114, 133, 0.93);margin-bottom: 50px;border: 1px solid #D1E7FF" """,
    'TH': """  style="border: 1px solid #D1E7FF;font-variant: small-caps;font-size: 1.2em;padding: 3px;color: #444;font-weight: 500;background-image: radial-gradient(#5FD7FF, #48D1FF)" """,
    'TD': """  style="border: 1px solid #D1E7FF;padding: 3px 20px 4px 10px;font-size: 0.8em" """
}


def send_mail(subject, body,
              recipients='abhishek',
              author='abhishek.pandey@grasshopperasia.com',
              author_name='Abhishek',
              recipient_name='Stakeholders'):
    # print('send _mail , locals={}'.format(locals()))
    server = 'smtp.gmail.com:587'
    if True:
        send_to = ['abhishek.pandey@grasshopperasia.com',
                    'tracy.tan@grasshopperasia.com',
                    'nashon.loo@grasshopperasia.com']
    else:
        send_to = ['abhishek.pandey@grasshopperasia.com',
               ]
    msg = MIMEMultipart()
    msg['From'] = author_name
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(body, 'html'))

    smtp = smtplib.SMTP(server)
    smtp.ehlo()
    smtp.starttls()
    smtp.login(author, 'Tsuguri0')
    smtp.sendmail(author, send_to, msg.as_string())
    smtp.close()

_BODY = '''
<h1>Haitong Commissions</h1>

<table{TBL_STYLE}>
    <thead>
        <tr>
            <th{TH_STYLE} rowspan="2">Exchange</th>
            <th{TH_STYLE} rowspan="2">Instrument</th>
            <th{TH_STYLE} rowspan="2">Expiry</th>
            <th{TH_STYLE} rowspan="2">Price</th>
            <th{TH_STYLE} rowspan="2">Commission</th>
            <th{TH_STYLE}>To Open</th>
            <th{TH_STYLE} colspan="2">To Close</th>
        </tr>
        <tr>
            <th{TH_STYLE}>Same Day</th>
            <th{TH_STYLE}>Same Day</th>
            <th{TH_STYLE}>Next Day</th>
        </tr>
    </thead>
    <tbody>
        {ROWS}
    </tbody>
</table>
'''
_FMT = '''
        <tr>
            <td{STYLE}>{EXCH}</td>
            <td{STYLE}>{INSTR}</td>
            <td{STYLE}>{EXPIRY}</td>
            <td{STYLE}>{PRICE}</td>
            <td{STYLE}>{COMM}</td>
            <td{STYLE}>{OPEN_SAME_DAY}</td>
            <td{STYLE}>{CLOSE_SAME_DAY}</td>
            <td{STYLE}>{CLOSE_NEXT_DAY}</td>
        </tr>
'''

def _read_haitong_csv(file_path, subject):
    subject = re.findall('.*-(.*)-.*', subject, re.IGNORECASE)[0]
    subject = 'Haitong Commission - ' + subject
    data = _get_haitong_csv_data(file_path)
    # print('data = {}'.format(data))
    vals = _get_interesting_tpls(data)
    commissions_by_sec = _get_highest_commissions_by_security(vals)
    logging.info('_'*80)
    logging.info('commissions = {}'.format(commissions_by_sec))
    html = _create_html(commissions_by_sec)
    send_mail(subject, html)


def read_haitong_percents(percent_file=_HAITONG_PERCENT_FILE):

    # f = open(file_path, 'rt', encoding='cp936')
    f = open(percent_file, 'rt', encoding='utf-8')
    data = []
    try:
        reader = csv.reader(f)
        for row in reader:
            data.append(row)
    finally:
        f.close()
    percents = dict([x[0].upper(), x[1:]] for x in data[2:])
    return percents

_PERCENTS = read_haitong_percents()

def _get_haitong_csv_data(file_path):
    # f = open(file_path, 'rt', encoding='cp936')
    f = open(file_path, 'rt', encoding='gbk')
    data = []
    try:
        reader = csv.reader(f)
        for row in reader:
            data.append(row)
    finally:
        f.close()
    return data

def _get_interesting_tpls(data):
    state = False
    vals = []
    for row in data:
        if row[1].upper() == 'TRADE DATE':
            state = True
            continue
        if row[1].upper() == '':
            state = False

        if state and row[10].upper().strip() == 'OPEN':
            tpls = [row[x]
                    for x in [2, 3, 4, 7, 8, 11]]
            logging.info('tpls = {}'.format(tpls))
            sys.stdout.flush()

            tpls_reduced = tpls[:4] + [
                     '{:.2f}'.format((float(tpls[5]) / float(tpls[4])))
                     ]
            if tpls_reduced[-1] != 0:
                vals.append(tpls_reduced)

    return vals
def _get_highest_commissions_by_security(data):
    res = []
    from itertools import groupby
    for name, v in groupby(data, key=lambda x: '-'.join(x[:3])):
        vals = sorted(v, key=lambda x: -float(x[4]))
        logging.info('name = {}. vals={}'.format(name, vals[0]))
        res.append(vals[0])
    return res

def _create_html(data):
    cleaned = lambda x: x.upper().strip()
    percent = lambda instr, index: \
        _PERCENTS[cleaned(instr)][index] \
        if cleaned(instr) in _PERCENTS \
            else 'NA'
    rows = ''.join([_FMT.format(
        EXCH=x[0],
        INSTR=x[1],
        EXPIRY=x[2],
        PRICE=x[3],
        COMM=x[4],
        STYLE=_STYLES['TD'],
        OPEN_SAME_DAY=percent(x[1], 0),
        CLOSE_SAME_DAY=percent(x[1], 1),
        CLOSE_NEXT_DAY=percent(x[1], 2),
    ) for x in data])
    text = _BODY.format(
        ROWS=rows,
        TBL_STYLE=_STYLES['TABLE'],
        TH_STYLE=_STYLES['TH']
    )
    logging.info('text = {}'.format(text))
    return text


def get_file_text(file_path):
    if os.path.exists(file_path):
        with open(file_path, 'rt') as file_placeholder:
            return '\n'.join(file_placeholder.readlines())
    return ''

def _save_haitong_uid(latest_uid):
    with open(_HAITONG_UID_FILE, 'wt') as file_pc:
        file_pc.writelines([str(latest_uid)])
    logging.info('File {} saved with UID: {}'.format(
        _HAITONG_UID_FILE, latest_uid
    ))

def _read_haitong_uid():
    uid = get_file_text(_HAITONG_UID_FILE)
    if not uid:
        return 0
    return int(uid)

class Account:
    def __init__(self):
        self.latest_uid = _read_haitong_uid()
        logging.info('Latest UID so far = {}'.format(self.latest_uid))

    def login(self):
        logging.debug("Email_Service::_check_mail:: Time to check mail")
        self._mailbox = imaplib.IMAP4_SSL('imap.gmail.com')

        try:
            rv, data = self._mailbox.login('tilde.recon@grasshopperasia.com', 'Tsuguri0')
            logging.debug("Worked for abhishek")
            logging.debug("EMAIL DATA:%s ", data)
        except imaplib.IMAP4.error:
            logging.debug("LOGIN FAILED for abhishek")
            return False
        return True

    def save_uid(self):
        _save_haitong_uid(self.latest_uid)

    def logout(self):
        self.save_uid()
        self._mailbox.logout()

    def run(self):
        if self.login():
            self.read_mails()
            self.logout()

    def read_mails(self):
        rv, data = self._mailbox.select('Inbox')
        result, mail_numbers = self._mailbox.uid('search', None, r'(Subject "Settlement Statement")')
        messages = mail_numbers[0].split()
        logging.debug('messages={}'.format(messages))

        for message_uid in messages:
            message_uid_i = int(message_uid)
            if message_uid_i <= self.latest_uid:
                continue
            # SEARCH command *always* returns at least the most
            # recent message, even if it has already been synced
            result, mail_data_raw = self._mailbox.uid('fetch', message_uid, '(RFC822)')
            self.latest_uid = message_uid_i
            self.save_uid()
            # yield raw mail body
            if mail_data_raw[0] is not None:
                self.process_mail(message_uid_i, mail_data_raw)


    def process_mail(self, num, mail_data):
        _mail_header = lambda x: _decoded_val(email.header.decode_header(mail[x]))
        header_decoder = lambda x: email.header.decode_header(mail[x])[0][0]
        mail = email.message_from_string(mail_data[0][1].decode("utf-8"))
        logging.debug("\n\n %s", "#_" * 90)
        subject = _mail_header('Subject')
        sbu = subject.upper()
        all_subs = [x[0].upper() for x in items]
        found = False
        logging.info('locals = {}'.format(subject))

        try:
            found = next((s for s in all_subs if s in sbu), None)
        except Exception as e:
            logging.info('Exception: e: {}'.format(str(e)))
            logging.exception('Exception: e: {}'.format(str(e)))
        # if sbu not in all_subs:
        if not found:
            logging.debug('Subject not matching. Ignoring {}'.format(subject))
            return
        if mail.get_content_maintype() != 'multipart':
            return
        folder = next((d[1] for d in items if d[0].upper() in sbu), '')
        if not folder:
            return

        mail_from = _mail_header('From').upper()
        mail_date = _mail_header('Date').upper()
        logging.debug("UID={}; Subject={}\n\tFrom={}\n".format(num, subject, mail_from))
        logging.info("UID={}; Subject={}\n\tFrom={}\n".format(num, subject, mail_from))

        logging.debug('Message:: {}'.format(subject))
        logging.debug('-' * 30 + '\n\t')


        for part in mail.walk():
            if part.get_content_maintype() == 'multipart':
                                continue

            # Confirm that there's an attachment ?
            if part.get('Content-Disposition') is None:
                                continue


            filename = part.get_filename()
            if filename.startswith('=?UTF-8?B?'):
                p = filename[10:]
                p1 = base64.b64decode(p)
                tmp_filename = p1.decode('ascii', "ignore")

                if tmp_filename.startswith('80801035JSD'):
                    filename = tmp_filename
                else:
                    continue

            logging.info('filename = {}'.format(filename))
            if _VALID_FILE_NAME not in filename:
                continue

            att_path = os.path.join(folder, filename)
            logging.debug('att_path ={}'.format(filename))

            # if os.path.exists(att_path.replace('.zip', '.xls')):
            #     continue
            logging.debug("Path on OS=%s", att_path)

            # finally write the stuff
            fp = open(att_path, 'wb')
            email_data_file = part.get_payload(decode=True)
            fp.write(email_data_file)
            fp.close()

            logging.debug("FILE SAVED: %s", att_path)
            _read_haitong_csv(att_path, subject)

items = [
        ('Settlement Statement', f0),
        ]

acc = Account()
acc.run()
