import zipfile
from decimal import *
from pprint import *
import sys
import imaplib
import email
import os
import re
import datetime
import logging
import subprocess
import os


from email import encoders
import email
from email.mime.base import *
from email.mime.multipart import *
from email.mime.text import *
from email.utils import COMMASPACE, formatdate
import email.utils
import smtplib
import os
import logging

from bs4 import BeautifulSoup
from urllib import request
import re

from inlinestyler.utils import inline_css


def send_mail(subject, body, recipients,
              author='abhishek.pandey@grasshopperasia.com',
              author_name='Abhishek',
              password='Tsuguri0',
              recipient_name='Stakeholders'):
    # Create the message
    # msg = MIMEText(body)
    # msg['To'] = email.utils.formataddr((recipient_name, recipients[0]))
    # msg['From'] = email.utils.formataddr((author_name, author))
    # msg['Subject'] = subject

    msg = MIMEMultipart()
    msg['From'] = email.utils.formataddr((author_name, author))
    msg['To'] = COMMASPACE.join(recipients)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(body, 'html'))



    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    # server.set_debuglevel(True) # show communication with the server
    server.starttls()
    server.login(author, password)
    try:
        server.sendmail(author, recipients, msg.as_string())
    finally:
        server.quit()




FIND_TEXT = ['shorts', 'selvedge', 'sew', 'dry ex']

HTML = '''<!DOCTYPE html>
<html>
<head>
    <title>Uniqlo sale</title>
    <style>
        table {{
            border: 1px solid black;
            border-collapse: collapse;
            padding: 40px;
            box-shadow: 10px 10px 5px #888888;
        }}
        th, td {{
            padding: 5px;
            border: 1px solid gray;
            text-align: center;
            vertical-align: middle;
        }}
        th {{
            font-size: 1.2em;
        }}
        span {{
            text-decoration: line-through;
        }}
    </style>
</head>
<body>
<table>
<tbody>
    <tr>
        <th>Product</th>
        <th>Price</th>
        <th>Online?</th>
    </tr>

{ROWS}
</tbody>
</table>

</body>
</html>
'''

_ROW_TR = '''
    <tr>
        <td><strong>{PROD}</strong><br><img alt="img" src="{IMG}"></td>
        <td><span>{ORIG_PX}</span><br>{NEW_PX}</td>
        <td>{ONLINE}</td>
    </tr>
'''



def find_rows(u):
    u  =  'http://www.uniqlo.com/sg/store/men/featured/sale.html'
    r = request.urlopen(u).read()
    soup = BeautifulSoup(r, 'html.parser')

    data = []
    for li in soup.findAll('li', class_='item'):
        classes = li.get('class', '')

        img = li.find_all('img')[0]['data-original'][2:]
        old_price = li.find('p', 'old-price').get_text()
        special_price = li.find('p', 'special-price').get_text().strip()
        name = li.find('h2', 'product-name').get_text()
        tag_online_special = len(li.find_all('span', 'onlineexcl'))
        # print('name = {}; online?={}'.format(name, tag_online_special))
        # print('img = {}'.format(img))
        # print('old price = {}'.format(old_price))
        # print('special_price = {}'.format(special_price))

        if any([re.search(t, name, re.IGNORECASE) for t in FIND_TEXT]):
            data .append([name, img, old_price, special_price,
                     'Yes' if tag_online_special else '-'])
    return data


def handler(event, context):
	data = find_rows('http://www.uniqlo.com/sg/store/men/featured/sale.html')
	data += find_rows('http://www.uniqlo.com/sg/store/men/featured/limited-offer.html')

	uniq_names = list( set([x[0] for x in data]) )
	uniq_data = [next(x for x in data if x[0] == n) for n in uniq_names]

	rows_text = ''.join([_ROW_TR.format(
	                PROD=x[0],
	                IMG=x[1],
	                ORIG_PX=x[2],
	                NEW_PX=x[3],
	                ONLINE=x[4]
		) for x in uniq_data])

	html_text = HTML.format(ROWS=rows_text)


	send_mail('Uniqlo Sale',
	          inline_css(html_text),
	          ['abhishek@tilde.sg'])
