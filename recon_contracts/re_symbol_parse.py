import traceback
import sys
from math import fabs
from itertools import chain
from functools import reduce
import operator
#from django.http import HttpResponse, HttpResponseBadRequest
#from django.shortcuts import render, get_object_or_404

#from django.db.models.query_utils import Q
#from django.conf import settings
# from django.views.decorators.csrf import csrf_exempt
from slugify import slugify
import xlrd
import uuid
from simplejson import dumps
import re
from enum import Enum
import os


import pprint
from django.db.models import Sum, Avg, Count
import django, os, sys
from django.db.models import F, FloatField
from django.db.models.query import QuerySet
from collections import defaultdict
import pprint, datetime
from itertools import chain, groupby


rw_path = '/home/abhishek.pandey/code/tildePython/reconwisev2/rw'

os.chdir(rw_path)
sys.path.append(rw_path)
print('path={}'.format(os.getcwd()))


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rw.settings")
os.chdir(rw_path)
django.setup()

from confo.views import *
from confo.models import *
from broker_parser.models import *
from rw.utils import TZ_CHOICES, A_DAY, EXPIRY


TradeAdapter_ID_GH = 1

from confo.models import split_apt, split_apt0, EXPIRY
from confo.models import Session, Client, Broker, split_fld, catattrs, UserDefinedGroup



def parse_attr(line, fld):
    """
    Parse the line using fld

    :param line: a rw.util.Line class containe line
    :param fld: a string class contain regular expression for matching and extraction value

    :return: string of the match on the field
    """
    fld_apt = split_apt(fld)
    data = first_filtered(catattrs(line, fld_apt[0]))
    if len(fld_apt) > 1 and fld_apt[1]:
        found = re.search(fld_apt[1], data)
        if not found:
            return None
        data = first_filtered(found.groups())
    if len(fld_apt) > 2 and len(data):
        if isinstance(data, float) or (str(data).replace('.','',1).isdigit() and len(str(data)) in [6, 7]):
            if len(str(data)) == 6 and fld_apt[0] == 'Maturity':
                return dt.strptime(data, fld_apt[2])
            return datetime.datetime(*xldate_as_tuple(data if isinstance(data, float) else float(data), 0))
        return dt.strptime(data, fld_apt[2])
    return data

Line = namedtuple('Line', ('exchange', 'contract_symbol'))

first_filtered = lambda g: (list(filter(lambda a:a, g)) or [''])[0]

def check_mappings(exch, symbol):
    adapter = TradeAdapter.objects.filter(id__exact=TradeAdapter_ID_GH)[0]
    print('re = {}'.format(adapter))


    hasattrs = lambda obj, attrs: reduce(operator.__and__, [hasattr(obj, attr) for attr in split_fld(attrs)], True)

    col = lambda fld: split_apt0(getattr(adapter, fld))
    cleaned_attr = lambda fld: getattr(adapter, fld) or ''
    fmt = lambda fld: cleaned_attr(fld).strip('<>')
    parse = lambda fld: parse_attr(line, fmt(fld)) if hasattrs(line, col(fld)) else fmt(fld)
    f = 'symbol_col'

    line = Line(exch, symbol)  # {'exchange': exch, 'contract_symbol': symbol}
    symbol = parse(f)
    print('symbl ' + symbol)


    print('1 = {}'.format(f))
    print('col(fld) = {}'.format(col(f)))
    print('split_fld = {}'.format(split_fld(col(f))))
    print('hasattrs = {}'.format(hasattrs(line, col(f))))
    # print('1 = {}'.format())

check_mappings('TOC', 'GOLD180626')
