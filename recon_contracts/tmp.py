import traceback
import sys
from math import fabs
from itertools import chain
from functools import reduce
import operator
#from django.http import HttpResponse, HttpResponseBadRequest
#from django.shortcuts import render, get_object_or_404

#from django.db.models.query_utils import Q
#from django.conf import settings
# from django.views.decorators.csrf import csrf_exempt
from slugify import slugify
import xlrd
import uuid
from simplejson import dumps
import re

import os


import pprint
from django.db.models import Sum, Avg, Count
import django, os, sys
from django.db.models import F, FloatField
from django.db.models.query import QuerySet
from collections import defaultdict
import pprint, datetime
from itertools import chain, groupby


rw_path = '/home/abhishek.pandey/code/tildePython/reconwisev2/rw'

os.chdir(rw_path)
sys.path.append(rw_path)
print('path={}'.format(os.getcwd()))


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rw.settings")
os.chdir(rw_path)
django.setup()

from confo.views import *
from confo.models import *
from broker_parser.models import *
from rw.utils import TZ_CHOICES, A_DAY, EXPIRY
from tenant_schemas.utils import schema_context


import json
from v1.models import Tblonetimeuid


CREATE_SYMBOLS = False
CREATE_CONTRACT_SYMBOLS = False
TradeAdapter_ID_GH = 1
TradeAdapter_ID_SOCGEN = 15
TradeAdapter_ID_TSE_CASH = 16

from django.db.models import Sum, Max, F, DecimalField, ExpressionWrapper
from recon_report_1.info_collector import *
from recon_report_1.info_collector import _find_contract_syms_universe

import timeit as timeit




def read_data1():

    with schema_context('grasshopper'):
    # All comands here are ran under the schema `schema_name`
        syms = _find_contract_syms_universe([10339, 10325, 10330, 10331, 10334, 10335, 10332, 10333, 10319, 10332])
        get_exch_fees_1(syms)
        print('lens = {}'.format(len(syms)))
        return len(syms)



def read_data2():

    with schema_context('grasshopper'):
    # All comands here are ran under the schema `schema_name`
        syms = _find_contract_syms_universe([10339, 10325, 10330, 10331, 10334, 10335, 10332, 10333, 10319, 10332])
        get_exch_fees_2(syms)
        print('lens = {}'.format(len(syms)))
        return len(syms)


a = timeit.timeit(read_data1, number=1)
print('results = {}'.format(a))

a = timeit.timeit(read_data2, number=1)
print('results = {}'.format(a))
