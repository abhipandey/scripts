import traceback
import sys
from math import fabs
from itertools import chain
from functools import reduce
import operator
#from django.http import HttpResponse, HttpResponseBadRequest
#from django.shortcuts import render, get_object_or_404

#from django.db.models.query_utils import Q
#from django.conf import settings
# from django.views.decorators.csrf import csrf_exempt
from slugify import slugify
import xlrd
import uuid
from simplejson import dumps
import re

import os


import pprint
from django.db.models import Sum, Avg, Count
import django, os, sys
from django.db.models import F, FloatField
from django.db.models.query import QuerySet
from collections import defaultdict
import pprint, datetime
from itertools import chain, groupby


rw_path = '/home/abhishek.pandey/code/tildePython/reconwisev2/rw'

os.chdir(rw_path)
sys.path.append(rw_path)
print('path={}'.format(os.getcwd()))


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rw.settings")
os.chdir(rw_path)
django.setup()

from confo.views import *
from confo.models import *
from broker_parser.models import *
from rw.utils import TZ_CHOICES, A_DAY, EXPIRY




CREATE_SYMBOLS = False
CREATE_CONTRACT_SYMBOLS = False
TradeAdapter_ID_GH = 1
TradeAdapter_ID_SOCGEN = 15
TradeAdapter_ID_TSE_CASH = 16

def check_mappings(allow_deletes=True):
    cs = sorted(ContractSymbol.objects.all(), key=lambda x: x.adapter.name)
    for ta, mappings in groupby(cs, key=lambda x: x.adapter):
        l_symbols = defaultdict(lambda: 0)
        l_contracts = defaultdict(lambda: 0)
        mappings_copy = list(mappings)
        for relation in mappings_copy:
            l_symbols[relation.symbol] += 1
            l_contracts[relation.contract.get_symbol_display()] += 1
            # print('{} ::: Relations={}'.format(ta, relation))

        err_symbols = [x for x in l_symbols if l_symbols[x] > 1]
        err_contracts = [x for x in l_contracts if l_contracts[x] > 1]
        if err_symbols:
            print('{} lsymbols = {}'.format(ta, err_symbols))
        if err_contracts:
            print('{} l_contracts = {}'.format(ta, err_contracts))

        if allow_deletes:
            ids_to_del = []
            for e in err_symbols:

                ids_to_del += [m for m in mappings_copy if m.symbol == e][1:]
                print('e : {}. IDS={}'.format(
                    e, sorted([m.id for m in mappings_copy if m.symbol == e])
                ))

            print('ids_to_del = ', ids_to_del)
            for m in ids_to_del:
                m.delete()

                # print('Adapter: {}; len={}'.format( ta, len(list(mappings))))

check_mappings(False)
