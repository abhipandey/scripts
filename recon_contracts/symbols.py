import traceback
import sys
from math import fabs
from itertools import chain
from functools import reduce
import operator
#from django.http import HttpResponse, HttpResponseBadRequest
#from django.shortcuts import render, get_object_or_404

#from django.db.models.query_utils import Q
#from django.conf import settings
# from django.views.decorators.csrf import csrf_exempt
from slugify import slugify
import xlrd
import uuid
from simplejson import dumps
import re

import os


import pprint
from django.db.models import Sum, Avg, Count
import django, os, sys
from django.db.models import F, FloatField
from django.db.models.query import QuerySet
from collections import defaultdict
import pprint, datetime
from itertools import chain, groupby


rw_path = '/home/abhishek.pandey/code/tildePython/reconwisev2/rw'

os.chdir(rw_path)
sys.path.append(rw_path)
print('path={}'.format(os.getcwd()))


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rw.settings")
os.chdir(rw_path)
django.setup()

from confo.views import *
from confo.models import *
from broker_parser.models import *
from rw.utils import TZ_CHOICES, A_DAY, EXPIRY




CREATE_SYMBOLS = False
CREATE_CONTRACT_SYMBOLS = False
TradeAdapter_ID_GH = 1
TradeAdapter_ID_SOCGEN = 15
TradeAdapter_ID_TSE_CASH = 16


def read_data():
    lines = AccountLine.objects.filter(
        info_type__exact='EXCHFEES',
        contract__isnull=False)\
        .values('symbol', 'contract')\
        .annotate(Max('id'))\
        .annotate(Max('content'))
    items = []
    for line in lines:
        l = named_tuple_from_dict(line)
        # Log.debug('Exch Fees: line={}'.format(l))
        Log.debug('ExchangeFees --> {} ({}) = {} '.format(l.symbol, l.contract, l.content__max))
        items.append([
            l.contract, l.symbol, l.content__max
        ])
    return items


def named_tuple_from_dict(dictionary):
    """

    :param dictionary: dictonary, example
            {'content__max': 'USD:-0.7200000000000001', 'contract': 6, 'symbol': 'AUG 15 SGX NIKKEI IDX', 'id__max': 9910}
    :return: a line tuple, example
             LineTuple(content__max='USD:-0.7200000000000001', contract=6, symbol='AUG 15 SGX NIKKEI IDX', id__max=9910)
    """
    return namedtuple('LineTuple', dictionary.keys())(**dictionary)

read_data()