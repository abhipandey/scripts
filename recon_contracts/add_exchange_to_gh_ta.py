import traceback
import sys
from math import fabs
from itertools import chain
from functools import reduce
import operator
#from django.http import HttpResponse, HttpResponseBadRequest
#from django.shortcuts import render, get_object_or_404

#from django.db.models.query_utils import Q
#from django.conf import settings
# from django.views.decorators.csrf import csrf_exempt
from slugify import slugify
import xlrd
import uuid
from simplejson import dumps
import re
from enum import Enum
import os


import pprint
from django.db.models import Sum, Avg, Count
import django, os, sys
from django.db.models import F, FloatField
from django.db.models.query import QuerySet
from collections import defaultdict
import pprint, datetime
from itertools import chain, groupby


rw_path = '/home/abhishek.pandey/code/tildePython/reconwisev2/rw'

os.chdir(rw_path)
sys.path.append(rw_path)
print('path={}'.format(os.getcwd()))


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rw.settings")
os.chdir(rw_path)
django.setup()

from confo.views import *
from confo.models import *
from broker_parser.models import *
from rw.utils import TZ_CHOICES, A_DAY, EXPIRY


TradeAdapter_ID_GH = 1
TradeAdapter_ID_SOCGEN = 15
TradeAdapter_ID_TSE_CASH = 16

class SymbologyType(Enum):
    ADD = 1
    REMOVE = 2

def check_mappings(symbology_type):
    exchanges = set()
    exchanges_to_use = {'CME': 'CME', 'DCE': 'DCE', 'CMX': 'CME', 'SGX': 'SGX', 'OSE': 'OSE',
     'TSE': 'TSE', 'TOC': 'TOC', 'SHF': 'SHF', 'CBT': 'CBT'}

    cs = sorted(ContractSymbol.objects.filter(adapter_id=1), key=lambda x: x.contract.exchange)
    for exch, mappings in groupby(cs, key=lambda x:  x.contract.exchange):
        exch_to_use = exchanges_to_use[get_exchange(exch)[0]]
        mappings_copy = list(mappings)
        for relation in mappings_copy:

            symbol = relation.symbol
            if symbology_type == SymbologyType.ADD:
                if not symbol.startswith('{}.'.format(exch_to_use)):
                    change_to = '{}.{}'.format(exch_to_use, symbol)
                    print('{} ::: Relations={} \t\t change to {}'.format(exch_to_use, relation, change_to))
                    relation.symbol = change_to
                    relation.save()
                    print('...... saved')
                    # print('Should change to ' + change_to')
                else:
                    print('--- No change for {}'.format(symbol))

            exchanges.add(exch_to_use)




                # print('Adapter: {}; len={}'.format( ta, len(list(mappings))))
    print('exchanges = {}'.format(exchanges))
check_mappings(SymbologyType.ADD)
