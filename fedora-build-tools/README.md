


Converting std::__cxx11::string to std::string
https://stackoverflow.com/a/37478234





```bash

# ./build-gcc.sh "gcc-9.1.0" "binutils-2.32" "mpfr-4.0.2" "gmp-6.1.2" "mpc-1.1.0" "isl-0.21" 0 1 >  /tmp/gcc.log 2>&1

# ./build-rpm.sh "gcc-9.1.0" "binutils-2.32" "mpfr-4.0.2" "gmp-6.1.2" "mpc-1.1.0" "isl-0.21" 0 1 >  /tmp/rpm.log 2>&1

```


says we should also install 
gmp-devel, mpfr-devel and libmpc-devel

However, interesting bit is on https://gcc.gnu.org/wiki/InstallingGCC
Maybe I need something from pre-requisites
https://gcc.gnu.org/install/prerequisites.html


