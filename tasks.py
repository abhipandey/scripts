from __future__ import print_function
import os
import sys, subprocess
import re
from invoke import task, run
from datetime import datetime
from pprint import pprint
import logging

_DATE_FMT = '%Y%m%d'

class MailGroups:
    RISKDEV = 'abhishek@tilde.sg:yuteng.xing@grasshopperasia.com:rudy.sudarman@grasshopperasia.com:nashon.loo@grasshopperasia.com:tracy.tan@grasshopperasia.com'
    BOUNCR = 'abhi@boun.cr'
    c = 'abhishek@tilde.sg'
    PERSONAL= 'abhi.pandey@gmail.com'
    TESTER = 'abhishek@tilde.sg:abhi.pandey@gmail.com'

@task
def mail(text, subject, add='abhishek@tilde.sg'):
    from base_mail import send_mail
    mails = add.split(':')
    send_mail(
        subject,
        text,
        mails,
        author='abhishek.pandey@grasshopperasia.com',
        password='Tsuguri0'
    )

@task
def todos(address='abhishek@tilde.sg', should_mail=False):
    def run_cmd(cmd, debug=False, replace_bash_texts=False):
        output = subprocess.Popen(['/bin/bash', '-i', '-c', cmd], stdout=subprocess.PIPE).communicate()[0]
        if debug:
            print('cmd={} \n\t OUTPUT={}'.format(cmd, output))
        if replace_bash_texts:
            output = re.sub(r'\[.*?K', '', output)
            output = output.replace('\n', '<br>')
        return output
    output  = run_cmd('todos', replace_bash_texts=should_mail)
    print('output = {}'.format(output))
    if should_mail:
        mail(
            '<pre style="font-weight:900; font-size: 1.3em;">' + output + '</pre>',
            "Abhishek's todos @ {}".format(datetime.now().strftime('%c')),
            address
            )

@task
def haitong_commissions(address='abhishek@tilde.sg', should_mail=False):
    from p_haitong import Account
    acc = Account()
    acc.run()


def share_todos(mailgroup=MailGroups.PERSONAL):
    a = subprocess.check_output(['/home/abhishek.pandey/code/scripts/_htmltodos.sh'])\
        .replace('<body>',
                 '<body style="font-weight:900; font-size:1.3em;">')
    mail(
        a,
        "Abhishek's todos @ {}".format(datetime.now().strftime('%c')),
        mailgroup
        # MailGroups.RISKDEV
        # MailGroups.TESTER
    )

def read_text(filename):
    if os.path.exists(filename):
        with open(filename) as f:
            return f.read()
    return ''
def write_text(filename, text):
    with open(filename, 'wt') as f:
        f.write(text)



@task
def send_todos(filename, weekday, days_since_last_run=6):
    fname = os.path.join('/home/abhishek.pandey/code/scripts/.tmp/', filename)
    logfile = os.path.join('/home/abhishek.pandey/code/scripts/.tmp/', 'log-' + filename) + '.log'
    logging.basicConfig(
        filename=logfile,
        level=logging.DEBUG,
        format="%(asctime)s - %(levelname)s - %(message)s"
    )
    now = datetime.now()
    if str(now.weekday()) != weekday:
        logging.info('We were expecting weekday input to be: {}'.format(
            now.weekday()))
        return

    text = read_text(fname).strip()
    if text:
        dt_prev_run = datetime.strptime(text, _DATE_FMT)
        logging.info('previous run = {}'.format(dt_prev_run))
        from datetime import timedelta
        dt_prev_run_plus = dt_prev_run + timedelta(days_since_last_run)
        if dt_prev_run_plus > now:
            # it is not time yet to run this
            logging.info('Returning: locals={}'.format(locals()))
            return
    logging.info('Ok to run :' + filename)

    if filename == 'todo_to_me':
        share_todos(mailgroup=MailGroups.PERSONAL)
    elif filename == 'todo_to_team':
        share_todos(mailgroup=MailGroups.RISKDEV)

    text = datetime.now().strftime(_DATE_FMT)
    write_text(fname, text)
    logging.info('text = {}'.format(text))
