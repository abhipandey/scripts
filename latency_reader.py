import re
import datetime
import sys

#  bob.settrd-ems01.grass.corp.2020-03-12.061001.14966.log.gz
# sudo date +%T -s "12:43:33" && LIBLOG_logtostderr=1 LIBLOG_logtosyslog=1 LIBLOG_log_dir=./log ./bobD --config=./bob.json HLA
# FILE=bob.settrd-ems01.grass.corp.2020-03-12.061001.14966.log.gz && GREP=ag && $GREP "7[56]=.*hb#" $FILE | $GREP Create | sed -e 's/.*Create//g'  -e 's/.*session_id.//g' -e 's/]//g'  | xargs -I {} $GREP "(Send|Recv).*{}" $FILE >> /tmp/3
# ag "7[56]=.*hb#" bobD.dev41-ws.grass.corp.2020-03-03.124333.5406.log | ag Create | sed -e 's/.*Create//g'  -e 's/.*session_id.//g' -e 's/]//g'  | xargs -I {} ag "(Send|Recv).*{}"  bobD.dev41-ws.grass.corp.2020-03-03.124333.5406.log >> /tmp/3
#  ag "Send TaxHeartbeatReq message precision_hb_seq|ecv ResponseMessage header" > /tmp/4

## Server version
# FILE=bob.current
# EGREP=egrep
# GREP=grep
# $EGREP "7[56]=.*hb#" $FILE | $EGREP Create | sed -e 's/.*Create//g'  -e 's/.*session_id.//g' -e 's/]//g'  | xargs -I {} $EGREP {} $FILE
#
# last insert rejected:
# $GREP "9:\(29\|30\).*Insert rejected" $FILE | tail -1 | sed -e 's/.*client.order.id.//g' -e 's/].*//g' | xargs -I {} $GREP "Send .eader.*{}" $FILE
#
# First accept details
# $GREP  -m1 "OrderInsertRsp.*2=OK" $FILE | sed -e 's/.*client.tx.ref..//g' -e 's/ .*//g' | xargs -I {} $GREP "client.tx.ref..{}" $FILE

# _FILE_HB = '/home/abhishek.pandey/code/docs/logs/hbs.log'
_FILE_HB = '/tmp/1'

# how to create a CSV file?#
# USAGE: grep "hb#\|Send.*msg.1=\|OrderInsertRsp" bob.current > /tmp/1


def decode_16(hash):
    return int(hash, 16)


def parse_time(hash):
    s = int(hash, 16) / 1000
    res = datetime.datetime.fromtimestamp(s).strftime('%Y-%m-%d %H:%M:%S.%f')
    return res


lines = []
_PATTERN = r'\|4=\w+-\w+-(\w+)-(\w+)'
_FILE = _FILE_HB
# _FILE = _FILE_QUERY_USER_REQ
with open(_FILE, 'rt') as file:
    lines = file.readlines()
lines = [l for l in lines if 'Recv' in l]
last_num = 0

for line in lines:
    type_str = 'Order' if 'OrderInsertRsp' in line else 'HB'
    hash_time_search = re.search(_PATTERN, line)
    if hash_time_search:
        time_str = parse_time(hash_time_search.group(1))
        num_str = decode_16(hash_time_search.group(2))
        diff_num = num_str - last_num
        last_num = num_str
        if type_str == 'HB':
            rem = line[line.find('|7=') + 3:]
            type_str = rem[:rem.find(']')]
            highlight = '  <<<-----' if diff_num > 150 else ''
        print(f'{line[4:32]} | {type_str} |  {time_str} , {num_str} | {diff_num} {highlight}')


    # print(line)

# with open('/home/abhishek.pandey/1', 'wt') as file:
#     for l in lines:
#         fmt_start = 'HB_ID'
#         # print(f'{l}')
#         if fmt_start in l:
#             start_info = re.search(_PAT_START, l, re.IGNORECASE)
#             if start_info:
#                 items = list(start_info.groups())
#                 print(f' FOUnd : {items}')
#                 file.write('{},{},{},{},{}'.format(items[0], items[1],
#                                                    items[2], items[3],
#                                                    items[4]))
#                 file.write('\n')
