#!/opt/py27env/bin/python2.7
import datetime

# This comes from gitlab/snippets/156
from base_pygh import *

from pyghweb.ghw.models import *
from pyghweb.eve.models import *
import core_pb2
import emscentral_pb2
from django.conf import settings
from django.db.models import Avg, Max, Min, Count
from pyghweb.ghw.messaging.solace import requests, topics
from gh import solace

from pyghweb.refdata.etf.models import *

# objs = EtfEodValue.objects.filter(etf__code__in=[1357,1570,1571])\
#             .order_by('-id')\
#             .distinct('etf_id')\
#             [:50]
#
# print('result = {}'.format('\n\n'.join([str(vars(x)) for x in objs])))
# print('_'*80)
# print()

objs = EtfEodValue.objects.raw('''SELECT *
FROM etf_etfeodvalue
WHERE id IN
    (SELECT id
     FROM
       (SELECT *
        FROM etf_etfeodvalue
        WHERE etf_id IN (SELECT id FROM `etf_etf`)
        ORDER BY id DESC LIMIT 0, 300) AS all_rows
     GROUP BY all_rows.etf_id
    )'''
)
cash_vals = {}
for o in objs:
    cash_vals[o.etf.code.encode('utf-8')] = o.cash

print(pp(cash_vals))
# for o in objs:
#     print('\n\n{:-<40} >> {}'.format(o.etf.id, vars(o)))
# print('result = {}'.format(s))
