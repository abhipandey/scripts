import asyncio
from contextlib import closing

import httplib2
from bs4 import BeautifulSoup, SoupStrainer

import sys
import os
import urllib
import threading
from queue import Queue
import urllib.request

import requests

URL = 'http://tse.factsetdigitalsolutions.com/iopv/table?language=en'
DIR_TO_DOWNLOAD = '/tmp/p'


def getLinks(url):
    http = httplib2.Http()
    status, response = http.request(URL)

    csv_files = []
    for link in BeautifulSoup(response,
                              parse_only=SoupStrainer('a'),
                              features="html.parser"):
        if link.has_attr('href'):
            href = link['href']
            if href.endswith('csv'):
                csv_files.append(href)
                print(link['href'])
    return csv_files


class DownloadThread(threading.Thread):
    def __init__(self, queue, destfolder):
        super(DownloadThread, self).__init__()
        self.queue = queue
        self.destfolder = destfolder
        self.daemon = True

    def run(self):
        while True:
            url = self.queue.get()
            try:
                self.download_url(url)
            except Exception as e:
                print(f'Exception: {str(e)}')
            self.queue.task_done()

    def download_url(self, url):
        # change it to a different way if you require
        name = url.split('/')[-1]
        dest = os.path.join(self.destfolder, name)
        print("[%s] Downloading %s -> %s" % (self.ident, url, dest))
        urllib.request.urlretrieve(url, dest)


def download(urls, destfolder, numthreads=4):
    queue = Queue()
    for url in urls:
        queue.put(url)

    for i in range(numthreads):
        t = DownloadThread(queue, destfolder)
        t.start()

    queue.join()


def main():
    urls = getLinks(URL)
    download(urls, DIR_TO_DOWNLOAD)


if __name__ == '__main__':
    main()