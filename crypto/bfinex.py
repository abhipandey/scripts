#!/home/abhishek.pandey/code/scripts/ve/bin/python
import asyncio

import logging
import time
import sys

from btfxwss import BtfxWss

log = logging.getLogger(__name__)

fh = logging.FileHandler('test.log')
fh.setLevel(logging.DEBUG)
sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.DEBUG)

log.addHandler(sh)
log.addHandler(fh)
logging.basicConfig(level=logging.DEBUG, handlers=[fh, sh])

wss = BtfxWss()
wss.start()

while not wss.conn.connected.is_set():
    time.sleep(1)

TICKER = 'BTCUSD'

# Subscribe to some channels
wss.subscribe_to_ticker(TICKER)
wss.subscribe_to_trades(TICKER)
wss.subscribe_to_order_book(TICKER)

# Do something else
t = time.time()
while time.time() - t < 10:
    pass

print('-------- tickers')

# Accessing data stored in BtfxWss:
ticker_q = wss.tickers(TICKER)  # returns a Queue object for the pair.
while not ticker_q.empty():
    print(TICKER + str(ticker_q.get()))

print('-------- trades')
trades_q = wss.trades(TICKER)
while not trades_q.empty():
    print(trades_q.get())
print('-------- trades')

# Do something else
t = time.time()
while time.time() - t < 20:
    pass

trades_q = wss.trades(TICKER)
while not trades_q.empty():
    print(trades_q.get())

# Unsubscribing from channels:
wss.unsubscribe_from_ticker(TICKER)
wss.unsubscribe_from_trades(TICKER)
wss.unsubscribe_from_order_book(TICKER)

# Shutting down the client:
wss.stop()
