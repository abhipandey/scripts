from notion.client import NotionClient

_TOKEN = '7788686c2be854090859318cfbf8d1579852e1b21a82328eca836d6f1276cbc67b787d950b5be52b5dade059695a87feb4d5228dd33886b97630611437ec3812a23fc4560ede1bc074a259345ca4'
# Obtain the `token_v2` value by inspecting your browser cookies on a logged-in (non-guest) session on Notion.so
client = NotionClient(token_v2=_TOKEN)

# Replace this URL with the URL of the page you want to edit
page = client.get_block(
    'https://www.notion.so/abhi1010/Example-sub-page-4988fe5e3e1f451d9bb6d66adbb96c05'
)

print("The old title is:", page.title)

# Note: You can use Markdown! We convert on-the-fly to Notion's internal formatted text data structure.
page.title = "The title has now changed, and has *live-updated* in the browser!"
