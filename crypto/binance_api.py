import ccxt


class Crypto:
    def __init__(self):
        self.exchange_id = 'binance'
        exchange_class = getattr(ccxt, self.exchange_id)
        self.exchange = exchange_class({
            'apiKey':
            'CwFaS1WYtyG6Qj2jqHafOmGhN5bf5aeEfZtDGXaoeaSiEk90iMzPrN58uTorD0jU',
            'secret':
            'TlDpPPpxwwJyI1MY4RDivEJ4bVuRm8G8DoI4if99eHdC103Evr6Prok4sVDlFleZ',
            'timeout': 30000,
            'enableRateLimit':
            True,  # required! https://github.com/ccxt/ccxt/wiki/Manual#rate-limit
        })

    def get_trades(self, symbol: str):
        trades = self.exchange.fetch_my_trades(symbol)
        return trades

    def exchanges(self):
        return ccxt.exchanges

    def get_my_orders(self, symbol: str):
        orders = self.exchange.fetch_orders(symbol)
        return orders

    def get_my_closed_orders(self, symbol: str):
        orders = self.exchange.fetch_closed_orders(symbol)
        return orders

    def get_available_markets(self):
        return self.exchange.load_markets()

    def get_available_symbols(self):
        return self.exchange.symbols

    def get_symbol_info(self, symbol: str):
        return self.exchange.fetch_ticker(symbol)

