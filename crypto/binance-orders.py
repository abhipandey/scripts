import pickle

import pandas as pd
import ccxt

import sheet_updater

_TRADES_FILE = 'trades.binance.pickle'
_ORDERS_FILE = 'orders.binance.pickle'
_SYMBOL = 'LTC/USDT'

from binance_api import Crypto


def get_unique_pairs(symbols, extension):
    return [s.split('/')[0] for s in symbols if s.endswith(extension)]

def save_items(items, filename):
    # filename = _TRADES_FILE

    with open(filename, 'wb') as out_s:
        pickle.dump(items, out_s)


def load_items(filename):
    # filename = _TRADES_FILE
    with open(filename, 'rb') as out_s:
        return pickle.load(out_s)


def analyze_pairs(crypto: Crypto):
    mkts = crypto.get_available_markets()
    # print(f'mkts = {mkts}')
    symbols = crypto.get_available_symbols()
    print(f'symbols = {symbols}')
    if not symbols:
        return
    busds = get_unique_pairs(symbols, '/BUSD')
    usdts = get_unique_pairs(symbols, '/USDT')

    missing_busd = set(usdts).difference(set(busds))
    missing_usdt = set(busds).difference(set(usdts))
    print(f'Symbols ONLY in USDT or BUSD = {missing_usdt + missing_busd}')

def main():
    crypto = Crypto()

    analyze_pairs(crypto)
    return


    ask_exchange = 0

    if ask_exchange:
        trades = crypto.get_trades(_SYMBOL)
        save_items(trades, _TRADES_FILE)
    else:
        trades = load_items(_TRADES_FILE)

    # for t in trades:
    #     print(f'trades = {t}')

    print(f'len  of trades = {len(trades)}')

    if ask_exchange:
        orders = crypto.get_my_closed_orders(_SYMBOL)
        save_items(orders, _ORDERS_FILE)
    else:
        orders = load_items(_ORDERS_FILE)

    for o in orders:
        print(f'Orders = {o}')
    print(f'len of orders = {len(orders)}')

    sheet = sheet_updater.SheetUpdater()
    sheet.write_data(orders)


if __name__ == '__main__':
    main()
