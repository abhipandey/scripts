import gspread

_URL = 'Crypto Updates'

sheet_key = 'crypto/crypto-sheets-proj-prom-1-igneous-bond-179003-5d4462174f27.json'


class SheetUpdater:
    def __init__(self):
        # self.sht1 = gc.open_by_key(
        #     '0BmgG6nO_6dprdS1MN3d3MkdPa142WFRrdnRRUWl1UFE')
        gc = gspread.service_account(filename=sheet_key)
        # gc = gspread.service_account()

        # titles_list = []
        # for spreadsheet in gc.openall():
        #     titles_list.append(spreadsheet.title)
        # print(f'titles = {titles_list}')
        self.sheet = gc.open(_URL)

        print(self.sheet.sheet1.get('A1'))

    def write_data(self, orders):
        if not len(orders):
            return
        # self.sheet.values_clear("Sheet1!A2:Z10000")
        first_order = orders[0]
        keys = sorted(
            list(first_order.keys()) +
            [f'info-{x}' for x in list(first_order['info'].keys())])
        keys.remove('info')
        worksheet = self.sheet.sheet1

        update_worksheet = lambda row_range, values: worksheet.batch_update([{
            'range':
            row_range,
            'values': [values],
        }])

        append_worksheet = lambda row_range, values: worksheet.append_row(
            values=values, table_range=row_range)

        update_worksheet('A1', keys)  # write header

        for index, order in enumerate(orders):
            vals = []
            for k in keys:
                if k in order:
                    vals.append(order[k])
                else:  # this is part of 'info-{something}'
                    orig_key = k.replace('info-', '')
                    vals.append(order['info'][orig_key])
            print(f'trying to write {vals}')
            cleaned_vals = [x if x else '' for x in vals]
            # update_worksheet(f'A{index+2}', cleaned_vals)
            append_worksheet(f'A2', cleaned_vals)
            # append_worksheet(f'A{index+2}', cleaned_vals)

        # for order in orders:
        #     print(f'order = {order}')

        print(f'keys = {keys}')


def main():
    gsheet = SheetUpdater()


if __name__ == '__main__':
    main()