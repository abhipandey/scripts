from google.protobuf import json_format
import os
os.system(
    '/home/abhishek.pandey/apps/code/githubs/docks/binary-centos-7.1/protoc-3.0.0-b3.2/bin/protoc --cpp_out=. --python_out=. ccgh.proto'
)
import google

from ccgh_pb2 import *
from google.protobuf.wrappers_pb2 import *

c1 = CfgEmsClient.CfgEmsCentral()
o1 = CfgEmsClient(EmsCentral=c1)
o2_0 = CfgCorePhase.CfgFilter()
o2 = CfgCorePhase(Filter=o2_0)
o3_1 = BoolValue(value=True)
o3_0 = CfgTaskScheduler.CfgScheduler(Affinity="2", Yield=o3_1)
o3 = CfgTaskScheduler(Scheduler=o3_0)


def show_me(obj, debug=""):
    def get_str():
        return json_format.MessageToJson(
            obj,
            including_default_value_fields=True,
            preserving_proto_field_name=True)

    json_s = get_str()
    print(debug + json_s)
    print('_' * 80)
    return json_s


show_me(o1, 'EmsCentral:---> ')
show_me(o2, 'CfgCorePhase:---> ')
json_str = show_me(o3, 'CfgTaskScheduler:---> ')

msg = json_format.Parse(json_str, CfgTaskScheduler())
print('msg = {}'.format(msg))
