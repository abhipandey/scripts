

There are 2 projects here so far.


1. `ex.py` - sample code on how to get python code working with `protobuf` `ccgh.proto` file
2. `protobuf-doc-maker.py` - iterates through a folder, to create SVG diagrams

`protobuf-doc-maker.py` has been installed on `csqprod-web09` at `~/higgs/cicd/`.
It utilizes `update-protobuf-doc.sh` script in the `~/higgs/cicd` folder.