abhishek.pandey > cat update-protobuf-docs.sh 
#!/usr/bin/env bash

cd ~/higgs/
echo -e "In dir " $PWD
git pull
echo -e "Pulled latest code"

source /tmp/ve/bin/activate

echo -e "About to run pb maker"
./cicd/pb-doc-maker.py

rm -rf /usr/local/http/protobuf/*
echo -e "Cleared protobuf folder"

cp /tmp/html/* /usr/local/http/protobuf/
echo -e "Run COMPLETE"
