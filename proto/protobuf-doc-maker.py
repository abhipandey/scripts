#!/usr/bin/python3

# Currently installed on csqprod-web09

import subprocess
import os
import re
import glob
import sys
from collections import defaultdict

import markdown
''' You MIGHT have to change the following values below '''
_PLANTUML = 'plantuml'
_PROTOC = 'protoc'

_WD = os.getcwd()
print(f'Working Directory = {_WD}')

_STR_PB = 'protobuf'
_STR_XRAID_PROTO = 'ccgh/aqua/src/gng/xraider/protobuf'
_DIR_PROTO = os.path.join(_WD, _STR_PB)
_DIR_AQUA_SRC = os.path.join(_WD, 'ccgh/aqua/src')
_DIR_XRAID_PROTO = os.path.join(_WD, _STR_XRAID_PROTO)

_DIR_FOR_XTRA = os.path.join(_WD, './jagh/box/tools/protobuf/include/')
_DIR_FOR_PUML_FULL = '/tmp/docs1'
_DIR_FOR_PUML_FILTERED = '/tmp/docs2'
_DIR_HTML = '/tmp/html'


def get_proto_files_list(dir):
    files = glob.glob(os.path.join(dir, '*.proto'))
    cond = ''
    filtered = [x for x in files if cond in x]
    print(f'dir={dir}; files = {filtered}')
    return filtered


def run_cmd(cmd, debug=True, keep_newlines=False):
    output = subprocess.check_output(
        cmd, shell=True, stderr=subprocess.STDOUT).decode('UTF-8')
    if not keep_newlines:
        output = output.replace('\n', '')
    if debug:
        print(f'cmd = {cmd}. output={output}')
    return output


def get_lines(file):

    lines = []
    with open(file, 'rt') as f:
        lines = f.readlines()
    return lines


def parse_and_imagify_full_puml(file):
    def ignore_some_mappings(lines):
        '''
        We are trying to ignore anything with :: in the mappings
        For example, Class A -- Class A::B  (private,/nested)
        For example, Class A -- Class A::ResponseCode (No interest in RespCode enum)
        '''

        filtered_lines = []
        for line in lines:
            if not ('--' in line and '::' in line):
                filtered_lines.append(line)
        return filtered_lines

    def get_sorted_packages(lines):
        '''
        The deepest nested package names should appear first.
        This is because when we replace them with empty strings (A.B.C.D.E), 
        we ensure that we are not removing a potential candidate (like A.B.C).
        This ensures that we do not leave behind classes like D.E.<class_name> : 
        which would happen if we started replacing with A.B.C first.
        '''

        packages = [x for x in lines if x.startswith('package ')]
        counts = defaultdict(int)
        for package_line in packages:
            p1 = package_line.replace('package ', '').replace('{', '').strip()
            counts[p1] = sum([(1 if p1 in p else 0) for p in packages]) - 1
        # print(f'counts = {counts}')

        colls = defaultdict(list)
        for c in counts:
            num = counts[c]
            colls[num].append(c)

        # print(f'colls = {colls}')
        sorted_packages = []
        for idx in colls:
            for c in colls[idx]:
                sorted_packages.append(c)

        # print(f'sorted_packages = {sorted_packages}')
        return sorted_packages

    def get_mapped_classes(lines, packages):
        mapped_classes = set()
        append = lambda x: mapped_classes.add(f'class {x} {{')
        to_parse = [l for l in lines if ' -- ' in l]
        for line in to_parse:
            mod_line = line
            for p in packages:
                mod_line = mod_line.replace(p, '')
            # print(f'final mod line= {mod_line}')
            classes = re.search(r'.(?P<CLASS1>.*) -- .(?P<CLASS2>.*)',
                                mod_line).groupdict()
            append(classes['CLASS1'])
            append(classes['CLASS2'])

        # print(f'mapped_classes = {mapped_classes}')
        return mapped_classes

    def find_classes_that_we_should_ignore(lines: list, mapped_classes: set):
        start_skips = False
        final_lines = []
        for l1 in lines:
            l = l1.strip()
            if l.startswith('class') and l not in mapped_classes:
                start_skips = True
            if '::ReturnCode' in l:
                start_skips = True
            if not start_skips:
                final_lines.append(l1)
            if start_skips and l.endswith('}'):
                start_skips = False
        return final_lines

    lines = get_lines(os.path.join(_DIR_FOR_PUML_FULL, file))
    lines = ignore_some_mappings(lines)
    packages = get_sorted_packages(lines)
    classes = get_mapped_classes(lines, packages)
    filtered_lines = find_classes_that_we_should_ignore(lines, classes)

    filtered_file = os.path.join(_DIR_FOR_PUML_FILTERED, file)
    with open(filtered_file, 'wt') as f:
        for line in filtered_lines:
            if not ('--' in line and '::' in line):
                f.writelines([line])
            # else:
            # print(f'Not including .... {line}')
    print(f'new file {file} complete')

    # convert puml to an image
    print(f'About to create image for {file}')
    cmd = f'{_PLANTUML} {filtered_file}'
    run_cmd(cmd)

    # also move the created png file into html folder
    png_file = filtered_file.replace('.puml', '.png')
    cmd = f'mv {png_file} ../html/'
    run_cmd(cmd)


def create_html_file(file):
    '''
    Uses the following project: https://github.com/pseudomuto/protoc-gen-doc
    However, it does not create png files for diagrams. They are nice to have.
    Hence, we will hack it a bit to add into the html files.
    '''
    just_file = os.path.splitext(os.path.basename(file))[0]
    cmd = f'{_PROTOC} --doc_out={_DIR_HTML} --doc_opt=html,{just_file}.html -I {_DIR_FOR_XTRA} -I {_DIR_XRAID_PROTO} -I {_DIR_PROTO} {just_file}.proto'
    run_cmd(cmd)

    def insert_img_in_html():
        html_file = os.path.join(_DIR_HTML, f'{just_file}.html')
        lookup = 'href="#title"'

        def get_line():
            with open(html_file) as myFile:
                for num, line in enumerate(myFile, 1):
                    if lookup in line:
                        # print('found at line:{}'.format(num))
                        return num + 2
            return 0

        line_num = get_line()
        if line_num:
            # print(f'line_num = {line_num}')
            fmt = f'<a href="{just_file}.png" target="_blank"><img src="{just_file}.png" width="100%" /></a>'
            cmd = f"sed -i '{line_num}i{fmt}' {html_file}"
            run_cmd(cmd)

    insert_img_in_html()


def create_png_image(file):
    def rename_complete_model_to_filename(file):
        os.chdir(_DIR_FOR_PUML_FULL)
        just_file = os.path.splitext(os.path.basename(file))[0]
        new_file = f'{just_file}.puml'
        cmd = f'mv complete_model.puml {new_file}'
        run_cmd(cmd)
        return new_file

    def convert_to_puml(file):
        cmd = f'Received {file}'
        print(cmd)
        cmd = f'{_PROTOC} --uml_out={_DIR_FOR_PUML_FULL} -I {_DIR_FOR_XTRA} -I {_DIR_XRAID_PROTO} -I {_DIR_PROTO} {file}'
        run_cmd(cmd)

    convert_to_puml(file)
    new_file = rename_complete_model_to_filename(file)
    parse_and_imagify_full_puml(new_file)


def create_index_html():
    index_file = os.path.join(_DIR_HTML, 'index.html')

    def get_readme_text():
        main_text = ['# List of Protobuf files']

        def get_ccgh_protobuf_md():
            text = ['']
            static_s = _STR_PB
            folder = _DIR_PROTO
            text += ['', '']
            header = '## ' + static_s
            text.append(header)

            files = get_proto_files_list(folder)
            files_with_simple_names = []
            for file in files:
                just_file = os.path.splitext(os.path.basename(file))[0]
                files_with_simple_names.append([file, just_file])
            for file, just_file in sorted(files_with_simple_names, key=lambda x: x[0]):
                just_file = os.path.splitext(os.path.basename(file))[0]
                li = f'* [{just_file}]({just_file}.html)'
                text.append(li)
            return text
        
        def get_aqua_pb_md():
            text = ['']*3
            static_s = _STR_XRAID_PROTO
            folder = _DIR_XRAID_PROTO

            text.append('## ' + static_s) ## h2 header

            text.append('| Protobuf File | Used in following files |')
            text.append('| -- | -- |')
            # for file in files:
            #     just_file = os.path.splitext(os.path.basename(file))[0]
            #     li = f'* [{just_file}.proto]({just_file}.html)'
            #     text.append(li)
            

            files = get_proto_files_list(_DIR_XRAID_PROTO)
            files_with_simple_names = []
            for file in files:
                just_file = os.path.splitext(os.path.basename(file))[0]
                files_with_simple_names.append([file, just_file])

            for file, just_file in sorted(files_with_simple_names, key=lambda x: x[0]):
                exp_pb_h = f'{just_file}.pb.h'
                cmd_to_find_files_containing_pb_h = f'egrep  -r "{just_file}.pb.h" {_DIR_AQUA_SRC}/*  || echo ""'
                results = run_cmd(cmd_to_find_files_containing_pb_h, debug=False,keep_newlines=True)
                loc_pattern = r'.*ccgh.aqua.(.*):#include.*'
                res_lines =  [r for r in results.split('\n') if r]
                print(f'file = {exp_pb_h}')
                
                references = ['']
                file_key = f'[{just_file}]({just_file}.html)'

                for l in res_lines:
                    search_res = re.search(loc_pattern, l, re.IGNORECASE)
                    if not search_res:
                        print(f' not found for {l}')
                        continue
                    file_found = search_res.group(1)
                    if 'factory' not in file_found and  'Factory' not in file_found:
                        just_file_name_simple = just_file_name(file_found, include_ext=True)
                        print(f'  l={just_file_name_simple}')
                        references.append(f'`higgs`: [{just_file_name_simple}](http://gitlab.grass.corp/p/higgs/tree/master/ccgh/aqua/{file_found})')
                ref_s = ' <br />'.join(sorted(references))
                text.append(f'| {file_key} | {ref_s} |')



            return text
        
        main_text += get_ccgh_protobuf_md()
        main_text += get_aqua_pb_md()
                
        return '\n'.join(main_text)

    def convert_readme_to_html():
        readme_text = get_readme_text()
        html_body = markdown.markdown(readme_text, extensions=['markdown.extensions.extra', 'markdown.extensions.tables'])

        # wrap in bootstrap css and js
        
        with open(index_file, 'wt') as opened_file:
            full_html = f'''

<html lang="en">
  <head>
  
  

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table.min.css">

    
  </head>
  <body>


            {html_body}


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table.min.js"></script>

  </body>
</html>

            '''
            full_html = full_html.replace('<table>', '<table data-toggle="table">')
            opened_file.write(full_html)
            print(f'Updated {index_file}')

    convert_readme_to_html()


def just_file_name(full_path, include_ext=False):
    if include_ext:
        return os.path.basename(full_path)
    return os.path.splitext(os.path.basename(full_path))[0]

def main():
    run_cmd('mkdir -p /tmp/{docs1,docs2,html}')
    #files = get_proto_files_list(_DIR_PROTO) + get_proto_files_list(
    #     _DIR_XRAID_PROTO)
    # files = get_proto_files_list(_DIR_XRAID_PROTO)
    # print(f'xraid files={files}')
    # files = get_proto_files_list(_DIR_PROTO)
    # files = get_proto_files_list(_DIR_XRAID_PROTO)

    # for file in files:
    #     print('-' * 80 + file)
        # create_png_image(file)
        # create_html_file(file)
    # files = get_proto_files_list(_DIR_XRAID_PROTO)
    # for file in files:
    #     just_file = just_file_name(file)
    #     exp_pb_h = f'{just_file}.pb.h'
    #     cmd_to_find_files_containing_pb_h = f'egrep  -r "{just_file}.pb.h" {_DIR_AQUA_SRC}/*  || echo ""'
    #     results = run_cmd(cmd_to_find_files_containing_pb_h, debug=False,keep_newlines=True)
    #     loc_pattern = r'.*ccgh.aqua.(.*):#include.*'
    #     res_lines =  [r for r in results.split('\n') if r]
    #     print(f'file = {exp_pb_h}')
    #     for l in res_lines:
    #         search_res = re.search(loc_pattern, l, re.IGNORECASE)
    #         if not search_res:
    #             print(f' not found for {l}')
    #             continue
    #         file_found = search_res.group(1).lower()
    #         if 'factory' not in file_found:
    #             just_file_name_simple = just_file_name(file_found, include_ext=True)
    #             print(f'  l={just_file_name_simple}')

        
        # print(f'   results = {results}')
    create_index_html()


main()
