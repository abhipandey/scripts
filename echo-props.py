#!/usr/bin/env python
import re
import sys

# if len(sys.argv) == 1:
#     print('Too less args. Try putting in a string.')
#     sys.exit(0)

regex = r"(\w+) {|}"
regex_param = r"parameter { type: (\w+) name: .([\w-]+).(?: val_(\w+)?: (.*))?"

# test_str = '''end BdcAlgoParameterChanged[algo { algo_id { factory_name: "XTremis" algo_name: "XTremis_child" engine_id: "JPX/jpxtrd-ems19/hummer02" login: "Kyle_Goh" } parameter { type: PAR_DEC64 name: "Active-Ref-Px" } parameter { type: PAR_DEC64 name: "Canary-Ref-Px" } parameter { type: PAR_STRING name: "IsQuoting" } parameter { type: PAR_DEC64 name: "NetPos" } parameter { type: PAR_INT64 name: "NumTrades" } parameter { type: PAR_DEC64 name: "OptionDelta" } parameter { type: PAR_DEC64 name: "OptionGamma" } parameter { type: PAR_INT64 name: "Pnl" } parameter { type: PAR_DEC64 name: "SprBuy" } parameter { type: PAR_DEC64 name: "SprSell" } parameter { type: PAR_STRING name: "StratState" } parameter { type: PAR_STRING name: "cfgTag" } parameter { type: PAR_STRING name: "display-name-add-on" val_string: "" } parameter { type: PAR_STRING name: "hRule.aeMinLeanVol" } parameter { type: PAR_STRING name: "hRule.maxQueueVol" } parameter { type: PAR_STRING name: "hRule.minCoverQty" } parameter { type: PAR_STRING name: "hRule.payUpDelayMs" } parameter { type: PAR_STRING name: "hRule.peMinLeanVol" } parameter { type: PAR_STRING name: "hRule.rodeoModeDurationMs" } parameter { type: PAR_STRING name: "hRule.rodeoModePxOffset" } parameter { type: PAR_STRING name: "hedger.skewRatioThres" } parameter { type: PAR_STRING name: "hedger.skewVolMin" } parameter { type: PAR_STRING name: "hedger.tradeSumMin" } parameter { type: PAR_STRING name: "hedger.tradeSumPctThres" } parameter { type: PAR_STRING name: "hedger.triggerVolMax" } parameter { type: PAR_STRING name: "hedger.triggerVolMin" } parameter { type: PAR_STRING name: "hedger.triggerVolPct" } parameter { type: PAR_STRING name: "hedger.watchPxOffset" } parameter { type: PAR_OBI name: "instOrderbook" } parameter { type: PAR_INT64 name: "last-trade-age" val_int64: 0 } parameter { type: PAR_INT64 name: "max-fills-count1" val_int64: 2000 } parameter { type: PAR_INT64 name: "max-fills-count2" val_int64: 4000 } parameter { type: PAR_INT64 name: "max-fills-time1" val_int64: 1000 } parameter { type: PAR_INT64 name: "max-fills-time2" val_int64: 5000 } parameter { type: PAR_DEC64 name: "max-fills-warning-ratio" val_dec64: "0.8" } parameter { type: PAR_INT64 name: "max-order-size" val_int64: 1000 } parameter { type: PAR_STRING name: "maxOptPos" } parameter { type: PAR_STRING name: "maxPos" } parameter { type: PAR_INT64 name: "order-count-count1" val_int64: 20 } parameter { type: PAR_INT64 name: "order-count-count2" val_int64: 90 } parameter { type: PAR_INT64 name: "order-count-time1" val_int64: 1000 } parameter { type: PAR_INT64 name: "order-count-time2" val_int64: 5000 } parameter { type: PAR_DEC64 name: "order-count-warning-ratio" val_dec64: "0.8" } parameter { type: PAR_STRING name: "quoter.askDepthOffset" } parameter { type: PAR_STRING name: "quoter.askMaxDepthOffset" } parameter { type: PAR_STRING name: "quoter.askOffset" } parameter { type: PAR_STRING name: "quoter.askQtyDelta" } parameter { type: PAR_STRING name: "quoter.bidOffset" } parameter { type: PAR_STRING name: "quoter.bidQtyDelta" } parameter { type: PAR_STRING name: "quoter.delEdge" } parameter { type: PAR_STRING name: "quoter.maxDepth" } parameter { type: PAR_STRING name: "quoter.minQty" } parameter { type: PAR_STRING name: "quoter.posEdgeC" } parameter { type: PAR_STRING name: "quoter.posEdgeM" } parameter { type: PAR_STRING name: "quoter.posMaxEdge" } parameter { type: PAR_STRING name: "quoter.qDepth" } parameter { type: PAR_STRING name: "quoter.qEdge" } parameter { type: PAR_STRING name: "quoter.queueEdgeM" } parameter { type: PAR_STRING name: "quoter.queueMaxEdge" } parameter { type: PAR_STRING name: "quoter.randQty" } parameter { type: PAR_STRING name: "refTicker.microprice.minLeanVol" } parameter { type: PAR_STRING name: "refTicker.microprice.weight" } parameter { type: PAR_STRING name: "refTicker.nanoprice.blackoutPeriodMs" } parameter { type: PAR_STRING name: "refTicker.nanoprice.efpLowerBound" } parameter { type: PAR_STRING name: "refTicker.nanoprice.efpUpperBound" } parameter { type: PAR_STRING name: "refTicker.nifty.aggMktDepthLvls" } parameter { type: PAR_STRING name: "refTicker.nifty.minLeanVol" } parameter { type: PAR_STRING name: "refTicker.nifty.trade.minPct" } parameter { type: PAR_STRING name: "refTicker.nifty.trade.minQty" } parameter { type: PAR_STRING name: "refTicker.nifty.transitOffset" } parameter { type: PAR_STRING name: "refTicker.simple.minLeanVol" } parameter { type: PAR_DEC64 name: "relative-limit" val_dec64: "0" } parameter { type: PAR_INT64 name: "state" val_int64: 0 } }] topic[hummer/v0/pb/pub/JPX/jpxtrd-ems19/hummer02/Kyle_Goh/XTremis_child/algo_param_changed] id[toseprod-sol02/production] aid[Kyle_Goh:XTremis_child]'''
# test_str = sys.argv[1]

for line in sys.stdin:
    test_str = line.strip()
    # print(line.strip())

    matches = re.finditer(regex, test_str, re.MULTILINE | re.IGNORECASE)

    last_start = 0
    last_end = -1
    start = 0
    end = -1
    counter = 0
    did_we_print_snip_last = False

    for index, match in enumerate(matches):

        # print(match)
        start = match.start()
        end = match.end()
        if match.group(1) is not None:
            counter += 1
            if index == 0:
                print(test_str[:start])
            indent = '  ' * counter

            # print(indent, test_str[last_start:start])
            # print(test_str[last_start:start])

            last_start = start
            last_end = end

            # print('   start = {}; end ={}; len={}'.format(start, end, len(match.groups())))
            # print("   Match was found at {start}-{end}: {match}".format(start = match.start(), end = match.end(), match = match.group()))

        else:
            # print('{}:{} ... >> {} {}'.format(last_start, end, test_str[last_start:end], '}'))
            # print('{} {}'.format(test_str[last_start:end]))
            shortened = test_str[last_start:end]
            results = re.search(regex_param, shortened)
            if results:
                if results.group(3):
                    print(indent + shortened)
                    did_we_print_snip_last = False
                else:
                    if not did_we_print_snip_last:
                        print(indent + '<snip...>')
                        did_we_print_snip_last = True
            else:
                did_we_print_snip_last = False
                print(indent + shortened)
            last_start = start
            # print('start ={}; end={}; last_start={}; last_end={}'.format(start, end, last_start, last_end))
            # print('Close }')
            counter -= 1
        # print(indent, test_str[last_start:start])

        # for groupNum in range(0, len(match.groups())):
        #     groupNum = groupNum + 1

        #     print (" Group {groupNum} found at {start}-{end}: {group}".format(groupNum = groupNum, start = match.start(groupNum), end = match.end(groupNum), group = match.group(groupNum)))

        # print(test_str[start:end])
        # print(test_str[last_end:])
