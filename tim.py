#!/home/abhishek.pandey/code/scripts/ve/bin/python


from bs4 import BeautifulSoup
from urllib import request
import re
import requests
import json
import sys
sys.setrecursionlimit(10000)
import pprint

from inlinestyler.utils import inline_css
from base_mail import send_mail
FIND_TEXT = ['']

def get_pretty_print_string(item):
    pp = pprint.PrettyPrinter(indent=4)
    return pp.pformat(item)
pp = lambda x: get_pretty_print_string(x)

_REGEX_ASIN = re.compile(r'ASIN\/(.*?)\/')

def read_tim_blog(u):
    res = {}
    u  =  'https://tim.blog/2018/03/05/jack-kornfield/'
    r = request.urlopen(u).read()
    soup = BeautifulSoup(r, 'html.parser')
    print('title=', soup.title.string)
    res['TITLE'] = soup.title.string


    data = []
    #  title = soup.find("meta",  property="og:title")

    # `desc= soup3.find(attrs={'name':'description'})
    # soup.find_all(attrs={"name": re.compile(r'Description', re.I)})
    # for x in soup.find_all(attrs={"property": re.compile(r'.*', re.I)}): print('x = ', x)
    metas = soup.find_all('meta', attrs={"property": re.compile(r'.*', re.I)})
    for m in metas:
        # print('m = {}'.format(m))
        key = m['property'].strip('og:').replace('article:', '').upper()
        # key  = m['property']
        res[key] = m['content']
        print(key, ": ", m['content'])

    books = []
    res['BOOKS'] = books
    for l in soup.find_all('a', attrs={'target': '_blank' , 
                                        'rel' :'noopener',
                                        'href': re.compile(r'www.amazon.com.*ASIN')})[:1]:
        print('l = {}: {}'.format(l['href'], l.string))
        books.append(get_ahref_book_info(l['href'], l.string))


    print(pp(res))
    # for li in soup.findAll('li', class_='item'):
    #     classes = li.get('class', '')

    #     img = li.find_all('img')[0]['data-original'][2:]
    #     old_price = li.find('p', 'old-price').get_text()
    #     special_price = li.find('p', 'special-price').get_text().strip()
    #     name = li.find('h2', 'product-name').get_text()
        # tag_online_special = len(li.find_all('span', 'onlineexcl'))

    return data

def get_ahref_book_info(href, name):
    book_info = {'HREF': href, 'NAME': name}
    book_info['ASIN'] = re.search(_REGEX_ASIN, href).groups()[0]
    read_amazon_book_link(href, name, book_info)
    return book_info

def read_amazon_book_link(href, name, book_info):
    print('href = {}; type={}'.format(href, type(href)))
    
    # r = request.urlopen(href).read()
    # soup = BeautifulSoup(r, 'html.parser')


    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'}
    r = requests.get(href, headers=headers)
    soup = BeautifulSoup(r.content, "lxml")

    # author
    author_name = soup.find('a', 'a-link-normal contributorNameID')
    print('author = {}'.format(author_name.string))
    book_info['AUTHOR'] = author_name.string

    # image src
    img = soup.find('img', attrs={'id': 'imgBlkFront'})
    img_src = list(json.loads(img['data-a-dynamic-image']).keys())[0]
    print('img = {}'.format(img_src))
    book_info['IMG'] = img_src

    # stars
    # span id="acrPopover" class="reviewCountTextLinkedHistogram noUnderline" 
    stars = soup.find('span', attrs={
            'id': 'acrPopover'
        })['title'].replace(' out of 5 stars', '')
    book_info['STARS'] = stars


    print('title = ', soup.title.string)

data = read_tim_blog('https://tim.blog/2018/03/05/jack-kornfield/')


# send_mail('Uniqlo Sale',
#           inline_css(html_text),
#           ['abhishek@tilde.sg'])
