#!/usr/bin/bash


# Once jenkins build is complete, you can copy over the tar file from csqmgmt-prov02 
# server to the csqdev-app01 server by using the following command:
# 
# scp dev@csqmgmt-prov02:/mnt/repo_mirror/higgs/higgs.riskweb.CentOS.7.riskweb-2016.10.006.tar.xz $(whoami)@csqdev-app01:/tmp/

# call this script as follows
# deploy_from_app01.sh higgs.riskweb.CentOS.7.riskweb-2016.10.006.tar.xz
#    which is expected to be in /tmp/ folder



# export TAR=higgs.riskweb.CentOS.7.riskweb-2016.10.006.tar.xz
export TAR=$1
scp dev@csqmgmt-prov02:/mnt/repo_mirror/higgs/$TAR $(whoami)@csqdev-app01:/tmp/

FOLDER=$(echo $TAR | sed -e 's/.*riskweb/riskweb/g' -e 's/.tar.*//g')
BASE_DIR=/usr/local/riskweb/riskweb01/
mkdir -p $BASE_DIR/$FOLDER
cp /tmp/$TAR $BASE_DIR/$FOLDER/
echo -e "folder = " $FOLDER
cd $BASE_DIR/$FOLDER
tar xf *.xz
rm *.xz
cd ../
rm -f current
ln -s $FOLDER currentls -lrth
