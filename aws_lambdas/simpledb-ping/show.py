#!/home/abhishek.pandey/code/scripts/aws_lambdas/_ve/bin/python

from ping import *


# Remove all handlers associated with the root logger object.
for handler in lgg.root.handlers[:]:
    lgg.root.removeHandler(handler)

lgg.basicConfig(
    filename='/home/abhishek.pandey/code/scripts/aws_lambdas/simpledb-ping/logs/_show.log',
    level=lgg.INFO,
    format=FORMAT)

import boto3
logging = lgg.getLogger('show')

def _make_time(now, hour, minutes):
    return localize_time(dt(
                            now.year,
                            now.month,
                            now.day,
                            hour,
                            minutes,
                            now.second,
                            now.microsecond))

def show_today():
    sdb, now, date, day = get_all_params()
    logging.info('locals = {}'.format(locals()))
    attrs = get_processed_attrs(sdb, date)
    logging.info('attrs = {}; '.format(attrs))
    print('Today: {}'.format(attrs[Items.Date]))
    print('Entry: {}'.format(attrs[Items.Entry][11:16]))
    print('Exit: {}'.format(attrs[Items.Exit][11:16]))

    # attrs[Items.Entry] = str(_make_time(now, 11, 7))
    # put_attrs(sdb, date, attrs, replace=True)

show_today()
