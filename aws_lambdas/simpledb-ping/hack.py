#!/home/abhishek.pandey/code/scripts/aws_lambdas/_ve/bin/python
import argparse
from ping import *


# Remove all handlers associated with the root logger object.
for handler in lgg.root.handlers[:]:
    lgg.root.removeHandler(handler)

lgg.basicConfig(
    filename='/home/abhishek.pandey/code/scripts/aws_lambdas/simpledb-ping/logs/_hack.log',
    level=lgg.INFO,
    format=FORMAT)

import boto3
logging = lgg.getLogger('hack')

def _make_time(now, hour, minutes):
    return localize_time(dt(
                            now.year,
                            now.month,
                            now.day,
                            hour,
                            minutes,
                            now.second,
                            now.microsecond))

def update_today(hour, min):
    sdb, now, date, day = get_all_params()
    logging.info('locals = {}'.format(locals()))
    attrs = get_processed_attrs(sdb, date)
    logging.info('attrs = {}; type={}'.format(attrs, type(attrs)))
    new_time = _make_time(now, hour, min)
    logging.info('new time = {}; type={}'.format(new_time, type(new_time)))
    attrs[Items.Entry] = str(new_time)
    put_attrs(sdb, date, attrs, replace=True)

parser = argparse.ArgumentParser()
parser.add_argument("hour", help="what hour to set", type=int)
parser.add_argument("min", help="what minute to set", type=int)
args = parser.parse_args()
print('hour = ', args.hour)
print('min = ', args.min)

update_today(args.hour, args.min)
