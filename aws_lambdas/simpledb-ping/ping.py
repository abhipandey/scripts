#!/home/abhishek.pandey/code/scripts/aws_lambdas/_ve/bin/python


from __future__ import print_function
from datetime import datetime as dt
from datetime import timedelta
import ast
import json
import pytz
import pprint
import logging as lgg


from email import encoders
import email
from email.mime.base import *
from email.mime.multipart import *
from email.mime.text import *
import email.utils
import smtplib
import os




_DOMAIN = 'TEST_DOMAIN'
# Date Day Entry Exit Time Remarks

FORMAT = '%(asctime)-15s %(filename)s <%(lineno)s> %(message)s'

lgg.basicConfig(
    filename='/home/abhishek.pandey/code/scripts/aws_lambdas/simpledb-ping/logs/_ping.log',
    level=lgg.INFO,
    format=FORMAT)

import boto3
logging = lgg.getLogger(__name__)

logging.info('_'*80)
RESP_CODE = lambda response: response['ResponseMetadata']['HTTPStatusCode']
_MAILING_DAY  = 'Tuesday'
DATE_FMT_STR  = lambda d: dt.strftime(d, '%b-%d')
DATE_CONVERT  = lambda a: dt.strptime(a, '%Y-%m-%d %H:%M:%S.%f%Z:00')
DATE_CONVERT2 = lambda a: dt.strptime(a, '%Y-%m-%d %H:%M:%S.%f')

def get_pretty_print(item, depth=None):
    custom_printer = lambda a: (isinstance(a,list) or isinstance(a, tuple)) and \
                               len(a) > 0 and \
                               (isinstance(a[0], list) or isinstance(a[0], tuple))
    # if is_nested_dict(item):
    #     return print_nested_dict(item)
    if custom_printer(item):
        s = ''
        for i in item:
            s += '\n' + str(i)
        return s
    pp = pprint.PrettyPrinter(indent=4, depth=depth)
    return pp.pformat(item)



def send_mail(subject, body, recipients,
              author='abhishek@tilde.sg', author_name='Abhishek',
              recipient_name='Stakeholders'):
    # Create the message
    msg = MIMEMultipart()
    msg['To'] = email.utils.formataddr((recipient_name, recipients[0]))
    msg['From'] = email.utils.formataddr((author_name, author))
    msg['Subject'] = subject
    msg.attach(MIMEText(body, 'html'))



    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    # server.set_debuglevel(True) # show communication with the server
    server.starttls()
    server.login(author, 'Tsuguri0')
    try:
        server.sendmail(author, recipients, msg.as_string())
        logging.info('Mail sent: {}'.format(subject))
    finally:
        server.quit()


def localize_time(date_time):
    kl = pytz.timezone('Asia/Kuala_Lumpur')
    return kl.localize(date_time)

def get_now():
    return localize_time(dt.now())

def break_date():
    now = get_now()
    date = DATE_FMT_STR(now)
    day = dt.strftime(now, '%A')
    return now, date, day


class Items:
    Name = 'Name'
    Date = 'Date'
    Day = 'Day'
    Entry = 'Entry'
    Exit = 'Exit'
    Time = 'Time'
    Remarks = 'Remarks'
    Duration = 'Duration'
    Mail_timestamp = 'Mail_timestamp'

def quote(string):
    return string.replace("'", "''").replace('"', '""').replace('`', '``')

#########################################################
###################### DOMAIN ATTRS #################
#########################################################

def get_attributes(sdb, id):
    response = sdb.get_attributes(
        DomainName=_DOMAIN,
        ItemName=id
    )
    # logging.info('get attr = {}; type={}\n'.format(response, type(response)))
    return response['Attributes'] if 'Attributes' in response else ''
#
# def put_attributes(sdb, id, color):
#     response = sdb.put_attributes(
#         DomainName=_DOMAIN,
#         ItemName=id,
#         Attributes=[
#             {
#                 'Name': 'color',
#                 'Value': color
#             },
#         ],
#     )
#     attrs = get_attributes(sdb, id)
#     logging.info('Put attr : <{}>:: response = {}; attrs={}\n'.format(
#         id,
#         RESP_CODE(response),
#         get_pretty_print(attrs)
#         ))


def put_attrs(sdb, date, attrs, replace=False):
    response = sdb.put_attributes(
        DomainName=_DOMAIN,
        ItemName=date,
        Attributes=[
            {
              'Name': date,
              'Value': str(attrs),
              'Replace': replace
            }
        ],
    )


def get_processed_attrs(sdb, date):
    values = get_attributes(sdb, date)
    return ast.literal_eval(values[0]['Value']) if values and 'Value' in values[0] else ''


#########################################################
#########################################################
#########################################################

def select_all():
    select = 'select * from %s where color like "%%%s%%"' % (_DOMAIN, quote('blue'))
    logging.info('select = {}\n'.format(select))
    response = sdb.select(
        SelectExpression=select,
    )
    logging.info(get_pretty_print(response['Items'] if 'Items' in response else ''))

def delete_domain():
    response = sdb.delete_domain(DomainName=_DOMAIN)
    # logging.info(RESP_CODE(response))


def has_today_started(sdb, date):
    return get_attributes(sdb, date)

def create_todays_entry(sdb, now, date, day):
    if now.hour < 6 or now.hour > 12:
        logging.info('Hour of now 6> {} is >=12. Ignoring'.format(now))
        return
    logging.info('Creating todays entry')
    if day in ['Saturday', 'Sunday']:
        logging.info('Today is {}. Not updating timestamps'.format(day))
        return
    put_attrs(sdb,
        date,
        {
          Items.Date: date,
          Items.Day: day,
          Items.Entry: str(now)
        }
    )
    return True

def update_todays_latest_entry(sdb, now, date, day, entry):
    logging.info('Updating todays entry')
    if now.hour >= 19:
        logging.info('Hour of now <{}> is >=19. Ignoring'.format(now))
        return

    put_attrs(sdb,
        date,
        {
          Items.Date: date,
          Items.Day: day,
          Items.Entry: entry,
          Items.Exit: str(now)
        },
        True
    )
    return True


def test_and_check_date(sdb, now, date, day):
    cur_attrs = has_today_started(sdb, date)
    logging.info('has today ({}) started = {}'.format(date, cur_attrs))
    if not cur_attrs:
        create_todays_entry(sdb, now, date, day)
    else:
        logging.info('Today has STARTED already: curr={}'.format(cur_attrs[0]['Value']))
        value = cur_attrs[0]['Value']
        entry  = ast.literal_eval(value)['Entry']
        logging.info('Date <{}> entry = {}'.format(date, entry))
        update_todays_latest_entry(sdb, now, date, day, entry)


def main():
    now, date, day = break_date()
    test_and_check_date(now, date, day)

def get_prev_7_days(sdb, now):
    return [DATE_FMT_STR(now + timedelta(days=x)) for x in range(-6, 1)]

def hours_minutes(td):
    seconds = td.total_seconds()
    hours = seconds // 3600
    minutes = (seconds % 3600) // 60
    seconds = seconds % 60
    return int(hours), int(minutes)


def get_timeperiods_of_days(sdb, dates):
    periods = {}
    for date in dates:
        period = get_processed_attrs(sdb, date)
        periods[date] = period
    return periods




STYLES = {
    'TABLE': """ style="width: 100%;border-collapse: collapse;-webkit-box-shadow: 10px 10px 17px -2px rgba(63, 114, 133, 0.93);-moz-box-shadow: 10px 10px 17px -2px rgba(63, 114, 133, 0.93);box-shadow: 10px 10px 17px -2px rgba(63, 114, 133, 0.93);margin-bottom: 50px;border: 1px solid #D1E7FF" """,
    'TH': """  style="border: 1px solid #D1E7FF;font-variant: small-caps;font-size: 1.2em;padding: 3px;color: #444;font-weight: 500;background-image: radial-gradient(#5FD7FF, #48D1FF)" """,
    'TD': """  style="border: 1px solid #D1E7FF;padding: 3px 20px 4px 10px;font-size: 0.8em" """
}


BODY = '''
<html>
<body>
    <table{TBL_STYLE}>

        <thead>
            <tr>
                <th{TH_STYLE}>Date</th>
                <th{TH_STYLE}>Entry</th>
                <th{TH_STYLE}>Exit</th>
                <th{TH_STYLE}>Duration</th>
            </tr>
        </thead>

        <tbody>
            {ROWS}
        </tbody>
    </table>
</body>
</html>
'''

TR_FMT = '''
            <tr>
                <td{STYLE}>{NAME}</td>
                <td{STYLE}>{ENTRY}</td>
                <td{STYLE}>{EXIT}</td>
                <td{STYLE}>{DURATION}</td>
            </tr>
'''

SUM_LINE = '''
</br></br></br>

<p>
    Total Summation of all hours so far: {}
</p>
'''

def define_durations(last_7_days):
    logging.info('last_7_days={}'.format(last_7_days))
    s = ''
    for d in last_7_days:
        if last_7_days[d]:
            logging.info('d = {}; entry={} / {}; exit={} / {}'.format(
                last_7_days[d],
                last_7_days[d][Items.Entry], type(last_7_days[d][Items.Entry]),
                last_7_days[d][Items.Exit], type(last_7_days[d][Items.Exit])
            ))
            try:
                last_7_days[d][Items.Duration] = DATE_CONVERT(last_7_days[d][Items.Exit]) \
                                            - DATE_CONVERT(last_7_days[d][Items.Entry])
            except:

                    last_7_days[d][Items.Duration] = DATE_CONVERT2(last_7_days[d][Items.Exit][:-6]) \
                                                - DATE_CONVERT2(last_7_days[d][Items.Entry][:-6])
            s += TR_FMT.format(
                STYLE= STYLES['TD'],
                NAME=last_7_days[d][Items.Date],
                ENTRY= last_7_days[d][Items.Entry],
                EXIT= last_7_days[d][Items.Exit],
                DURATION= last_7_days[d][Items.Duration],
            )
    return BODY.format(
        TBL_STYLE= STYLES['TABLE'],
        TH_STYLE=  STYLES['TH'],
        ROWS= s

    )

# not in use
def define_durations_str(last_7_days):
    fmt = '{:30} {:<40} {:<40} {:25}'
    s = ''
    logging.info('last_7_days={}'.format(last_7_days))
    s += fmt.format(
        Items.Name,
        Items.Entry,
        Items.Exit,
        Items.Duration
        )
    for d in last_7_days:
        if last_7_days[d]:
            logging.info('d = {}; entry={} / {}; exit={} / {}'.format(
                last_7_days[d],
                last_7_days[d][Items.Entry], type(last_7_days[d][Items.Entry]),
                last_7_days[d][Items.Exit], type(last_7_days[d][Items.Exit])
            ))
            last_7_days[d][Items.Duration] = DATE_CONVERT(last_7_days[d][Items.Exit]) \
                                            - DATE_CONVERT(last_7_days[d][Items.Entry])
            s += fmt.format(
                last_7_days[d][Items.Date],
                last_7_days[d][Items.Entry],
                last_7_days[d][Items.Exit],
                last_7_days[d][Items.Duration],
            )
    return s


def set_mail_timestamp(sdb, date, now_attrs):
    logging.info('set_mail_timestamp <{}>: {}'.format(date, now_attrs))
    put_attrs(sdb, date, now_attrs, True)
    return True

def get_all_params():
    sdb = boto3.client('sdb', 'ap-southeast-1',
        aws_access_key_id='AKIAJERI43L7GKNMUR5A',
        aws_secret_access_key='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')
    response = sdb.create_domain(DomainName=_DOMAIN)
    logging.info('create domain response={}\n'.format(response))

    now, date, day = break_date()
    return sdb, now, date, day

if __name__ == "__main__":
    sdb, now, date, day = get_all_params()
    logging.info('now={}, date={}, day ={}'.format(now,date, day))
    test_and_check_date(sdb, now, date, day)



    # delete_domain()
