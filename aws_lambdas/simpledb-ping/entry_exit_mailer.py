#!/home/abhishek.pandey/code/scripts/aws_lambdas/_ve/bin/python

from ping import *


# Remove all handlers associated with the root logger object.
for handler in lgg.root.handlers[:]:
    lgg.root.removeHandler(handler)

lgg.basicConfig(
    level=lgg.INFO,
    format=FORMAT)

import boto3
logging = lgg.getLogger('mailer')





def send_mailer():
    sdb, now, date, day = get_all_params()
    logging.info('locals = {}'.format(locals()))
    test_and_create_report(sdb, now, date, day)


def test_and_create_report(sdb, now, date, day):
    # if day == _MAILING_DAY:
    now_attrs = get_processed_attrs(sdb, date)

    last_7_days = get_timeperiods_of_days(sdb, get_prev_7_days(sdb, now))
    msg = define_durations(last_7_days)
    total_duration_in_last_7_days = sum([last_7_days[d][Items.Duration] for d in last_7_days if last_7_days[d]], timedelta(0))

    msg += SUM_LINE.format('{}:{}'.format(
        *hours_minutes(total_duration_in_last_7_days)))
    send_mail(
        'Work hours on {}'.format(date),
        msg, 'abhi.pandey@gmail.com'
    )


if "__main__" == __name__:
    send_mailer()

def handler(event, context):
    send_mailer()
