import re
import pyshark
import binascii
import pickle

_FILE = './capture-all.pcap'
# _FILE = 'p1_00000_20200221060000.pcap'
# _FILE = 'p2_00000_20200221060000.pcap'
PICKLE_FILE = _FILE + '.pkl'
READ_PCAP_NOT_PICKLE = True
BREAK_AT_1K = False

if READ_PCAP_NOT_PICKLE:
    print(f'Reading PCAP {_FILE}')
    cap = pyshark.FileCapture(_FILE,
                              use_json=True,
                              include_raw=True,
                              display_filter='tcp')
    print('reading complete')

    b = dict()
    seqs = dict()
    first = dict()
    seen = dict()
    full_break = False

    conn_req = 0
    logon_req = 0
    logon_rsp = 0
    subscribe_req = 0
    subscribe_rsp = 0

    for packet in cap:
        src = packet.ip.src + ":" + packet.tcp.srcport
        dst = packet.ip.dst + ":" + packet.tcp.dstport
        contents = str(binascii.unhexlify(packet.frame_raw.value))

        if src not in seqs:
            # print(f'src: {src} not in seqs: {seqs}; len={len(seqs)}')
            seqs[src] = conn_req
            conn_req = conn_req + 1

        while True:
            meta = re.match(".*(63|64|69|70)=\[.*", contents)
            update = re.match(".*(140|49)=\[.*?\|2=(\d+)\|.*\|5=([0-9]+)\|",
                              contents)
            meta_found = 'meta' if meta else ''
            update_found = 'update' if update else ''
            # print(
            #     f'content = {content_str}; meta={meta_found}; update_found={update_found}'
            # )
            if meta:

                mtype = meta.group(1)

                txn_type = 'Req' if mtype in ['63', '69'] else 'Resp'
                me = src if mtype in ['63', '69'] else dst
                them = dst if mtype in ['63', '69'] else src

                if me not in b:
                    b[me] = dict()
                    b[me]['conn'] = seqs[me]
                    # print(f'One conn for {me}; seq={seqs[me]}')
                b_tmp = b[me].copy()
                b[me]['src'] = them
                b[me]['dst'] = me
                # print(
                #     f'mtype = {mtype}/{txn_type}; me = {me}; src = {them}; dest = {me}'
                # )
                if mtype == '63':
                    b[me]['TaxLogonReq'] = logon_req
                    logon_req = logon_req + 1
                elif mtype == '64':
                    if 'Time' not in b[me]:
                        def_time = str(packet.sniff_timestamp)[:-7].replace(
                            ', 2020', '')
                        b[me]['Time'] = def_time
                    b[me]['TaxLogonRsp'] = logon_rsp
                    logon_rsp = logon_rsp + 1
                elif mtype == '69':
                    b[me]['TaxSnapshotSubscribeReq'] = subscribe_req
                    subscribe_req = subscribe_req + 1
                elif mtype == '70':
                    b[me]['TaxSnapshotSubscribeRsp'] = subscribe_rsp
                    subscribe_rsp = subscribe_rsp + 1

                contents = contents[meta.end(1):]

                # value = {k: b[me][k] for k in set(b[me]) - set(b_tmp)}
                # if value:
                #     print(f'b.tmp[{me}] change:: {value}')
                continue

            elif update:
                mtype = update.group(1)
                seq = update.group(2)
                # print(f'Update: mtype={mtype}; seq={seq}; ob={ob}')

                if mtype not in first:
                    first[mtype] = dict()
                    print(f'update.mtype. add in first: {first}')
                if seq not in seen:
                    seen[seq] = True
                    if dst not in first[mtype]:
                        first[mtype][dst] = 0
                    else:
                        first[mtype][dst] = first[mtype][dst] + 1
                        if (BREAK_AT_1K and first[mtype][dst] > 1000):
                            full_break = True
                            print(
                                f'greater than 1000 ; mtype={mtype}; dst={dst} '
                            )

                contents = contents[update.end(3):]
                continue

            else:
                break
        if full_break:
            break

    print('cap loop finished')
    print(f'b so far = {b}')

    for stat in b:
        if '49' in first and stat in first['49']:
            b[stat]['49'] = first['49'][stat]
        else:
            b[stat]['49'] = 0
        if '140' in first and stat in first['140']:
            b[stat]['140'] = first['140'][stat]
        else:
            b[stat]['140'] = 0

    print('b loop finished')
    print(f'-----------------')
    print(f'b = {b}')
    print(f'-----------------')
    print(f'first={first}')
    print(f'-----------------')
    if not BREAK_AT_1K:
        pickle.dump(b, open(PICKLE_FILE, "wb"))
        print('pickle saved')

else:
    print('reading pickle')
    b = pickle.load(open(PICKLE_FILE, "rb"))
    print(f'read b as {b}')


def normalize(key):
    # normalize
    value_range = [v[key] for k, v in b.items()]
    if len(set(value_range)) == 1:
        return  # all the same

    s_value_range = sorted(range(len(value_range)),
                           key=lambda k: value_range[k])
    print(
        f'normalize: key={key}; values={value_range}; s_value_range ={s_value_range}'
    )
    for k in b:
        b[k][key] = s_value_range[0]
        s_value_range = s_value_range[1:]


normalize('conn')
normalize('TaxLogonReq')
normalize('TaxLogonRsp')
normalize('TaxSnapshotSubscribeReq')
normalize('TaxSnapshotSubscribeRsp')

print(
    "|| destination || source || connection || TaxLogonReq || LoginTime || TaxLogonRsp || TaxSnapshotSubscribeReq || TaxSnapshotSubscribeRsp || MarketByLevelEvent || TradeEvent ||"
)

for stat in sorted(b.values(), key=lambda x: x['140'], reverse=True):
    print(" | " + " | ".join([
        stat['dst'], stat['src'],
        str(stat['conn']),
        str(stat['TaxLogonReq']),
        str(stat['Time']),
        str(stat['TaxLogonRsp']),
        str(stat['TaxSnapshotSubscribeReq']),
        str(stat['TaxSnapshotSubscribeRsp']),
        str(stat['140']),
        str(stat['49'])
    ]) + " | ")
