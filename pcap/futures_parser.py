import re
import pyshark
import binascii
import pickle
import sys

_FILE = './futures-3.pcap'
PICKLE_FILE = _FILE + '.pkl'
READ_PCAP_NOT_PICKLE = True
BREAK_AT_1K = False
msg_types = set()

MSG_TYPE_PATTERN = re.compile(' (\d+)=\[(.*)\]')
if READ_PCAP_NOT_PICKLE:
    print(f'Reading PCAP {_FILE}')
    cap = pyshark.FileCapture(_FILE,
                              use_json=True,
                              include_raw=True,
                              display_filter='tcp')
    print('reading complete')

    # b = dict()
    # seqs = dict()
    # first = dict()
    # seen = dict()
    # full_break = False

    # conn_req = 0
    # logon_req = 0
    # logon_rsp = 0
    # subscribe_req = 0
    # subscribe_rsp = 0

    for packet in cap:
        src = packet.ip.src + ":" + packet.tcp.srcport
        dst = packet.ip.dst + ":" + packet.tcp.dstport
        contents = str(binascii.unhexlify(packet.frame_raw.value))
        # print(f'contents = {contents}')
        time_str = str(packet.sniff_timestamp)

        while True:
            find_msg = re.search(MSG_TYPE_PATTERN, contents)
            if find_msg:
                msg_type = find_msg.group(1)
                orig_msg = find_msg.group(2)
                if (msg_type == '295'):
                    print(f'Msg: 295: {orig_msg}')
                if msg_type not in msg_types:
                    msg_types.add(msg_type)
                    print(
                        f'{time_str}:: new msg type {msg_type}; msg = {orig_msg}; time = \n'
                    )
                contents = contents[find_msg.end(2):]
            else:
                break
            # print(f'{time_str} >> found : {msg_type}; orig={orig_msg}')

        # if src not in seqs:
        #     # print(f'src: {src} not in seqs: {seqs}; len={len(seqs)}')
        #     seqs[src] = conn_req
        #     conn_req = conn_req + 1

        # while True:
        #     meta = re.match(".*(63|64|69|70)=\[.*", contents)
        #     update = re.match(".*(140|49)=\[.*?\|2=(\d+)\|.*\|5=([0-9]+)\|",
        #                       contents)
        #     meta_found = 'meta' if meta else ''
        #     update_found = 'update' if update else ''
        #     if meta or update:
        #         print(f'content = {contents}; meta={meta_found}; '
        #               f'update_found={update_found}; time_str = {time_str}')
        #         sys.exit(0)
        #     break
        # if meta:

        #     mtype = meta.group(1)

        #     txn_type = 'Req' if mtype in ['63', '69'] else 'Resp'
        #     me = src if mtype in ['63', '69'] else dst
        #     them = dst if mtype in ['63', '69'] else src

        #     if me not in b:
        #         b[me] = dict()
        #         b[me]['conn'] = seqs[me]
        #         # print(f'One conn for {me}; seq={seqs[me]}')
        #     b_tmp = b[me].copy()
        #     b[me]['src'] = them
        #     b[me]['dst'] = me
        #     # print(
        #     #     f'mtype = {mtype}/{txn_type}; me = {me}; src = {them}; dest = {me}'
        #     # )
        #     if mtype == '63':
        #         b[me]['TaxLogonReq'] = logon_req
        #         logon_req = logon_req + 1
        #     elif mtype == '64':
        #         if 'Time' not in b[me]:
        #             def_time = str(packet.sniff_timestamp)[:-7].replace(
        #                 ', 2020', '')
        #             b[me]['Time'] = def_time
        #         b[me]['TaxLogonRsp'] = logon_rsp
        #         logon_rsp = logon_rsp + 1
        #     elif mtype == '69':
        #         b[me]['TaxSnapshotSubscribeReq'] = subscribe_req
        #         subscribe_req = subscribe_req + 1
        #     elif mtype == '70':
        #         b[me]['TaxSnapshotSubscribeRsp'] = subscribe_rsp
        #         subscribe_rsp = subscribe_rsp + 1

        #     contents = contents[meta.end(1):]

        #     # value = {k: b[me][k] for k in set(b[me]) - set(b_tmp)}
        #     # if value:
        #     #     print(f'b.tmp[{me}] change:: {value}')
        #     continue

        # else:
        #     break

    if not BREAK_AT_1K:
        pickle.dump(b, open(PICKLE_FILE, "wb"))
        print('pickle saved')

else:
    print('reading pickle')
    b = pickle.load(open(PICKLE_FILE, "rb"))
    print(f'read b as {b}')
