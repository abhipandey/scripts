#!/usr/bin/env python3.7
import json
# from datetime import datetime as dt
import datetime
from collections import defaultdict
import re

import decimal

# P = r'(?P<TIME>.{27}).*\|6=(?P<REDIRECTED>.*?)\|.*ipaddress.(?P<PRELOGON>.*?)]'
# _FILE = '/tmp/2'
# import re

# lines = []
# with open(_FILE, 'r') as f:
#     lines = f.readlines()
#     print(f'{lines}')

# seq = defaultdict(list)
# info = []
# for l in lines:
#     d = re.search(P, l, re.IGNORECASE).groupdict()
#     info.append(d)
#     key = d["TIME"][:17]
#     ip = d['PRELOGON'][ -2:]
#     seq[key].append(ip)
#     # print(f'{key} = {seq[key]}')

# for key, ip_seq in seq.items():
#     print(f'{key} --> {ip_seq}')
# get_dt = lambda x: dt.strptime(x, '%Y-%m-%d %H:%M:%S.%f')
FMT = '%Y-%m-%d %H:%M:%S.%f'
s = '2020-02-05 09:29:59.990000'
x = datetime.datetime.strptime(s, FMT)
print(x)

lines = []
with open('r.06.log', 'rt') as file:
    lines = file.readlines()

for l in lines:
    res = re.findall('sent.(.*).remote.(.*).received.(.*)', l)[0]
    print(f'res = {res}')
    stamps = [datetime.datetime.strptime(s, FMT) for s in res]
    print(f' stamps = {stamps}')
    exp_remote = stamps[0] + (stamps[2] - stamps[0]) / 2
    actual_remote = stamps[1]
    sign = ''
    if actual_remote > exp_remote:
        diff = actual_remote - exp_remote
    else:
        sign = '-'
        diff = exp_remote - actual_remote

    # print(f'  diff = {diff}')
    diff_ms = (diff.microseconds) / 1000
    print(
        f'   expect remote = {exp_remote}; act remote = {stamps[1]}; diff_ms = {sign}{diff_ms}; sent={stamps[0]}; recv={stamps[2]}'
    )

    # line_dict = eval(l)
    # del line_dict['SENT_DT']
    # del line_dict['RECV_DT']
    # exp_time = line_dict['serv_exp_s']
    # exp = datetime.datetime.strptime(s, FMT)
    # line_dict['EXP'] = exp
    # diff = 0

    # if line_dict['REMOTE_DT'] > exp:
    #     diff = line_dict['REMOTE_DT'] - exp
    #     sign = ''
    # else:
    #     diff = exp - line_dict['REMOTE_DT']
    #     sign = '-'
    # diff_ms = diff.microseconds / 1000
    # print(f'exp = {line_dict}; diff = {sign}{diff_ms}')