#!/home/abhishek.pandey/apps/venvs/ve_opt_3.5/bin/python3.5

import os
import sys
import glob
import subprocess
import re


def run_command(command):
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    while True:
        output = process.stdout.readline()
        if output == b'' and process.poll() is not None:
            break
        if output:
            print(output.decode(encoding='UTF-8', errors='ignore').strip())
            sys.stdout.flush()
    if process.returncode:
        print(
            'ERROR: Some problem occurred. Please investigate. Command run was: {}'.
            format(command))
        sys.exit(process.returncode)
    rc = process.poll()
    if process.returncode:
        print(
            'ERROR: Some problem occurred during poll. Please investigate. Command run was: {}'.
            format(command))
        sys.exit(process.returncode)
    return rc


_DIR = '/home/abhishek.pandey/apps/code/p/higgs7/'
_HUMMER_DIR = 'ccgh/hummer/src/gng/strategy/'
_AQUA_DIR = 'ccgh/aqua/src/gng/xraider/algos/'


def get_files(dir):
    files_cc = glob.glob('{}/**/*.cc'.format(dir), recursive=True)
    files_h = glob.glob('{}/**/*.h'.format(dir), recursive=True)
    return files_cc + files_h


def port_algo(algo_name):
    src_dir = os.path.join(_DIR, _HUMMER_DIR)
    dest_dir = os.path.join(_DIR, _AQUA_DIR)
    algo_hummer_dir = os.path.join(src_dir, algo_name)
    algo_aqua_dir = os.path.join(dest_dir, algo_name)

    run_command('rm -rf {}'.format(algo_aqua_dir))
    run_command('cp -r {} {}'.format(algo_hummer_dir, algo_aqua_dir))
    run_command('ls {}'.format(algo_aqua_dir))

    files = get_files(algo_aqua_dir)
    for f in files:

        manage_file(f, algo_hummer_dir, algo_aqua_dir)
        run_command('/opt/clang50/bin/clang-format -i {}'.format(f))

        run_command(
            "perl -i -p0e 's/namespace gng \{\nnamespace strategy \{/namespace gng::engine::xraider \{/igs' "
            + f)
        run_command(
            "perl -i -p0e 's/\}  \/\/ namespace strategy\n\}  \/\/ namespace gng/}  \/\/ namespace gng::engine::xraider/igs' "
            + f)


def get_file_lines(file_path):
    with open(file_path, 'rt') as file_placeholder:
        lines = [
            l.strip('\n') for l in file_placeholder.readlines()
            if l.startswith('#include "gng')
        ]
        return lines


def manage_file(filename, algo_hummer_dir, algo_aqua_dir):
    lines = get_file_lines(filename)
    pat = re.compile('"(.*)"')
    for l in lines:
        include_path = re.search(pat, l).group(1)
        just_filename = os.path.basename(include_path)
        dirname = os.path.dirname(include_path)
        # print(dirname, ",", just_filename)
        if dirname.startswith('gng/strategy'):
            # do we need to change?
            src_dir = os.path.join(_DIR, 'ccgh/aqua/src/')
            ret = get_new_path_for_aqua(src_dir, just_filename, include_path)
            manage_new_path_for_aqua(include_path, ret, filename)
            print('src_dir={:>50} ----> {}'.format(include_path, ret))


def manage_new_path_for_aqua(include_path, new_path, filename):
    def remove_str(text):
        text = text.replace('/', '.')
        cmd = "sed -i 's/.*{}.*//g' {}".format(text, filename)
        print(' Remove : {}'.format(cmd))
        run_command(cmd)

    def replace_str(text, replacement):
        text = text.replace('/', '.')
        replacement = replacement.replace('/', '\/')
        cmd = "sed -i 's/{}/{}/g' {}".format(text, replacement, filename)
        print(' Replace: {}'.format(cmd))
        run_command(cmd)

    if not new_path:
        remove_str(include_path)
    elif include_path != new_path:
        replace_str(include_path, new_path)


def get_new_path_for_aqua(dir, filename, include_path):
    def trim_file_location(results, filename, include_path):
        indices = [i for i, x in enumerate(include_path) if x == '/']
        # print('indices={}'.format(indices))
        search_for = include_path[indices[-2]:]
        # print('search_for={}'.format(search_for))
        new_loc = [x for x in results if search_for in x] or []
        # print('new loc = {}'.format(new_loc))
        return new_loc

    command = 'find {} -iname "{}"'.format(dir, filename)
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    ret = process.communicate()[0].decode('utf-8', 'ignore')
    # print('ret = {}'.format(ret))
    ret = [x.replace(dir, '') for x in ret.split('\n') if x]
    if len(ret) > 1:
        ret = trim_file_location(ret, filename, include_path)
    return ret[0] if ret else ''


def insert_text(filename, lineno, text):
    # sed -i '8i22 This is Line 8'
    cmd = "sed -i '{}i{}' {}".format(lineno, text, filename)
    run_command(cmd)


port_algo('stopper')
