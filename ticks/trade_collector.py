
import os, sys, re
from fnmatch import translate
import datetime
import csv
import pprint
from collections import namedtuple
import pandas as pd
import numpy as np
from itertools import groupby

try:
    from .l3_to_l2 import get_order_ts, get_trade_ts
except:
    pass

from l3_to_l2 import get_order_ts, get_ts

_SLOTS = '''instrument	box_time	type	seq_number	update_type	instrument_id	state	ask_price	ask_qty	ask_time	ask_time_nanos	bid_price	bid_qty	bid_time	bid_time_nanos	trade_price	trade_qty	trade_time	trade_time_nanos	trade_date	trade_condition_is_deleted	ohlc_current_open	ohlc_current_high	ohlc_current_low	ohlc_current_close	ohlc_previous_open	ohlc_previous_high	ohlc_previous_low	ohlc_previous_close	ask_at_market_size	bid_at_market_size	cumulative_volume	phase'''.split('\t')
class TradeLine:
    __slots__ = tuple(_SLOTS)


    def __init__(self, *kwargs):
        for slot, val in zip(TradeLine.__slots__, kwargs):
            setattr(self,
                    slot,
                    float(val) if slot == 'trade_price' else
                    int(val) if slot == 'trade_qty' else val)

    def __getitem__(self, item):
        return getattr(self, _SLOTS[item])

    def __setitem__(self, key, value):
        setattr(self, _SLOTS[key], value)

    def __str__(self):
        return ' ; '.join(['{}: {}'.format(slot, getattr(self, slot)) for slot in OneTick.__slots__])

    def __repr__(self):
        return str(self)


class TradeCollector:
    '''
    Keeps track of the trades done by specific instrument
    '''
    def __init__(self, files):
        files = [f for f in files if f.endswith('csv')]
        self.files = [x.replace('l3', 'l1').replace('5900', '0000')
                      for x in files]
        print('files = ', self.files)
        self.trades_by_instr = dict()

        self.file_descriptors = []
        self.readers = dict()
        self.ticks = dict()
        self.read_trades()
        self.header = None


    def destroy(self):
        for fd in self.file_descriptors:
            fd.close()

    def read_trades(self):
        for file in self.files:
            f = open(file, 'rt')
            r = csv.reader(f)
            key = self.get_key(file)
            self.header = next(r)
            self._next_trade_line(r, key)
            self.file_descriptors.append(f)
            self.readers[key] = r

    def get_key(self, instr):
        if 'NK225M' in instr:
            return 'NK225M'
        if 'TOPIX' in instr:
            return 'TOPIX'
        return 'NK225'

    def _next_trade_line(self, reader, key):
        print(key + '#' * 300)
        cached_tick = None
        while True:
            # print('_' * 3)
            l = next(reader)
            # print('R: {}; {} ; line={}'.format(key, l[19], l))
            if l[19]:
                print('{} tick = {}'.format(key, l))
                cached_tick = self.ticks[key] if key in self.ticks else None
                self.ticks[key] = l
                break
        return cached_tick

    def get_trades(self, tick):
        global TradeLine
        instr = tick.instrument
        tick_ts = get_order_ts(tick)
        key = self.get_key(instr)
        reader = self.readers[key]

        # print('reader = {}'.format(reader))

        trades = []
        try:

            def _next_trade_line():
                while True:
                    l = next(reader)
                    if l[19]:
                        print('{} tick = {}'.format(key, l))
                        # cached_tick = self.ticks[key]
                        # self.ticks[key] = l
                        return l
            while(True):
                # f_tick: file tick
                '''
                # cached_tick = self.ticks[key]
                        # self.ticks[key] = l
                '''
                f_tick = self.ticks[key]  # _next_trade_line()
                f_ts = get_ts('{} {}'.format(f_tick[19], f_tick[17]))
                if f_ts > tick_ts:
                    print('{}: Moved past <{}>: {}'.format(
                        key, tick_ts, f_tick
                    ))
                    return trades
                if f_ts == tick_ts:
                    print('{}: Matched trade times <{}>. Adding {}'.format(
                        key, tick_ts, f_tick))
                    trades.append(TradeLine(*f_tick))
                    trades[-1].trade_price = float(trades[-1].trade_price)
                    trades[-1].trade_qty = float(trades[-1].trade_qty)


                if f_ts < tick_ts:
                    print('{} read next line.... '.format(key))
                self.ticks[key] = _next_trade_line()


        except Exception as e:
            print(str(e))
            print('locals = ', locals())
            raise e