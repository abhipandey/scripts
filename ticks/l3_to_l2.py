import logging as l_logger
import os, re, sys
import pandas as pd
import numpy as np
import bisect
import datetime as dt

from fractions import Fraction

from enum import Enum
from collections import namedtuple

OP_BUY = 'buy'
OP_SELL = 'sell'
NoneType = type(None)
UOC_DELETE = 'delete'
UOC_UPDATE = 'update'

BPOS, SPOS = 0, 1

# MKTKEY = float('inf')
MKTKEY = np.inf
DOWN = False
UP = True


class TickSide(Enum):
    BUY = 1
    SELL = 2


_TS_FMT = '%Y-%m-%d %H:%M:%S.%f+00:00'
X = _TICKS = 1
Y = _TIMEFRAME = 10  # ms
Z = 50  # %
STICK_REF_TS = 2  # ms
FLIPPER_VALIDITY = 5  # ms
_MN_ASSUMPTION = X * (1 + Z)
COLUMNS = ['instrument',
          'box_time',
          'type',
          'update_op_code',
          'instrument_id',
          'order_date',
          'order_time',
          'order_time_nanos',
          'symbol',
          'order_type',
          'side',
          'price',
          'qty',
          'seq_number',
          'exchange_order_id',
          'matching_on_close']

####################################################################################################
####################################################################################################
                                # Qing Huang Version
####################################################################################################
####################################################################################################


def get_trade_ts(trades):
    return get_ts_attr(trades[0], 'trade_date', 'trade_time')

def get_order_ts(order):
    return get_ts_attr(order, 'order_date', 'order_time')

def get_ts_attr(obj, obj_date, obj_time):
    ts_str = '{} {}'.format(
        getattr(obj, obj_date),
        getattr(obj, obj_time))
    # print('order = {}'.format(order))
    return get_ts(ts_str)



def get_ts(ts_str):
    return dt.datetime.strptime(ts_str, _TS_FMT)


# filename expression hard coded inside. Change it to suit your needs!
def read_data_order(symbol, date, time='070000-140000'):
    # yy, mm, dd = date / 10000, date / 100 % 100, date % 100
    # time_start, time_end = time.split('-')[0], time.split('-')[1]

    # You'll need to change these for sure!!!
    # file_name = 'data_dump/STOCK_XTKS_' + str(symbol) + '_-l3-' + str(date) + str(time_start) + '.000000-' + str(date) + str(
    #     time_end) + '.000000.csv'
    file_name = '/home/abhishek.pandey/data/l3/FUTURE_XOSE_FUT_NK225_1703_1703-l3-20170124050000.000000-20170124145900.000000.csv'

    order_data = pd.read_csv(file_name)

    # replace nan with zeros
    order_data['order_qty'].fillna(0, inplace=True)
    order_data['order_price'].fillna(0, inplace=True)
    order_data['order_side'].fillna(0, inplace=True)

    order_data.columns = COLUMNS

    return order_data

def find_gt(a, x):
    'Find leftmost value greater than x'
    i = bisect.bisect_left(a, x)
    if i != len(a):
        return i
    raise ValueError

class L2State(object):
    '''
    L2 Book. Feed it a list of orders, it will maintain its own order book, 
        and will return you depths (ful L2 state), BBO prx and qty.

    This book is NOT aware of the different types of orders that can exist on
        each level, hence it is up to the user to NOT feed matching_on_close
        orders into it.

    _depths no longer a default dict, and will need to implement .get() to query
        values in it

    To add orders, call add_orders() with a list of orders, in the format 
        prescribed by read_data_order function above

    To get depths, call get_depths()

    To get quantity at bbo, call get_bbo??
    
    '''
    def __init__(self):
        self._depths = {} #defaultdict(lambda: np.zeros(2))
        self._order_book = OrderBook()
        self.prev_order = None
        self._edge_require_reset = True
        self._top_price = None
        self._bot_price = None
        self.tick_ts = []
        self.trade_prices = []
        self.x_tick_moves_in_y = 0
        self.vol_cleared_pct = 0
        self.last_trades = None
        self.last_bbo = None
        self.trade_side = None
        self.trade_pct_cleared = 0
        self.last_known_sizes = None



    def add_orders(self, orders):
        for row in orders.iterrows():
            self.add_order(row[1])

    def _print_trades(self, trades, tag):
        if trades:
            for t in trades:
                print('{:>15} {:40} {:30} Px: {:10} Qty: {:10} C.Vol: {:20}'.format(
                    tag,
                    *[getattr(t, a) for a in [
                        'instrument',
                        'trade_time',
                        'trade_price',
                        'trade_qty',
                        'cumulative_volume',
                    ]]
                ))

    def add_order(self, order, trades):

        # when order comes in, I need to find out the actual size, first thing
        # so that i can use this value eventually to decide whether incoming trade clears 30%
        if trades:
            self._print_trades(self.last_trades, 'LAST_TRADES')
            self._print_trades(trades, 'CURR TRADES')
            trade_price = trades[-1].trade_price
            if trade_price in self._depths:
                depth_at_trd_px = self._depths[trade_price]
                depth_at_trd_px = depth_at_trd_px[0] \
                    if depth_at_trd_px[0] > 0 \
                    else depth_at_trd_px[1]

                total_qty_traded = sum([t.trade_qty for t in trades
                                        if t.trade_price == trade_price])
                self.trade_pct_cleared = total_qty_traded * 100 / depth_at_trd_px
                print('ABHI: depth at price <{}> = {}; trade_pct_cleared={}'.format(
                    trade_price,
                    depth_at_trd_px,
                    self.trade_pct_cleared
                ))


            # print('incoming trades=', trades)
        self.vol_cleared_pct = ''
        self.x_tick_moves_in_y = ''
        order, ref_order = self._manage_order(order)  # Now check if this was a trade
        # print(order.order_time_nanos)
        # print(type(order.order_time_nanos))

        if trades:
            if not self.last_trades or \
                            self.last_trades[-1].trade_time_nanos != trades[-1].trade_time_nanos:
                # calculate vol_cleared
                if not self.last_bbo:
                    self.trade_side = TickSide.BUY
                else:
                    self.trade_side = TickSide.BUY \
                        if trades[-1].trade_price <= self.last_bbo[0]\
                        else TickSide.SELL
                self.vol_cleared_pct, additional_level = self._get_trade_vol_change(trades)

                self._check_trade(trades,
                                  order,  # current order
                                  self.prev_order,  # current -1 order
                                  ref_order,  # the old order for "order"
                                  additional_level and self.vol_cleared_pct > 130.0
                                  )
                self.prev_order = order

                print('ABHI: vol cleared: {}; additional_level={}'.format(
                    self.vol_cleared_pct,
                    additional_level))
                if self.vol_cleared_pct < 0:
                    print('_' * 80)
                    print('before={}; \nafter={}'.format(self.prev_order, order))
                    print('last bbo={}; \nnew bbo={}'.format(self.last_bbo, self.get_bbo()))

                # TODO only set last_trades if this new list is different than last one
                self.last_trades = trades
        self.last_bbo = self.get_bbo()
        print('Last BBO={}'.format(self.last_bbo))

        return [getattr(order, c) for c in COLUMNS] + \
               [
                self.trade_side,
                ('%.1f' % self.vol_cleared_pct) if self.vol_cleared_pct else '',
                self.x_tick_moves_in_y]


    def _check_trade(self, trades, order, prev_order, ref_order, add_additional_tick, debug=True):
        print('+'*80)
        # definitely trades occurred at this time
        curr_bbo = self.get_bbo()
        if debug:
            print('This is a trade <{}>: TradeTime={}; \nLast BBO= {} \nCurr BBO= {}'.format(
                order.order_time,
                trades[0].trade_time,
                self.last_bbo, curr_bbo))
        # print('prev_order = {}; \n new_order={}'.format(prev_order, order))
        # print('ref_order = {}; \n new_order={}'.format(ref_order, order))

        # if ref_order is not None and ref_order.price != 0.0:
            # main work is to calculate x_tick_moves_in_y
        self._check_for_tick_movements(trades, add_additional_tick)



    def _get_trade_vol_change(self, trades):
        print('TRADES HAVE CHANGED. Last known good BBO={}'.format(
            self.last_bbo
        ))
        additional_level = 0
        trade_price = trades[-1].trade_price
        if self.last_bbo:
            print('ABHI: trades prices  ={}; 0={}; 2={}'.format(
                trade_price, self.last_bbo[0], self.last_bbo[2]
            ))
            if trade_price not in [self.last_bbo[0], self.last_bbo[2]]:
                print('ABHI: THIS IS A NEW VALID TRADE')
                # print('ABHI: depths keys = {}'.format(self._depths))
                # print('ABHI: depths keys = {}'.format(sorted(list(self._depths.keys()))))
                # if trade_price in self._depths:
                #     depth_at_trd_px = self._depths[trade_price]
                #     print('ABHI: depth at price = ', depth_at_trd_px)
                additional_level += 1
                return 100 + self.trade_pct_cleared, additional_level
            else:
                index = 0 if trade_price == self.last_bbo[0] else 1
                print('ABHI: Side: {} size change == {} to {}... {}'.format(
                    index,
                    self.last_bbo[index + 1],
                    sum([t.trade_qty for t in trades]),
                    ''
                ))
                return min(100, (sum([t.trade_qty for t in trades]) * 100)\
                       / self.last_bbo[index + 1]), additional_level
        return 0, additional_level


    def _check_for_tick_movements(self, trades, add_additional_tick):
        ts = get_trade_ts(trades)

        # makes sense to check for tick movements only if new trades
        if ts not in self.tick_ts:
            self.tick_ts.append(ts)

            # assuming that ref ord price will never be empty or 0.0
            # assert ref_order.price != '' and ref_order.price != 0.0

            # print('trade price = {}; type={}'.format(ref_order.price, type(ref_order.price)))

            #  makes sense to add this price,, only if it valid
            self.trade_prices += [t.trade_price for t in trades]
            # Check the condition for X ticks in Y timeframe
            ts_gt = ts - dt.timedelta(milliseconds=Y)
            index = find_gt(self.tick_ts, ts_gt)
            self.tick_ts = self.tick_ts[index:]
            self.trade_prices = self.trade_prices[index:]
            self.x_tick_moves_in_y = max(self.trade_prices) \
                                     - min(self.trade_prices)\
                                     + 0.5 if add_additional_tick else 0
            # self.ticks = self.ticks[index:]
            # self.x_ticks_in_y = len(self.ticks) >= X
            print('ts = {}; ts_gt = {}'.format(ts, ts_gt))
            print('ticks_gt = {}'.format([str(s) for s in self.tick_ts]))
            print('trade_prices = {}'.format([str(s) for s in self.trade_prices]))
            print('ABHI: x_tick_moves_in_y = {}'.format(self.x_tick_moves_in_y))

            # not true anymore
            # assert len(self.tick_ts) == len(self.trade_prices)


    def _manage_order(self, order):
        symbol = order.symbol
        ref_order = self._order_book.get_order(symbol)
        if ref_order is not None:

            if (order.update_op_code == UOC_DELETE):
                self._remove_from_depth(ref_order)
                self._order_book.delete_order(symbol)
                order.price = ref_order.price
            else:
                # dirty hack very bad
                # if order.symbol == ';B5E6D450300249F37':
                #     print('#'*80)
                #     print(order)
                #     print('#'*80)
                self._replace_depth(ref_order, order)
                order = self._patch_modifying_order(ref_order, order)
                self._order_book.replace_order(symbol, order)
        else:
            # we have a New order!
            self._order_book.add_order(symbol, order)
            self._add_to_depth(order)

        return order, ref_order

    def _patch_modifying_order(self, ref_order, new_order):
        '''
        Needed because modifying orders don't come with all fields, since we are using box raw ticks for this study
        Fields which are not changed are omitted.
        Need to take values from old order and patch the new modifying order
        '''
        # print('\nMODIFYING')
        # print(new_order)
        #
        # print('\nwith')
        # print(ref_order)
        if not new_order.qty:
            new_order.qty = ref_order.qty
        if not new_order.side:
            new_order.side = ref_order.side
        if not new_order.price:
            new_order.price = ref_order.price
        if not new_order.matching_on_close:
            new_order.matching_on_close = ref_order.matching_on_close

        # print('\nMODIFIED')
        # print(new_order)
        # print('\n\n')
        return new_order


    def get_bbo(self):
        # print('self._depths = {}'.format(self._depths))

        bid = max([key for key, value in self._depths.items() if value[0] > 0] or [-1])
        ask = min([key for key, value in self._depths.items() if value[1] > 0] or [-1])
        if ask == -1 or bid == -1:
            return None
        bid_qty = self._depths[bid][0]
        ask_qty = self._depths[ask][1]

        return (bid, bid_qty, ask, ask_qty)


    def get_depths(self):
        return self._depths

    def get_depth_edges(self):
        '''
        Function for Itayose.. just leaving it here in case you need it

        get depth edges is an expensive operation if _edge_require_reset is True
        the dict of depths has to be sorted, then the top and bottom values
        can be returned
        '''
        if self._edge_require_reset:
            keys = sorted([k for k in self._depths.keys() if k != MKTKEY])

            if len(keys) > 0:
                self._top_price = keys[-1]
                self._bot_price = keys[0]
            else:
                self._top_price = self._bot_price = None
        return self._bot_price, self._top_price

    def reset_book(self):
        self._depths.clear()
        self._order_book.reset_book()

        self._edge_require_reset = True
        self._top_price = None
        self._bot_price = None

    def _qty_convert(self, side, qty):
        return side == OP_SELL and (0, qty) or (qty, 0)

    def _add_to_depth(self, order):
        side = order.side
        qty = int(order.qty) or 0
        price = order.price or MKTKEY
        # print('locals so far={}'.format(locals()))

        # needed to avoid using default dict
        self._depths[price] = self._depths.get(price, np.zeros(2)) + self._qty_convert(side, qty)

        # adding to depth should never result in 0, so no need to delete level

        self._check_depth_edges(price, False)

    def _remove_from_depth(self, order):
        side = order.side
        qty = int(order.qty) or 0
        price = order.price or MKTKEY

        # should not need to use the try except method here since it always
        # should have qty at that level if we're trying to remove it,
        # unless sth is buggy
        # This commented line will work, but could result in error
        # self._depths[price] = self._depths.get(price, np.zeros(2)) - self._qty_convert(side, qty)

        #special addition to deal with weird box data
        if qty == 0:
            return

        try:
            self._depths[price] = self._depths.get(price, np.zeros(2)) - self._qty_convert(side, qty)
            # self._depths[price] -= self._qty_convert(side, qty)
        except KeyError:
            # print('Order received was:\n', order)
            raise("Tried to remove from a level which does not exist yet.")

        if self._depths[price][SPOS] == 0 and self._depths[price][BPOS] == 0:
            del(self._depths[price])
            self._check_depth_edges(price, True)

    def _replace_depth(self, ref_order, new_order):
        # print('old order=', ref_order)
        # print('new_order=', new_order)
        o_side = ref_order.side
        o_qty = int(ref_order.qty) or 0
        o_price = ref_order.price or MKTKEY

        n_side = o_side
        n_qty = int(new_order.qty) or o_qty
        n_price = new_order.price or o_price

        try:

            self._depths[o_price] -= self._qty_convert(o_side, o_qty)
            self._depths[n_price] += self._qty_convert(n_side, n_qty)
        except:
            pass
            # print('Exception: depths={}'.format(sorted(self._depths.keys())))
            # print('ref_order = {}'.format(ref_order))
            # print('_'*80)
            # print('new_order = {}'.format(new_order))


    def _check_depth_edges(self, price, strict):
        '''
        checks if altered depth level is at the top or bottom edge
        '''
        if self._edge_require_reset is False:

            if strict:
                if price >= self._top_price or price <= self._bot_price:
                    self._edge_require_reset = True
            else:
                if price > self._top_price or price < self._bot_price:
                    self._edge_require_reset = True


class OrderBook(object):
    '''
    Just holds all the individual orders...
    can be restructured later to take advantage of faster data structs other
    than a dictionary

    currently, OrderBook is NOT dilligent about its checks. ie, it doesnt
    make sure an order exists before trying to delete it or replace it.
    '''

    def __init__(self):
        self._orders = {}

    def reset_book(self):
        self._orders.clear()

    def get_order(self, order_symbol):
        '''
        finds order_symbol in book and returns it.
        if unable to find, return None
        '''
        order = self._orders.get(order_symbol)
        return order

    def replace_order(self, order_symbol, new_order):
        assert new_order.side in (OP_BUY, OP_SELL)
        self._orders[order_symbol] = new_order

    def delete_order(self, order_symbol):
        del(self._orders[order_symbol])

    def add_order(self, order_symbol, new_order):
        self._orders[order_symbol] = new_order

if __name__ == '__main__':
    orders = read_data_order('', '', '')
    # for o in orders:
    #     print(o)
    # print('orders = ', len(orders))
    # print('orders = ', type(orders))
    # print('orders = ', orders)
    l2 = L2State()
    l2.add_orders(orders)
