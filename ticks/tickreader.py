#!/usr/bin/python3.5

import os, sys, re
from fnmatch import translate
import datetime
import csv
import pprint
from collections import namedtuple


#############################################################################################
####################################### CONFIGURATION #######################################
#############################################################################################

# TODO Read ticks based on priority. currently it craps out
# TODO reading ticks should be time based - no matter
# TODO not every
# TODO it maybe possible that files are being repeated, with a certain time range - reader should ignore duplicated ticks in that case

_SYMBOLS = [
           ( "FUTURE|XOSE|FUT_NK225M_1703|1703", 1),
           ("FUTURE|XOSE|FUT_NK225_1703|1703", 1),
           ("FUTURE|XOSE|FUT_TOPIX_1703|1703", 2),
           #"FUTURE|XOSE|FUT_NK225_1703|1703",
           # "FUTURE|XSES|TWG17|1702",
          ]

_DAY = 20170124
_T_BEGIN = "000000.000000"  # 00:00
_T_END = "235900.000000"  # 23:59

_FROM_TIME = _T_BEGIN
_TO_TIME   = _T_END
_TZ  = "SGT"
_TS_CREATOR = lambda row: row[5] + ' ' + row[6][:row[6].find('+')]+ row[7][-3:]

_FOLDER_L3 = '/home/abhishek.pandey/data/l3/'
_FOLDER_L1 = '/home/abhishek.pandey/data/l1/'



######################################################################################################
######################################################################################################
######################################################################################################
######################################################################################################


TickLine = namedtuple('TickLine', ('SYMBOL', 'BOX_TIME', 'TYPE',
                                  'UPDATE', 'INSTR_ID', 'ORDER_DATE',
                                  'ORDER_TIME', 'ORDER_TIME_NANOS', 'ORDER_SYMBOL',
                                  'ORDER_TYPE', 'ORDER_SIDE', 'ORDER_PRICE',
                                  'ORDER_QTY', 'ORDER_SEQ_NUM', 'ORDER_EXCH_ORD',
                                  'MATCHING_ON_CLOSE'))



def pretty_print_tick(tick):
    # b = [50, 40, 10, 10, 50, 20, 30 , 15, 20, 10, 5, 10, 5, 10, 20, 10]
    # ' '.join(['{{:{}}}'.format(x) for x in b])
    print('{:50} {:40} {:10} {:10} {:50} {:20} {:30} {:15} {:20} {:10} {:5} {:10} {:5} {:10} {:20} {:10}'.format(
        *tick
    ))

def pretty_short_print(tick):
    # t = list(map(TickLine._make, tick))
    t = TickLine(*tick)
    pp = pprint.PrettyPrinter(indent=4)
    print(pp.pformat(t))

def get_next_tick():
    treader = get_reader()
    for tick in treader.read_ticks():
        # pretty_print_tick(tick)
        # print(tick)

        yield tick
        # print(len(tick))
        # if counter > 20:
        #     break

def print_row(row):
    print('{:50}: {:25}'.format(row[0], row[6][:row[6].find('.')], row[7]))

def get_all_files(folder):
    return [x[2] for x in os.walk(folder)][0]


def get_file_with_pattern(dir, pattern):
    files = os.listdir(path=dir)
    file_found = [file for file in files if re.match(pattern, file, flags=re.IGNORECASE)]
    return file_found

def find_dirs_and_files(pattern, where='.', include_dirs=True, include_files=True, debug=False):
    '''Returns list of filenames from `where` path matched by 'which'
       shell pattern. Matching is case-insensitive.'''
    if not os.path.exists(where):
        return []
    if debug:
        print("where={}; pattern={};".format(where, pattern))
        # print("where={}; pattern={};".format(where, translate(pattern)))
    rule = re.compile(pattern, re.IGNORECASE)
    # list_files = [name for name in os.listdir(where) if rule.search(name)]
    list_files = [name for name in os.listdir(where) if re.search(pattern, name)]
    if debug:
        print('list_files={}'.format(list_files))
    
    if include_dirs and not include_files:
        list_files = [name for name in list_files if
                      os.path.isdir(os.path.join(where, name))]
    if include_files and not include_dirs:
        list_files = [name for name in list_files if
                      os.path.isfile(os.path.join(where, name))]
    if include_files:
        list_files = add_folder_to_list_of_files(where, list_files)
    return list_files

def add_folder_to_list_of_files(folder, files):
    return [os.path.join(folder, file) if not os.path.isabs(file)
                and os.path.exists(os.path.join(folder, file)) else file for file in files]

_P = ['.*l[123]-(?P<FROM>.*?)-(?P<TO>.*?).csv$', ]


######################################################################################################
######################################################################################################
######################################################################################################
######################################################################################################

class TickerColReader:
    '''
    Reads all the symbol ticker files in sequence.
    Provides an interface to iterate through the l3 tick lines one by one
    '''
    def __init__(self):
        self.last_ticker_ticked = None
        self.last_ticker_ts = None
        self.ticks = None
        self.symtickers = []

    def add_symbol_ticker(self, symticker, priority):
        self.symtickers.append((symticker, priority, symticker.get_tick()))
        # self.symtickers = sorted(self.symtickers, key=lambda x: x[1])

    def read_ticks(self, debug=False):
        """
        ###################################################################################
        This is where we select the ticks to be "yielded" one by one, based on when they arrive
        Should be called only when all the symbol tickers have been added.
        ###################################################################################
        """
        if debug:
            print('_'*80)
        # Once im here, there's always gonna be tick, until the files are done
        # get the ticks with the lowest ts
        lbd_ticker = lambda : list([[i,j] for i,j in enumerate([next(symticker[2]) for symticker in self.symtickers])])
        if not self.ticks:
            self.ticks = lbd_ticker()
            # self.ticks = lbd_ticker()
        ticks_with_ts = [(x[0], _TS_CREATOR(x[1])) for x in self.ticks]
        # if debug:
        #     print('self.ticks = {}'.format(self.ticks))
        if debug:
            print('  ticks_with_ts {}'.format(ticks_with_ts))


        # this can be buggy
        lowest_ts = min([x[1] for x in ticks_with_ts])
        if debug:
            print('  lowest_ts {}'.format(lowest_ts))

        similar_ticks = [x for x in ticks_with_ts if x[1] == lowest_ts]
        if debug:
            print('  similar_ticks {}'.format(similar_ticks))
        if len(similar_ticks) > 2:
            print('Found an exception, we need to break')
            exit(1)
        index_to_move = similar_ticks[0][0]
        if debug:
            print(' ... indexto move = {}'.format(index_to_move))
        # next_tick = self.symtickers[index_to_move][2]
        # next(self.ticks[index_to_move][2])
        val = self.ticks[index_to_move][1]
        self.ticks[index_to_move][1] = next(self.symtickers[index_to_move][2], None)
        # print('    val = {}'.format(self.ticks[index_to_move][1]))
        yield val
        # print('self.ticks[index_to_move][1]={}'.format(self.ticks[index_to_move][1]))
        # self.ticks[index_to_move][1] = next(self.symtickers[index_to_move][2])


    def __str__(self):
        return 'CollReader: We have {} symbols: {}'.format(len(self.symtickers), self.symtickers)

class SymbolTicker:
    def __init__(self, ticker_name, files):
        self.ticker_name = ticker_name
        self.filepaths = files
        self.tickerfiles = []
        for f in files:
            matches = re.search(_P[0], f)
            if matches:
                dates = matches.groupdict()
                # print('dates = {}'.format(dates))
                dto = datetime.datetime.strptime(dates['TO'], '%Y%m%d%H%M%S.000000')
                dfrom = datetime.datetime.strptime(dates['FROM'], '%Y%m%d%H%M%S.000000')
                # print('dates = {}'.format(dates))
                # print ('    {}; {}'.format(dto, dfrom))
                self.tickerfiles.append(TickerFile(f, dto, dfrom))
        self.tickerfiles = sorted(self.tickerfiles, key = lambda x: x.date_from)

    def get_tick(self):
        for tickerfile in self.tickerfiles:
            f = tickerfile.name
            with open(f, 'rt') as opened_file:
                reader = csv.reader(opened_file)
                next(reader)  # first line, being the header, can be ignored
                for row in reader:
                    # print('ABHI ............... row = {}'.format(row))
                    # return only actual data
                    yield row
    def __repr__(self):
        return str(self)
    def __str__(self):
        return '\n\n{}: {}'.format(
            self.ticker_name,
            '\n|____>'.join([x.name for x in self.tickerfiles])
            )

class TickerFile:
    def __init__(self, file, date_to, date_from):
        self.name = file
        self.date_from = date_from
        self.date_to = date_to


def get_reader(begin_date=_DAY, from_time=_FROM_TIME):
    reader = TickerColReader()
    files = []

    for s, priority in [(s[0].replace('|', '.'), s[1]) for s in _SYMBOLS]:
        # print('s = {}'.format(s))
        s_files = find_dirs_and_files(
            '.*{}.*{}.*'.format(s, begin_date),
            _FOLDER_L3,
            debug=False)
        files += s_files
        print('s _files for {}: \n - {}'.format(s, '\n - '.join(s_files)))
        reader.add_symbol_ticker(SymbolTicker(s, s_files), priority)
    # print('reader = {}'.format(reader))
    return files, reader


######################################################################################################
######################################################################################################
######################################################################################################
######################################################################################################

if "__main__" == __name__:
    counter = 1
    treader = get_reader()
    while(True):
        for tick in treader.read_ticks():
            # pretty_print_tick(tick)
            # print(tick)
            pretty_short_print(tick)
            # print(len(tick))
            counter += 1
            print('1')
            # if counter > 20:
            #     break
