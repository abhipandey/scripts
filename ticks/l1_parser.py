#!/usr/bin/env python

"""
Market data parsing routines
Supports the old BOX quote format

*Customized for JPX instruments*
"""

import csv
import copy
from datetime import datetime as dt
from datetime import timedelta

localTimeOffset = timedelta(hours=8)

def parseMDFile(filename):
    """ Process the specified market data file and returns a list of 
    mdUpdate """
    inFile = open(filename, 'r')
    # skip the header
    inFile.readline()
    #print(inFile.readline())
    # process line by line
    csvreader = csv.reader(inFile, delimiter=',', quotechar='"',
                           skipinitialspace=True)
    mdUpdates = []
    md = MDUpdateL1()
    for buffer in csvreader:
        
        # grab market data update
        mdUpdate = parse(buffer)
        #print('mdu',mdUpdate.askPx)
        if not mdUpdate:
            break
        #print(mdUpdate.timestamp)
        if mdUpdate.timestamp is not None:
            md.applyUpdate(mdUpdate)
            # print('MD = {}'.format(md))
            print(md)
            mdUpdates.append(copy.copy(md))

    inFile.close()
    return mdUpdates

def parse(buffer):
    """ Parses buffer and returns a MDUpdateL1 object"""
    tokens = [x.strip() for x in buffer]    
    if len(tokens) != 33:
        return None
    #print('test')
    mdUpdate = MDUpdateL1()
    date_=dt.strptime(tokens[1][:10], "%Y-%m-%d")
    mdUpdate.seqNum = int(tokens[3])
    #update exchange timestamp
    if (tokens[9]):
        mdUpdate.timestamp = dt.strptime(tokens[9][:8], "%H:%M:%S") + timedelta(microseconds=int(tokens[10].rjust(9, '0')[:6]))
    elif (tokens[13]):
        mdUpdate.timestamp = dt.strptime(tokens[13][:8], "%H:%M:%S") + timedelta(microseconds=int(tokens[14].rjust(9, '0')[:6]))
    elif (tokens[17]):
        mdUpdate.timestamp = dt.strptime(tokens[17][:8], "%H:%M:%S") + timedelta(microseconds=int(tokens[18].rjust(9, '0')[:6]))
#     else:
#         print("Error: no timestamp!")
    if mdUpdate.timestamp is not None:
        mdUpdate.timestamp = mdUpdate.timestamp + localTimeOffset
        mdUpdate.timestamp = mdUpdate.timestamp.replace(date_.year,date_.month,date_.day)
    #mdUpdate.timestamp = mdUpdate.timestamp + timedelta(microseconds=int(tokens[2]))
    mdUpdate.instr = tokens[0]
    mdUpdate.bidQty = int(tokens[12]) if tokens[12] else None
    mdUpdate.bidPx = float(tokens[11]) if tokens[11] else None
    mdUpdate.askQty = int(tokens[8]) if tokens[8] else None
    mdUpdate.askPx = float(tokens[7]) if tokens[7] else None
    mdUpdate.tradeQty = int(tokens[16]) if tokens[16] else None
    mdUpdate.tradePx = float(tokens[15]) if tokens[15] else None
    if tokens[17]:    
        tradeTime = dt.strptime(tokens[17][:8], "%H:%M:%S") + timedelta(microseconds=int(tokens[18].rjust(9, '0')[:6]))
        tradeTime = tradeTime + localTimeOffset
        tradeTime = tradeTime.replace(date_.year,date_.month,date_.day)
        #print(tradeTime)
        mdUpdate.tradeTime = tradeTime.strftime("%Y-%m-%d %H:%M:%S")  + '.'+ \
                             tokens[18].rjust(9, '0')
    #print(tokens)
    return mdUpdate

class MDUpdateL1(object):
    def __init__(self):
        self.timestamp = None
        self.instr = None
        self.seqNum = None
        self.bidQty = None
        self.bidPx = None
        self.askQty = None
        self.askPx = None
        self.tradeQty = None
        self.tradePx = None
        self.tradeTime = None # stored as string with nanosecond precision   
        
    def applyUpdate(self, update):
        """ Apply update onto self """
        if not self.instr:
            self.instr = update.instr
        elif self.instr != update.instr:
            print("Error: Multiple instruments in the data file!")
            return
        
        self.timestamp = update.timestamp
        self.seqNum = update.seqNum
        if update.bidQty:
            self.bidQty = update.bidQty
        if update.bidPx:
            self.bidPx = update.bidPx
        if update.askQty:
            self.askQty = update.askQty
        if update.askPx:
            self.askPx = update.askPx
            
        # always overwrite the tradePx, tradeQty and tradeTime
        self.tradeQty = update.tradeQty
        self.tradePx = update.tradePx
        self.tradeTime = update.tradeTime

    def __str__(self):
        x = '{:>10}  # {:<10}'
        return ('[ {:30} ] [{:10} ] ' + x + x + '    |    ' + x + ' <{}>').format(
            str(self.timestamp),
            self.seqNum,
            self.bidPx, self.bidQty,
            self.askPx, self.askQty,
            self.tradePx or '.', self.tradeQty or '.', self.tradeTime
        )

if __name__ == '__main__':
    print('test')
    parseMDFile('/home/abhishek.pandey/data/l1/FUTURE_XOSE_FUT_NK225M_1703_1703-l1-20170127050000.000000-20170127140000.000000.csv')