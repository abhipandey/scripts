#!/usr/bin/env python

import os, sys, re
# import csv
# import pprint
from collections import defaultdict

import pandas as pd
import  csv
import numpy as np
# import ctypes as c
import math

try:
    from .tickreader import *
    from .l3_to_l2 import *
    from .trade_collector import *
except:
    pass



from tickreader import *
from l3_to_l2 import *
from trade_collector import *


### -------------------- ### -------------------- ### -------------------- ###

# _INSTR_TO_FOLLOW = 'FUTURE|XOSE|FUT_NK225M_1703|1703'
# _TS_FMT = '%Y-%m-%d %H:%M:%S.%f+00:00'

### -------------------- ### -------------------- ### -------------------- ###


class OneTick:
    __slots__ = tuple(COLUMNS)  # from givemeorders

    def __init__(self, *kwargs):
        for slot, val in zip(OneTick.__slots__, kwargs):
            # price should be 'float' type
            setattr(self, slot, val if slot != 'price' else float(val) if val else 0.0)
        self._fillna(['qty', 'price', 'side'])

    def _fillna(self, attrs):
        for attr in attrs:
            if getattr(self, attr) == '':
                setattr(self, attr, 0)

    def __getitem__(self, item):
        return getattr(self, COLUMNS[item])

    def __setitem__(self, key, value):
        setattr(self, COLUMNS[key], value)

    def __str__(self):
        return ' ^ '.join(['{}: {}'.format(slot, getattr(self, slot)) for slot in OneTick.__slots__])

    def __repr__(self):
        return str(self)

_DAY = 20170124



def _has_validity_expired(tick, begin_ts):
    tick_ts = get_order_ts(tick)
    flip_val_end_ts = begin_ts + dt.timedelta(milliseconds=FLIPPER_VALIDITY)
    print('tick ts = {}; begin ts = {}; flip end = {}'.format(
        tick_ts, begin_ts, flip_val_end_ts
    ))
    return tick_ts <= flip_val_end_ts


def _round_clearing_ticks(movement, side):
    adj = (movement * 10) * 1.5
    fn = math.ceil if side == TickSide.SELL else math.floor
    return int(fn(adj / 5)) * 5


if __name__ == '__main__':
    file_name = '/tmp/1'

    books = defaultdict(lambda: L2State())
    with open('/home/abhishek.pandey/data/csv/xfade-{}.csv'.format(
            _DAY
    ), 'wt') as file:
        writer = csv.writer(file)
        cols = COLUMNS
        del cols[4]
        writer.writerow(cols + ['vol_cleared', str(X) + '_tick_moves_in_' + str(Y),
                                'prev_bbo', 'curr_bbo'])
        files, treader = get_reader(_DAY)
        tradescol = TradeCollector(files)
        # print('ABHI: files= {}'.format(files))

        topix_flipper = False
        last_flipper_ts = None
        topix_clearing_ticks = 0
        flip_side = None
        ref_bbo_s = dict()
        counter = 0

        while (True):
            for tick in treader.read_ticks():

                print('\n' + '###'*80 )
                print('\n' + '###'*80 )
                pretty_short_print(tick)
                print('topix_flipper={}; last_flipper_ts={}; topix_clearing_ticks={}'.format(
                    topix_flipper, last_flipper_ts, topix_clearing_ticks
                ))

                t = OneTick(*tick)
                tick_ts = get_order_ts(t)

                if tick_ts.hour == 0 and tick_ts.minute == 0 and \
                                tick_ts.second >= 2:
                    tradescol.destroy()
                    exit(1)

                trades = tradescol.get_trades(t)
                trd_ts = get_trade_ts(trades) if trades else None
                book = books[t.instrument]
                prev_bbo = list(book.get_bbo() or [''])
                res = book.add_order(t, trades)

                if 'TOPIX' in t.instrument:
                    if res[-2] and res[-1] and float(res[-2]) >= 30 and res[-1] >= X:

                        # if more than STICK_REF_TS ms have passed since last one,
                        # re-calculate all values
                        if not last_flipper_ts or last_flipper_ts + dt.timedelta(milliseconds=FLIPPER_VALIDITY) <= \
                            trd_ts:

                            last_flipper_ts = trd_ts
                            flip_side = res[-3]
                            topix_clearing_ticks = _round_clearing_ticks(res[-1], flip_side)
                            topix_flipper = True
                            print('ABHI: gotta check. Valid from {}; Side={}'.format(
                                last_flipper_ts, res[-3]))

                # topix_flipper_begin = topix_flipper
                topix_flipper = topix_flipper and \
                                _has_validity_expired(t, last_flipper_ts)

                # if topix_flipper != topix_flipper_begin:
                #     print('ABHI: topix_flipper  has flipped: ', topix_flipper_begin)

                if not topix_flipper:
                    ref_bbo_s = dict()

                bbo = ['']*4

                if topix_flipper and 'TOPIX' not in t.instrument:
                    if t.instrument not in ref_bbo_s:
                        print('ABHI: REPOSITION {} by {}'.format(
                            topix_clearing_ticks, flip_side))

                        bbo = list(book.get_bbo())
                        print('INSTR BBO = {}'.format(bbo))
                        if topix_clearing_ticks:
                            idx, multiplier = (0, -1) if flip_side == TickSide.BUY else (2, 1)
                            bbo[idx] += multiplier * topix_clearing_ticks
                            bbo[idx + 1] = 'X'
                            bbo[idx] = '*{}'.format(bbo[idx])
                            # bbo = ['*{}'.format(b) for b in bbo]
                            print('INSTR new BBO = {}'.format(bbo))
                            ref_bbo_s[t.instrument] = bbo
                    else:
                        bbo = ref_bbo_s[t.instrument]
                        non_change_idx, _ = (0, -1) if flip_side == TickSide.SELL else (2, 1)
                        orig_bbo = list(book.get_bbo())
                        bbo[non_change_idx] = orig_bbo[non_change_idx]



                counter += 1
                if not counter % 1000:
                    sys.stdout.write("\rRows Handled: <%s> %d " % (res[COLUMNS.index('order_time')], counter))
            #         # print('Rows handled: ', counter)
                res[0] = res[0].split('|')[2]

                del res[4]
                del res[-3]
                writer.writerow(res + [''.join(str(prev_bbo if any(prev_bbo) else [])),
                                       ''.join(str(bbo if any(bbo) else []))])



