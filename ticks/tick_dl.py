#!/usr/bin/env python


import subprocess
import os


symbols = [
           # "FUTURE|XOSE|FUT_NK225M_1703|1703",
           "FUTURE|XOSE|FUT_NK225_1703|1703",
           "FUTURE|XOSE|FUT_TOPIX_1703|1703",
           # "FUTURE|XOSE|FUT_NK225_1703|1703",
           # "FUTURE|XSES|TWG17|1702",
          ]

#symbols = ['FUTURE|XSES|NKU16|1609',
#           'FUTURE|XSES|TWQ16|1608',
#           'FUTURE|XSES|SGPQ16|1608',
#           'FUTURE|XSES|INQ16|1608']

# _DAY = day = 20170118
# _NUM_OF_DAYS = 3

# from_time = "05" + _TIME_FILLER  # "00:00"
# to_time = "12" + _TIME_FILLER  # "23:59"
# timezone = "SGT"

_TIME_FILLER = "0000.000000"
_DATE_RANGES = [
    (20170124, '05' + _TIME_FILLER, '14' + _TIME_FILLER, 'l1'),
    (20170125, '05' + _TIME_FILLER, '14' + _TIME_FILLER, 'l1'),
    (20170127, '05' + _TIME_FILLER, '14' + _TIME_FILLER, 'l1'),
    (20170130, '05' + _TIME_FILLER, '14' + _TIME_FILLER, 'l1'),
    (20170202, '05' + _TIME_FILLER, '09' + _TIME_FILLER, 'l1'),
    (20170202, '05' + _TIME_FILLER, '09' + _TIME_FILLER, 'l1'),
    (20170203, '05' + _TIME_FILLER, '11' + _TIME_FILLER, 'l1'),
    (20170213, '05' + _TIME_FILLER, '09' + _TIME_FILLER, 'l1'),
    (20170214, '05' + _TIME_FILLER, '12' + _TIME_FILLER, 'l1'),
    (20170215, '05' + _TIME_FILLER, '09' + _TIME_FILLER, 'l1'),
]
output_folder = "/home/abhishek.pandey/data/"

_FMT = '''/home/abhishek.pandey/code/higgs/pygh/env.sh /opt/py27env/bin/python \
    /home/abhishek.pandey/code/higgs/pygh/app/boxrawclient.py --solace-log-level INFO \
    --server-app-id "" --solace-session-prop-host csqprod-sol02 \
    --solace-session-prop-username risk --solace-session-prop-password risk \
    --solace-session-prop-vpn-name production --feed {} csv \
    --symbol "{}" --start-time {} --end-time {} --dest-folder {}
'''



if __name__ == '__main__':
    #subprocess.call(['cd','/home/abhishek.pandey/data/dev/latency/higgs/pygh'])
    for symbol in symbols:
        for tp_date, tp_from_time, tp_to_time, feed in _DATE_RANGES:
        # for d in range(0, _NUM_OF_DAYS):
            cmd = _FMT.format(
                feed,
                symbol,
                str(tp_date) + tp_from_time,
                str(tp_date) + tp_to_time,
                os.path.join(output_folder, feed)
            )
            print(cmd)
            os.system(cmd)
