import os, sys, re, datetime

_FOLDER = "/home/abhishek.pandey/data/TW/"


def get_all_files(folder=_FOLDER):
    return [x[2] for x in os.walk(folder)][0]


def get_file_with_pattern(dir, pattern):
    files = os.listdir(path=dir)
    file_found = [file for file in files if re.match(pattern, file, flags=re.IGNORECASE)]
    return file_found


def find_dirs_and_files(pattern, where='.', include_dirs=True, include_files=True, debug=False):
    '''Returns list of filenames from `where` path matched by 'which'
       shell pattern. Matching is case-insensitive.'''
    if not os.path.exists(where):
        return []
    if debug:
        print("where={}; pattern={};".format(where, pattern))
        # print("where={}; pattern={};".format(where, translate(pattern)))
    rule = re.compile(pattern, re.IGNORECASE)
    # list_files = [name for name in os.listdir(where) if rule.search(name)]
    list_files = [name for name in os.listdir(where) if re.search(pattern, name)]
    if debug:
        print('list_files={}'.format(list_files))

    if include_dirs and not include_files:
        list_files = [name for name in list_files if
                      os.path.isdir(os.path.join(where, name))]
    if include_files and not include_dirs:
        list_files = [name for name in list_files if
                      os.path.isfile(os.path.join(where, name))]
    if include_files:
        list_files = add_folder_to_list_of_files(where, list_files)
    return list_files

def add_folder_to_list_of_files(folder, files):
    return [os.path.join(folder, file) if not os.path.isabs(file)
                and os.path.exists(os.path.join(folder, file)) else file for file in files]



_DAY_BEGIN = '20170211'
_DAY_END = '20170212'
_T_BEGIN = "000000.000000"  # 00:00
_T_END = "235900.000000"  # 23:59

_FROM_TIME = _T_BEGIN
_TO_TIME   = _T_END


_SYMBOLS = [
           ( "FUTURE|XOSE|FUT_NK225M_1703|1703", 1),
           # ("FUTURE|XOSE|FUT_NK225_1703|1703", 1),
           # ("FUTURE|XOSE|FUT_TOPIX_1703|1703", 2),
           #"FUTURE|XOSE|FUT_NK225_1703|1703",
           # "FUTURE|XSES|TWG17|1702",
          ]


_DT_FMT = '%Y%m%d%H%M%S.000000'
_DT_BEGIN = datetime.datetime.strptime(_DAY_BEGIN + _T_BEGIN, _DT_FMT)
_DT_END = datetime.datetime.strptime(_DAY_END + _T_END, _DT_FMT)
print('begin = ', _DT_BEGIN)
print('begin = ', _DT_END)
for s, priority in [(s[0].replace('|', '.'), s[1]) for s in _SYMBOLS]:
    s_files = find_dirs_and_files( '.*' + s+ '.*', _FOLDER, debug=False)
    print('files = {}'.format('\n'.join(s_files)))

